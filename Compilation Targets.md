How should Dysnomia compile to machine code?

# Options

## LLVM
| Pros                          | Cons                                                                             |
| ----------------------------- | -------------------------------------------------------------------------------- |
| Popular option                | Documentation and guides could be better                                         |
| Loads of target architectures | Lack of C API tutorials                                                          |
|                               | So far, I've had trouble linking it                                              |
|                               | Compiling the lib takes lots of time and more memory than my computer can handle |


## WebAssembly
| Pros                                                         | Cons                                                                        |
| ------------------------------------------------------------ | --------------------------------------------------------------------------- |
| Can also target the browser free of charge                   | WASM to Native doesn't seem to be a common approach, but solutions do exist |
| Seemingly nicer to use than LLVM                             | Can debug info be added?                                                    |
| Can run using a WASM interpreter WASM-to-Native gets working | Not really WASM's purpose                                                   |
| Comes with sandboxing somehow, which could be useful         | Possible limitations in what data types WASM can represent                  |

### Links
[WebAssembly as an Intermediate Language for Provably-Safe Software Sandboxing [POPL 2020]](https://popl20.sigplan.org/details/prisc-2020-papers/2/WebAssembly-as-an-Intermediate-Language-for-Provably-Safe-Software-Sandboxing)
[Build your own WebAssembly Compiler](https://blog.scottlogic.com/2019/05/17/webassembly-compiler.html)
[Writing WebAssembly By Hand](https://blog.scottlogic.com/2018/04/26/webassembly-by-hand.html)


## C
| Pros                                                      | Cons                                                                          |
| --------------------------------------------------------- | ----------------------------------------------------------------------------- |
| Easier to map to Dysnomia source code than an IR would be | Debugging might be tricky, but doable (see links)                             |
| Loads of C compilers to choose from                       | Not really C's purpose                                                        |
|                                                           | Stuck with C baggage, UB, etc. Might need to stick with a limited subset of C |

### Links
[C as an Intermediate Language](https://yosefk.com/blog/c-as-an-intermediate-language.html)


## Zig
| Pros                                          | Cons                                                                                                |
| --------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| More modern than C, less surprise UB (I hope) | Debugging might be tricky, if at all possible, and I doubt anyone's already done it                 |
|                                               | Not really Zig's purpose                                                                            |
|                                               | For now, Zig still uses LLVM, and going Dys → Zig → LLVM IR → Machine code feels rather roundabout. |


## Direct to Native
| Pros                                                 | Cons                                                                   |
| ---------------------------------------------------- | ---------------------------------------------------------------------- |
| No wrangling 3rd party libs                          | A lot of work for something I don't intend to keep forever, because... |
| No learning a new framework                          | Supporting multiple platforms is demanding work                        |
| A custom IR can be as simple or as complex as needed | "Just looking at Register Allocation Coaster 1 makes me feel sick."    |
| Could be a good learning opportunity                 |                                                                        |


## Custom Bytecode with Interpreter

| Pros                                                                                          | Cons                                    |
| --------------------------------------------------------------------------------------------- | --------------------------------------- |
| No 3rd party libs                                                                             | Might take more work than other options |
| Offers flexibility, can be adapted to the needs of Dysnomia                                   | Needs a custom debugger                 |
| Portable to any platform supported by the language used to implement the bytecode interpreter |                                         |
| Sounds fun?                                                                                   |                                         |
