#!/usr/bin/env sh

set -e

cd "$(dirname "$0")"

echo "Setting up git hooks..."
ln -sf ../../.git-hooks/pre-commit ../.git/hooks/pre-commit
ln -sf ../../.git-hooks/pre-push ../.git/hooks/pre-push
