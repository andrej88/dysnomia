#!/usr/bin/env dub
/+ dub.sdl:
    name "run-tests"
    version "1.0.0"
    license "public domain"
    dependency "colorize" version="~>1.0"
    dependency "exceeds-expectations" version="~>0.7"
    stringImportPaths "."
+/

import colorize;
import exceeds_expectations;
import exceeds_expectations.exceptions;
import std.algorithm : any, filter, findSplit, fold, joiner, map, sort;
import std.array : array;
import std.conv : to;
import std.datetime.stopwatch;
import std.file : dirEntries, DirEntry, exists, FileException, getcwd, isDir, isFile, readText, SpanMode;
import std.format : format;
import std.path : asRelativePath, baseName, buildPath, extension, setExtension;
import std.process : Pid, pipeProcess, ProcessPipes, spawnProcess, wait;
import std.range : chain, drop;
import std.regex : matchFirst;
import std.stdio : fflush, File, stderr, stdout, write, writeln;
import std.string : stripRight;


int main(string[] args)
{
    auto splitArgs = args.findSplit(["--"]);
    string[] argsForDub = splitArgs[0].drop(1).array;
    string[] argsForDysnomia = splitArgs[2].array;
    Pid dubBuildPid = spawnProcess(["dub", "build", "--compiler=dmd"] ~ argsForDub);
    int buildStatus = wait(dubBuildPid);
    if (buildStatus != 0) return buildStatus;

    string dysnomiaPath = getcwd().buildPath("dysnomia");

    auto testSubfolders = getcwd()
        .buildPath("test", "build-and-run")
        .dirEntries(SpanMode.shallow)
        .array
        .sort!((a, b) =>  a.name < b.name);

    auto allTestsFlattened = testSubfolders
        .map!((DirEntry dirEntry) {
            return getcwd()
                .buildPath("test", "build-and-run", dirEntry.name)
                .dirEntries(SpanMode.depth)
                .filter!(e => e.isDir)
                .filter!(e => !e.dirEntries(SpanMode.shallow).any!(subFolder => subFolder.isDir))   // Filter out directories that contain other directories i.e. are groups of tests
                .array
                .sort!((a, b) => a.name < b.name);
        })
        .joiner;

    int passed;
    int skipped;
    int failed;
    int total;

    foreach (DirEntry entry; allTestsFlattened)
    {
        total++;

        string[] sourceFilePaths =
            entry
            .name
            .dirEntries(SpanMode.shallow)
            .map!((DirEntry entry) => entry.name)
            .filter!((string path) => path.extension == ".dys")
            .filter!isFile
            .array;

        // Sort because sometimes the order in which test files are provided
        // affects the error shown (e.g. duplicate namespace names).
        sourceFilePaths.sort();

        string expectedOutputPath = entry.name.buildPath("expected_output.txt");

        bool errorShouldUseMatch = false;
        string expectedErrorPath = entry.name.buildPath("expected_error.txt");
        if (!exists(expectedErrorPath))
        {
            errorShouldUseMatch = true;
            expectedErrorPath = entry.name.buildPath("expected_error_match.txt");
        }

        string expectedTokensPath = entry.name.buildPath("expected_tokens.sdl");
        string actualTokensPath = entry.name.buildPath("actual_tokens.sdl");
        string expectedCstPath = entry.name.buildPath("expected_cst.sdl");
        string actualCstPath = entry.name.buildPath("actual_cst.sdl");
        string expectedAstPath = entry.name.buildPath("expected_ast.sdl");
        string actualAstPath = entry.name.buildPath("actual_ast.sdl");
        string expectedExitCodePath = entry.name.buildPath("expected_exit_code.txt");

        bool checksStdOut = exists(expectedOutputPath) && isFile(expectedOutputPath);
        bool checksStdErr = exists(expectedErrorPath) && isFile(expectedErrorPath);
        bool checksTokens = exists(expectedTokensPath) && isFile(expectedTokensPath);
        bool checksCst = exists(expectedCstPath) && isFile(expectedCstPath);
        bool checksAst = exists(expectedAstPath) && isFile(expectedAstPath);
        bool checksExitCode = exists(expectedExitCodePath) && isFile(expectedExitCodePath);

        string testLabel = entry.name.asRelativePath(getcwd().buildPath("test")).to!string.color(mode.bold);

        write("▶".color(fg.blue) ~ " " ~ testLabel);
        fflush(stdout.getFP());

        if (sourceFilePaths.length == 0)
        {
            write("\r");
            writeln(
                "⏭".color(fg.yellow) ~ " " ~ testLabel ~ "\n" ~
                "  Test's directory contains no \".dys\" source files.".color(fg.yellow)
            );
            skipped++;
            continue;
        }

        if (!checksStdOut && !checksStdErr && !checksTokens && !checksCst && !checksAst && !checksExitCode)
        {
            write("\r");
            writeln(
                "⏭".color(fg.yellow) ~ " " ~ testLabel ~ "\n" ~
                "  Test does not check tokens, CST, AST, stdout, stderr, or exit code.".color(fg.yellow)
            );
            skipped++;
            continue;
        }

        fflush(stdout.getFP);

        string[] extraArgs = ["--stdlib-path", "./stdlib"];

        if (checksTokens)
            extraArgs ~= ["--debug-print-tokens", actualTokensPath];
        if (checksCst)
            extraArgs ~= ["--debug-print-cst", actualCstPath];
        if (checksAst)
            extraArgs ~= ["--debug-print-Ast", actualAstPath];

        ProcessPipes pipes = pipeProcess(
            [dysnomiaPath] ~ argsForDysnomia ~ extraArgs ~ sourceFilePaths
        );

        StopWatch stopWatch = StopWatch(AutoStart.no);
        stopWatch.start();
        int status = wait(pipes.pid);
        stopWatch.stop();
        double msecsElapsed = stopWatch.peek.total!"usecs" / 1000.0;

        if (!checksExitCode && !checksStdErr && status != 0)
        {
            write("\r");
            writeln("✗".color(fg.red) ~ " " ~ testLabel);
            writeln(
                "Test case exited with code " ~ status.to!string ~
                ", but neither expected_stderr.txt nor expected_exit_code.txt were found." ~
                "Program's standard error stream:"
            );
            writeln(readFileText(pipes.stderr));
            writeln();
            failed++;
            continue;
        }

        string failingTokensMessage;
        string failingCstMessage;
        string failingAstMessage;
        string failingOutputMessage;
        string failingErrorMessage;
        string failingExitCodeMessage;

        string actualOutput = removeSgrCodes(readFileText(pipes.stdout));
        string actualError = removeSgrCodes(readFileText(pipes.stderr));

        if (checksStdOut)
        {
            try
            {
                string expectedOutput = readText(expectedOutputPath).stripRight("\n");
                expect(actualOutput).toEqual(expectedOutput);
            }
            catch (FailingExpectationError e)
            {
                failingOutputMessage = e.message().to!string;
            }
        }

        if (checksStdErr)
        {
            try
            {
                string expectedError = readText(expectedErrorPath).stripRight("\n");

                if (errorShouldUseMatch)
                    expect(actualError).toMatch(expectedError);
                else
                    expect(actualError).toEqual(expectedError);
            }
            catch (FailingExpectationError e)
            {
                failingErrorMessage = e.message.to!string;
            }
        }

        if (checksTokens)
        {
            try
            {
                string actualTokens = readText(actualTokensPath);
                string expectedTokens = readText(expectedTokensPath);
                expect(actualTokens).toEqual(expectedTokens);
            }
            catch (FailingExpectationError e)
            {
                failingTokensMessage = e.message.to!string;
            }
            catch (FileException e)
            {
                failingAstMessage = "Could not open " ~ actualTokensPath;
            }
        }

        if (checksCst)
        {
            try
            {
                string actualCst = readText(actualCstPath);
                string expectedCst = readText(expectedCstPath);
                expect(actualCst).toEqual(expectedCst);
            }
            catch (FailingExpectationError e)
            {
                failingCstMessage = e.message.to!string;
            }
            catch (FileException e)
            {
                failingAstMessage = "Could not open " ~ actualCstPath;
            }
        }

        if (checksAst)
        {
            try
            {
                string actualAst = readText(actualAstPath);
                string expectedAst = readText(expectedAstPath);
                expect(actualAst).toEqual(expectedAst);
            }
            catch (FailingExpectationError e)
            {
                failingAstMessage = e.message.to!string;
            }
            catch (FileException e)
            {
                failingAstMessage = "Could not open " ~ actualAstPath;
            }
        }

        if (checksExitCode)
        {
            try
            {
                string actualExitCode = status.to!string;
                string expectedExitCode = readText(expectedExitCodePath).stripRight("\n");
                expect(actualExitCode).toEqual(expectedExitCode);
            }
            catch (FailingExpectationError e)
            {
                failingExitCodeMessage = e.message.to!string;
            }
        }

        if (
            failingTokensMessage == string.init &&
            failingCstMessage == string.init &&
            failingAstMessage == string.init &&
            failingOutputMessage == string.init &&
            failingErrorMessage == string.init &&
            failingExitCodeMessage == string.init
        )
        {
            write("\r");
            writeln("✓".color(fg.green) ~ " %-90s".format(testLabel) ~ " %9.3f ms".format(msecsElapsed));
            passed++;
            continue;
        }

        write("\r");
        writeln("✗".color(fg.red) ~ " %-90s".format(testLabel) ~ " %9.3f ms".format(msecsElapsed));

        if (failingExitCodeMessage != string.init)
        {
            writeln("Unexpected exit code:");
            writeln(failingExitCodeMessage);
        }
        else if (status != 0)
        {
            writeln("Program exited with code " ~ status.to!string ~ ".");
        }

        if (failingTokensMessage != string.init)
        {
            writeln("Unexpected tokens:");
            writeln(failingTokensMessage);
        }

        if (failingCstMessage != string.init)
        {
            writeln("Unexpected CST:");
            writeln(failingCstMessage);
        }

        if (failingAstMessage != string.init)
        {
            writeln("Unexpected AST:");
            writeln(failingAstMessage);
        }

        if (failingOutputMessage != string.init)
        {
            writeln("Unexpected standard output:");
            writeln(failingOutputMessage);
        }
        else if (actualOutput != string.init)
        {
            writeln("Got this standard output:");
            writeln(actualOutput);
        }

        if (failingErrorMessage != string.init)
        {
            writeln("Unexpected standard error:");
            writeln(failingErrorMessage);
        }
        else if (actualError != string.init)
        {
            writeln("Got this standard error:");
            writeln(actualError);
        }


        failed++;
    }

    writeln();
    writeln(
        (passed.to!string ~ " passed").color(fg.green) ~ "  " ~
        (failed.to!string ~ " failed").color(fg.red) ~ "  " ~
        (skipped.to!string ~ " skipped").color(fg.yellow) ~ "  " ~
        total.to!string ~ " total"
    );

    if (failed > 0)
        return 1;
    else
        return 0;
}

string readFileText(File file)
{
    return file.byLine.joiner("\n").array.to!string;
}

string removeSgrCodes(string input)
{
    import std.regex : regex, replaceAll;

    return replaceAll(input, regex("\033" ~ `\[[\d;]*m`), "");
}
