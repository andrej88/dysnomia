#!/usr/bin/env dub
/+ dub.sdl:
    name "test-watch"
    version "1.0.0"
    license "public domain"
    dependency "fswatch" version="~>0.6"
+/


import core.thread;
import fswatch;
import std.algorithm;
import std.array;
import std.datetime;
import std.file;
import std.path;
import std.process;
import std.string;


void main(string[] args)
{
    FileWatch sourceWatcher = FileWatch("source/", true);
    FileWatch programsWatcher = FileWatch("test/", true);

    runTests(args);

    while (true)
    {
        auto sourceChanges = sourceWatcher.getEvents().array;

        auto programsChanges = programsWatcher.getEvents().filter!(
            fce => fce.path.endsWith("expected_error.txt")
        ).array;

        if (sourceChanges.length > 0 || programsChanges.length > 0)
        {
            runTests(args);
        }

        Thread.sleep(500.msecs);
    }
}

void runTests(string[] args)
{
    Pid clearPid = spawnShell("clear");
    wait(clearPid);
    Pid dubTestPid = spawnProcess([__FILE__.dirName.buildPath("show_all_error_messages.d")] ~ args[1..$]);
    wait(dubTestPid);
}
