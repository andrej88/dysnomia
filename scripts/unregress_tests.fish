#!/usr/bin/env fish

for TEST_PATH in ./test/build-and-run/**/*
    if test -f "$TEST_PATH/actual_tokens.sdl"
        cp "$TEST_PATH/actual_tokens.sdl" "$TEST_PATH/expected_tokens.sdl"
    end

    if test -f "$TEST_PATH/actual_cst.sdl"
        cp "$TEST_PATH/actual_cst.sdl" "$TEST_PATH/expected_cst.sdl"
    end

    if test -f "$TEST_PATH/actual_ast.sdl"
        cp "$TEST_PATH/actual_ast.sdl" "$TEST_PATH/expected_ast.sdl"
    end
end
