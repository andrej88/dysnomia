#!/usr/bin/env dub
/+ dub.sdl:
    name "show-all-error-messages"
    version "1.0.0"
    license "public domain"
    dependency "colorize" version="~>1.0"
    stringImportPaths "."
+/

import std.algorithm;
import std.array;
import std.conv;
import std.file;
import std.path;
import std.process;
import std.range;
import std.stdio;
import colorize;


int main(string[] args)
{
    auto splitArgs = args.findSplit(["--"]);
    string[] argsForDub = splitArgs[0].drop(1).array;
    string[] argsForDysnomia = splitArgs[2].array;
    Pid dubBuildPid = spawnProcess(["dub", "build", "--compiler=dmd"] ~ argsForDub);
    int buildStatus = wait(dubBuildPid);
    if (buildStatus != 0) return buildStatus;
    writeln();

    string dysnomiaPath = getcwd().buildPath("dysnomia");

    auto testSubfolders = getcwd()
        .buildPath("test", "build-and-run")
        .dirEntries(SpanMode.shallow)
        .array
        .sort!((a, b) =>  a.name < b.name);

    auto allTestsFlattened = testSubfolders
        .map!((DirEntry dirEntry) {
            return getcwd()
                .buildPath("test", "build-and-run", dirEntry.name)
                .dirEntries(SpanMode.depth)
                .filter!(e => e.isDir)
                .filter!(e => !e.dirEntries(SpanMode.shallow).any!(subFolder => subFolder.isDir))   // Filter out directories that contain other directories i.e. are groups of tests
                .array
                .sort!((a, b) => a.name < b.name);
        })
        .joiner;

    foreach (DirEntry entry; allTestsFlattened)
    {
        string[] sourceFilePaths =
            entry
            .name
            .dirEntries(SpanMode.shallow)
            .map!((DirEntry entry) => entry.name)
            .filter!((string path) => path.extension == ".dys")
            .filter!isFile
            .array;

        string expectedErrorPath = entry.name.buildPath("expected_error.txt");
        string expectedErrorRelativePath = expectedErrorPath.asRelativePath(getcwd().buildPath("test")).to!string;

        bool checksStdErr = exists(expectedErrorPath) && isFile(expectedErrorPath);

        if (checksStdErr)
        {
            ProcessPipes pipes = pipeProcess(
                [dysnomiaPath] ~ argsForDysnomia ~ sourceFilePaths
            );

            wait(pipes.pid);

            string actualError = readFileText(pipes.stderr);

            cwriteln(("--- BEGIN " ~ expectedErrorRelativePath ~ " ---").color(fg.light_black));
            writeln();
            cwriteln(actualError);
            writeln();
            cwriteln(("--- END " ~ expectedErrorRelativePath ~ " ---").color(fg.light_black));
            writeln();
            writeln();
            writeln();
        }
    }

    return 0;
}

string readFileText(File file)
{
    return file.byLine.joiner("\n").array.to!string;
}
