<div style="display: flex; flex-direction: row; gap: 4ch; align-items: flex-start;">

| Implemented Feature        | Example                                                                                      |
| -------------------------- | -------------------------------------------------------------------------------------------- |
| Line Comment               | `; comment`                                                                                  |
| Function declaration       | `fn foo(x: Int, y:Int) {}`<br/>The type may go after the parameter list and before the body. |
| Function call              | `theUsual(param1, param2)`                                                                   |
| Member access              | `a.b.c`<br/>`d.e.f()`<br/>Technically these are operators!                                   |
| Namespace declaration      | `namespace This.Is.A.Namespace`<br/>                                                         |
| Addition and Subtraction   | `1 + 2 - 3`<br/>Lines may end with a binary operator, but not start with one.                |
| Unary negation             | `-6`<br/>`-foo`                                                                              |
| Typed variable declaration | `let myVar: Int = 5`                                                                         |

| To-Do Feature                      | Prototypes/ideas | Blockers                    |
| ---------------------------------- | ---------------- | --------------------------- |
| Type-inferred variable declaration | `let x = 3`      | Type checking and inference |
| Block Comment                      | ???              |                             |

</div>
