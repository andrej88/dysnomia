# Brainstorming

## Binary operator grammar

I would like to avoid implementing binary operators and precedence in a way that results in a complicated and highly nested grammar. 

A possible solution is to forgo operator precedence and require parentheses:

```
let x = 2 + 3 * 4
```

This would fail to parse with `Unexpected token '*'`. You would have to write:

```
let x = 2 + (3 * 4)
```

So there's no binary operator precedence, because parentheses must always be used to separate different operators. The grammar for this might be something like:

```abnf
Expr = PolyadicExpr / PrimaryExpr

PolyadicExpr = SumExpr / ProdExpr / ConcatExpr

SumExpr = PrimaryExpr SumExprTail *SumExprTail
SumExprTail = SumOp PrimaryExpr
SumOp = "+" / "-"
; etc.

PrimaryExpresion = ParenExpr / FnCall / tk.Ident ; / etc...
ParenExpr = "(" Expr ")"
; etc.
```

A downside of this I see right away is that to parse something like `(123)`, the grammar has to:
- Try `PolyadicExpr`
  - Try `SumExpr`
    - Try `PrimaryExpr`
      - Try `ParenExpr`
        - Try `Expr`
          - Try `PolyadicExpr` and fail all of its paths
          - Try `PrimaryExpr`
            - Try each path until it succeeds with `tk.NumberLiteral` or whatever
    - Try `SumExprTail`
      - Try `SumOp` and fail
  - Now do all that with `ProdExpr`, `ConcatExpr`, and any other path.

...you see where this is going. It needs to try the whole tree of rules for every possible operator before realizing it's a plain old `PrimaryExpr`.

So maybe a different approach is needed. Perhaps it *always* tries primary expression first, since that part is constant between branches.

```abnf
Expr = PrimaryExpr [PolyadicExprTail]

PolyadicExprTail = *SumExprTail / *ProdExprTail / *ConcatExprTail

SumExprTail = SumOp PrimaryExpr
SumOp = "+" / "-"
; etc.

PrimaryExpresion = ParenExpr / FnCall / tk.Ident ; / etc...
ParenExpr = "(" Expr ")"
; etc.
```

The issue I see there is the array-of-tails in the `PolyadicExprTail` rule. It needs to be a one-or-more repetition, not a zero-or-more. Following the restrictions defined in `cst.d`, this means something more like:

```abnf
Expr = PrimaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumOp PrimaryExpr [SumExprTail]
SumOp = "+" / "-"
; etc.

PrimaryExpresion = ParenExpr / FnCall / tk.Ident ; / etc...
ParenExpr = "(" Expr ")"
; etc.
```

But we can't have recursive rules because each rule is a struct. And that might be a problem... even if the `SumExprTail` recursion is removed, there is still recursion via `ParenExpr`.

**Update:** Now that concatenation and alternation nodes are classes, this might be doable.

Nonetheless, it might be nice to remove the recursion and work with a shallower CST. What about this?

```abnf
Expr = PrimaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumOp PrimaryExpr SumExprTailTail*
SumExprTailTail = SumOp PrimaryExpr
SumOp = "+" / "-"
; etc.

PrimaryExpresion = ParenExpr / FnCall / tk.Ident ; / etc...
ParenExpr = "(" Expr ")"
; etc.
```

Now there is that extra `TailTail` node, which might be a bit too abstract to have a sensible name. The recursive approach might be better after all. Unless...

```abnf
Expr = PrimaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumExprSegment SumExprSegment*
SumExprSegment = SumOp PrimaryExpr
SumOp = "+" / "-"
; etc.

PrimaryExpresion = ParenExpr / FnCall / tk.Ident ; / etc...
ParenExpr = "(" Expr ")"
; etc.
```

`Segment` is a better name than `TailTail` for sure.


### Member Access

Member access `.` is technically an operator. The above rules restrict its use to be in parentheses whenever it's part of some other operation. For example:

```
let magnitude = Math.sqrt(((v.x) * (v.x)) + ((v.y) * (v.y)))
```

This is pretty horrible compared to:

```
let magnitude = Math.sqrt((v.x * v.x) + (v.y * v.y))
```

And that's already pushing it.

So how to avoid it?

One option might be to make member access a type of `PrimaryExpression`.

```abnf
PrimaryExpresion = MemberAccess / ParenExpr / FnCall / tk.Ident ; / etc...
MemberAccess = tk.Ident MemberAccessTail
MemberAccessTail = MemberAccessSegment *MemberAccessSegment
MemberAccessSegment = "." tk.Ident
```

Some thoughts:
- This doesn't allow for `(vec1 + vec2).x` without left-recursion, I think
- Does the language have methods? How do we deal with that?
- What about accessing things within a namespace? That's also done using `.`, right? They should be treated the same in the grammar.
- Function calls are almost the same, but end in parentheses. Aren't function call parentheses a kind of operator? Kind of like a postfix operator?
- On that note, how to unary operators fit into the picture?

A new take on the binary operator grammar taking the above into account:


```abnf
Expr = UnaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumExprSegment SumExprSegment*
SumExprSegment = SumOp UnaryExpr
SumOp = "+" / "-"
; etc.

UnaryExpr = [PrefixOp] AtomicExpr [PostfixOp] ; Forbid spaces = annoying?
PrefixOp = "-" / "!" / etc
PostfixOp = FnCall / "++" / "--" / etc

AtomicExpr = ParenExpr / MemberAccessChain / literals / etc

MemberAccessChain = UnaryExpr *MemberAccessSegment
MemberAccessSegment = "." tk.Identifier     ; Forbid spaces = annoying? Maybe not, if the error messages are precise enough...

ParenExpr = "(" Expr ")"
; etc.
```

Possible alternative:

```abnf
Expr = UnaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumExprSegment SumExprSegment*
SumExprSegment = SumOp UnaryExpr
SumOp = "+" / "-"
; etc.

UnaryExpr = [PrefixOp] AtomicExpr [PostfixOp]
PrefixOp = "-" / "!" / etc
PostfixOp = MemberAccess / FnCall / etc
MemberAccess = "." tk.Identifier

AtomicExpr = ParenExpr / literals / etc


ParenExpr = "(" Expr ")"
; etc.
```

A problem with this is that you can't chain postfix operators, which means you can't do something as simple as `a.b()`. Let's make those lists:

```abnf
Expr = UnaryExpr [PolyadicExprTail]

PolyadicExprTail = SumExprTail / ProdExprTail / ConcatExprTail

SumExprTail = SumExprSegment SumExprSegment*
SumExprSegment = SumOp UnaryExpr
SumOp = "+" / "-"
; etc.

UnaryExpr = [PrefixOp] AtomicExpr *PostfixOp  ; No spaces - imagine "a \n (3)"
PrefixOp = "-" / "!" / etc
PostfixOp = MemberAccess / FnCall       ; no other postfix ops.
MemberAccess = "." tk.Identifier
FnCall = "(" *(Arg ",") ")"             ; just for demo

AtomicExpr = ParenExpr / literals / etc


ParenExpr = "(" Expr ")"
; etc.
```

Now there is inconsistency between `[PrefixOp]` and `*PostfixOp`. This might be ok, because why would you want double `-` or `!` anyway?

However, I'm questioning whether member access is a unary operation.

- On one hand, there are two sides, the object and the field name
- On the other hand, the dot and the field name could be seen as a single "operator".

Things still to consider:
- Null-safe member access (like Kotlin's `?.`)


## Binary operator semantics

Should binary operators:
- Have exactly one semantic meaning?
- Have multiple semantic meanings on a language level?
- Allow overloading to allow for user-defined semantics?

Let's compare each approach using made-up words.


### Mono-semantics

#### Pros
- Meaning is always clear from context. `a + b` is always addition, never anything else.
- Easier to implement. It doesn't take a type checker to know that `a + b` is a `SumExpression` (or something).

#### Cons
- We have to be frugal about defining operators or else risk running out of ASCII symbols for other purposes.
- All user-defined operations must be functions, increasing verbosity.


### Poly-semantics

#### Pros
- Allows reusing the same symbol for multiple operations. For example, `+` could be used for both addition and concatenation.

#### Cons
- It takes a type checker to know whether `a + b` is a valid operation.
- Associativity and arity might be inconsistent. Either they are hard-coded for each operator, or the code becomes more difficult to understand.


### User-semantics
aka operator overloading

#### Pros
- All the pros of Poly-semantics
- Allows for conveniences like adding two vectors using `+`

#### Cons
- All the problems of Poly-semantics
  - Specifically about arity: If `*` is overloaded to mean the dot-product of two vectors, then `vec1 * vec2 * vec3` is invalid.
- Implementation is much more complex, since `+` is effectively a compile-time defined function.


### Mono-semantics in-depth

At first glance, mono-semantics certainly look the most appealing to me. What about its cons? How bad are they?

#### Frugal symbol use
For con 1, we can plan ahead and be frugal with symbols. Here is a proposal:

Non-operator symbols:
| Symbol               | Meaning                  |
|----------------------|--------------------------|
| <code>&grave;</code> |                          |
| `!`<sup>1</sup>      | Mutable type             |
| `(` `)`              | Grouping, invocation     |
| `_`                  | Identifier character     |
| `[` `]`              | Reference                |
| `{` `}`              | Block                    |
| `\`                  | Escape                   |
| `;`                  | Comment                  |
| `:`                  | Type separator           |
| `"`                  | String                   |
| `,`                  | Param/Arg separator      |
| `.`                  | Member access            |
| `?`                  | Nullable type            |

Operator symbols:
| Symbol             | Meaning  | LHS    | RHS    | Result | Assoc. | Arity |
|--------------------|----------|--------|--------|--------|--------|-------|
| `~`                |          |        |        |        |        |       |
| `!`<sup>1</sup>    | Not      | n/a    | Bool   | Bool   | n/a    | 1     |
| `!=`               | NotEqual?| T      | T      | Bool   | n/a    | 2     |
| `@`                |          |        |        |        |        |       |
| `#`                |          |        |        |        |        |       |
| `$`                | ToString | n/a    | T      | String | n/a    | 1     |
| `%`                |          |        |        |        |        |       |
| `^`                |          |        |        |        |        |       |
| `&`                |          |        |        |        |        |       |
| `&&`               | And (SC) | Bool   | Bool   | Bool   | Left   | Poly  |
| `*` <sup>4</sup>   | Multiply | Number | Number | Number | Yes    | Poly  |
| `-` <sup>3</sup>   | Subtract | Number | Number | Number | Left   | Poly  |
| `-`                | Negate   | n/a    | Number | Number | n/a    | 1     |
| `=`                | Assign   | L      | R <: L | ⊥      | None   |       |
| `==`<sup>2</sup>   | IsEqual? | T      | T      | Bool   | Chain  | Poly  |
| `+` <sup>3</sup>   | Add      | Number | Number | Number | Yes    | Poly  |
| <code>&vert;<code> |          |        |        |        |        |       |
| <code>&vert;&vert;<code>| Or (SC) | Bool | Bool | Bool   | Left   | Poly  |
| <code>&vert;><code>| Pipe     | L      | R      | T      | Left   | Poly  |
| `'`                |          |        |        |        |        |       |
| `<`<sup>2</sup>    | Less?    | Number | Number | Bool   | Chain  | Poly  |
| `<=`<sup>2</sup>   | Less/Eq? | Number | Number | Bool   | Chain  | Poly  |
| `>`<sup>2</sup>    | Greater? | Number | Number | Bool   | Chain  | Poly  |
| `>=`<sup>2</sup>   | Gr/Eq?   | Number | Number | Bool   | Chain  | Poly  |
| `/` <sup>4</sup>   | Divide   | Number | Number | Number | Left   | Poly  |

<div style="border: solid 1px rgba(0,0,0,0.3); padding: 1em;">
<sup>1</sup> Has two meanings. I can live with that.

<sup>2</sup> These are polyadic among each other and have the same precedence. Unlike other operators, they are interpreted as a group instead of one-by-one, allowing for stuff like `0 <= x < 100`. Might not end up doing this, but I like the idea.  

<sup>3</sup> These are polyadic among each other and have the same precedence.

<sup>4</sup> These are polyadic among each other and have the same precedence.
</div>

So maybe running out of symbols isn't a huge concern. There are quite a few left over for stuff like concatenation.

#### Avoiding verbosity

Let's look at some common use-cases for operator overloading and see what value they bring.

| Use-case    | Operator | Non-operator |
|-------------|----------|--------------|
| Vector math | `vec1 * vec2` | `vec1.dot(vec2)` |
| Time ops    | `Time.now() + (2 * seconds)` | `Time.now().add(seconds(2))` |
| SemVer      | `SemVer(1, 2, 1) > SemVer(1, 1, 5)` | `SemVer(1, 2, 1).isGreater(SemVer(1, 1, 5))` |
| BigInt      | `BigInt(123) + BigInt(456)` | `BigInt(123).add(BigInt(456))` |

...personally I find the "Operator" variant nicer for all of these, except maybe the dot product. I also think the semantics are clear for all of these. Not to mention that overloading the equality operator is *super* useful in OOP.


### User-semantics Lite™

Maybe some kind of combination between mono-semantics and user-semantics would be nice, but they seem to contradict each other. What is it about them that we listed as pros earlier?

1. "Meaning is always clear from context."
2. "\[Mono-semantics are\] easier to implement."
3. "Allows reusing the same symbol for multiple operations."
4. "Allows for conveniences like adding two vectors using `+`."

2 is probably a cost we will have to bear for the sake of making the programmer's experience nicer. 3 is an advantage user-semantics inherits from poly-semantics, but our new "User-semantics lite" approach will prioritize point 1 over this.

This leaves us with points 1 and 4. Meaning should be clear from context while allowing for conveniences like we see in the table above.

How can we implement operator overloading to guarantee a nice user experience?

To start off with the low-hanging fruit, `==` must:
- not mutate either operand
- not cause side effects
- not accept operands of different types
- evaluate to a boolean

Relational operators could be customizable via some kind of `compareTo` function which must:
- not mutate its operands
- not cause side effects
- not accept operands of different types
- evaluate to an integer (or some kind of "less/equal/greater" enumerable)

But now we get to the trickier operators. What is it about the `BigInt` and `Time` examples that makes the "meaning always clear from context"? For one thing, they're both... "amounts"? There's probably a proper term for this. When you add two durations together, you get another duration. When you add two `BigInt`s, you get a `BigInt`. How can this be enforced? Let's say `+` must:
- not mutate either operand
- not cause side effects
- not accept operands of different types
- evaluate to the same type as its operands

These rules are pretty consistent so far.

The `SemVer` example also feels like an "amount" but I don't think it makes sense to add two `SemVer`s together, for example. Likewise, it doesn't really make sense to multiply two durations. So even though they are all "amounts", these operators should still be overloaded separately.

What about assignment operators? Overloading `=` might not be that useful, but overloading stuff like `+=`... that might be useful.

Some languages (e.g. D) require overloading `+=` separately from `+`. I don't like this, because it means that `a += b` might have completely different semantics to `a = a + b`. I would honestly just forbid that and define `+=` to essentially just be syntax sugar for `a = a + b`.

A consequence of all these rules is that an object can only be mutated via `=` or direct member access. A downside is that stuff like array growth is more verbose (e.g. `Array.push(arr, 4)` instead of `arr ~= 4`). I think it's worth it if it means fewer surprise mutations in the code and more consistent semantics.

### Conclusion

The preferred approach is to not overload operators on the language level, but allow overloading operators on the library/user code level with some restrictions. All operator overloads must be free of any side effects and abide by the same type restrictions of their built-in counterparts. For example, `==` must always return a boolean; `+`'s operands must have the same type, and it must evaluate to a new value of that type.

Following this, here is a table of all operators and their meanings:

| Operator | Meaning     | Functional signature | Can overload? |
|----------|-------------|----------------------|---------------|
| `a + b`  | Add         | `(T, T) -> T`        | ✓             |
| `a - b`  | Subtract    | `(T, T) -> T`        | ✓             |
| `a * b`  | Multiply    | `(T, T) -> T`        | ✓             |
| `a / b`  | Divide      | `(T, T) -> T`        | ✓             |
| `a = b`  | Assign      | `(A, B) -> ⊥`        |               |
| `!a`     | Not         | `(Bool) -> Bool`     |               |
| `a == b` | Equals?     | `(T, T) -> Bool`     | ✓             |
| `a != b` | `!(a == b)` | `(T, T) -> Bool`     | via `==`      |
| `a < b`  | Less?       | `(T, T) -> Bool`     | via `compare` and `==` |
| `a > b`  | NotLess, NotEq | `(T, T) -> Bool`  | via `compare` and `==` |
| `a <= b` | Less or Eql | `(T, T) -> Bool`     | via `compare` and `==` |
| `a >= b` | Not Less    | `(T, T) -> Bool`     | via `compare` and `==` |

TODO: Does this make sense? Can you really overload all relational operators with just two definitions?
