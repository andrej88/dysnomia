module dysnomia.log;

import colorize : bg, color, cwrite, fg, mode;
import std.conv : to;
import std.range : padRight;
import std.stdio : stderr, stdout;
import std.string;
import dysnomia.data.source_coordinates;
import std.datetime.systime;


shared Logger logger;

shared static this()
{
    import std.process : environment;
    import std.uni : asCapitalized;

    logger = new shared Logger();

    string logLevelFromEnvVar = environment.get("DYSNOMIA_LOG_LEVEL", "");
    if (logLevelFromEnvVar != "")
    {
        try
        {
            logger.minimumLogLevel = logLevelFromEnvVar.asCapitalized.to!string.to!LogLevel;
        }
        catch (Exception e)
        {
            stderr.writeln("Failed to set log level: " ~ e.to!string);
            throw e;
        }
    }
}

/// Semantics of each log level:
/// - **Error**: an unrecoverable error has occurred.
/// - **Info**: a non-error message that is meaningful to the end user.
/// - **Trace**: a message that is mostly only meaningful to the
///   developer for debugging purposes. Includes the file and line
///   number it was called at.
public enum LogLevel
{
    All,
    Trace,
    Info,
    Error,
    None,
}

private class Logger
{
    private shared LogLevel minimumLogLevel;

    private this()
    shared
    {
        debug(DYSNOMIA_LOGDEFAULT_TRACE)
            minimumLogLevel = LogLevel.Trace;
        else debug(DYSNOMIA_LOGDEFAULT_INFO)
            minimumLogLevel = LogLevel.Info;
        else debug(DYSNOMIA_LOGDEFAULT_ERROR)
            minimumLogLevel = LogLevel.Error;
        else debug(DYSNOMIA_LOGDEFAULT_NONE)
            minimumLogLevel = LogLevel.None;
        else
            minimumLogLevel = LogLevel.Info;
    }

    private void errorInCompile(Message)(Message message, SourceCoordinates sourceCoordinates)
    shared
    {
        if (minimumLogLevel > LogLevel.Error) return;
        enum label = " ERROR ".color(fg.light_white, bg.red, mode.bold);
        enum separator = " @ ".color(fg.light_black);

        stderr.cwrite(
            label, separator, sourceCoordinates.to!string, '\n',
            message, '\n',
        );
    }

    private void errorGeneric(Message)(Message message)
    shared
    {
        if (minimumLogLevel > LogLevel.Error) return;
        enum label = " ERROR ".color(fg.light_white, bg.red, mode.bold);

        stderr.cwrite(label, " ", message, '\n');
    }

    private void info(Message)(Message message)
    shared
    {
        if (minimumLogLevel > LogLevel.Info) return;
        enum label = color("INFO", fg.green, bg.init, mode.bold) ~ "    ";

        static if (__traits(compiles, message.splitLines))
            foreach (messageLine; message.splitLines)
                stderr.cwrite(label, messageLine, '\n');
        else
            stderr.cwrite(label, message, '\n');
    }

    private void trace(Message)(Message message, string file = __FILE__, int line = __LINE__)
    shared
    {
        if (minimumLogLevel > LogLevel.Trace) return;
        enum label = color("TRACE", fg.cyan, bg.init, mode.bold) ~ "   ";

        string timestamp = Clock.currTime.toString.color(fg.light_cyan).padRight(' ', 40).to!string;

        string location = color(file ~ ":" ~ line.to!string, fg.light_black);
        location = padRight(location, ' ', ((location.length / 4) + 1) * 4).to!string;


        static if (__traits(compiles, message.splitLines))
            foreach (messageLine; message.splitLines)
                stderr.cwrite(label, timestamp, location, messageLine, '\n');
        else
            stderr.cwrite(label, timestamp, location, message, '\n');
    }
}

public LogLevel getLogLevel()
{
    return logger.minimumLogLevel;
}

public void setLogLevel(LogLevel level)
{
    logger.minimumLogLevel = level;
}

public void errorInCompile(Message)(Message message, SourceCoordinates sourceCoordinates)
{
    logger.errorInCompile(message, sourceCoordinates);
}

public void errorGeneric(Message)(Message message)
{
    logger.errorGeneric(message);
}

public void info(Message)(Message message)
{
    logger.info(message);
}

public void trace(Message)(Message message, string file = __FILE__, int line = __LINE__)
{
    logger.trace(message, file, line);
}
