module dysnomia.phases.parse;

import cst = dysnomia.data.cst;
import dysnomia.data.common;
import dysnomia.data.source_coordinates;
import dysnomia.error.parse;
import dysnomia.util.sumtype;
import log = dysnomia.log;
import std.algorithm;
import std.conv : to;
import std.range;
import std.sumtype;
import std.traits : FieldNameTuple, isArray, Unqual;
import std.typecons : Nullable;
import tk = dysnomia.data.tokens;


private struct ParseResult(CstNodeType)
{
    CstNodeType cstNode;
    int tokensConsumed;
    ParseException[] failures;
}


// TODO: Make this only require the stack?
private void traceParser(Message)(size_t depth, Message message, string file = __FILE__, int line = __LINE__)
{
    log.trace(' '.repeat(depth).to!string ~ depth.to!string ~ " " ~ message.to!string, file, line);
}


/// A function to parse an array of tokens into a Concrete Syntax Tree
/// (see [dysnomia.data.cst]).
///
/// The return value is a pair of the CST and the number of tokens consumed.
/// [ParseException] is thrown if parsing fails.
///
/// As mentioned in the CST module, the CST data structures represent
/// the language's grammar. Pseudocode to translate the grammar into D
/// data structures:
///
/// ```pseudocode
/// For each rule:
///     If the rule is an alternation:
///         Create a SumType with the name of the rule,
///         with each alternative as an argument to the SumType.
///
///     Else if the rule is a concatenation:
///         Create a struct with the name of the rule
///         For each element of the concatenation:
///             If the item is optional:
///                 Make the field a std.typecons.Nullable of the type corresponding to that element
///             Else If the item is a zero-or-more repetition:
///                 Make the field an array of the type corresponding to that element
///             Else:
///                 Make the field just have the type corresponding to that element.
///
/// ```
private ParseResult!Cst parse(Cst)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
if (cst.isAlternationNode!Cst)
{
    stack = stack ~ typeid(Cst);
    traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");
    ParseException[] branchFailures;

    static foreach (Type; Cst.alternations.Types)
    {{
        try
        {
            ParseResult!Type parseNestedCstResult = parse!Type(tokens, position, stack);
            if (parseNestedCstResult.tokensConsumed != 0)
            {
                ParseResult!Cst result = ParseResult!Cst(
                    new Cst(parseNestedCstResult.cstNode),
                    parseNestedCstResult.tokensConsumed,
                    branchFailures ~ parseNestedCstResult.failures,
                );
                traceParser(stack.length, "Succeeded: `" ~ result.cstNode.source ~ "`");
                return result;
            }
        }
        catch (ParseException e)
        {
            branchFailures ~= e;
        }
    }}

    traceParser(stack.length, "Failed " ~ Cst.stringof ~ ": no alternatives matched");

    throw new FailingRuleException(
        tokens[position].sourceCoordinates,
        position,
        stack,
        branchFailures,
    );
}

/// ditto
private ParseResult!Cst parse(Cst)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
if (cst.isConcatenationNode!Cst)
{
    stack = stack ~ typeid(Cst);

    traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");
    ParseResult!Cst result;
    result.cstNode = new Cst();

    static foreach (string fieldName; FieldNameTuple!Cst)
    {{
        try
        {
            alias FieldType = typeof(__traits(getMember, Cst, fieldName));
            ParseResult!FieldType concatItemParseResult = parse!FieldType(tokens, position, stack);

            __traits(getMember, result.cstNode, fieldName) = concatItemParseResult.cstNode;
            result.tokensConsumed += concatItemParseResult.tokensConsumed;
            result.failures ~= concatItemParseResult.failures;
            position += concatItemParseResult.tokensConsumed;
        }
        catch (ParseException e)
        {
            traceParser(
                stack.length,
                "Failed " ~ Cst.stringof ~ ": " ~
                typeof(__traits(getMember, Cst, fieldName)).stringof ~ " failed to parse"
            );

            throw new FailingRuleException(
                tokens[position].sourceCoordinates,
                position,
                stack,
                result.failures ~ e,
            );
        }
    }}

    traceParser(stack.length, "Succeeded: `" ~ result.cstNode.source ~ "`");
    return result;
}


private ParseResult!Cst parse(Cst)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
if (cst.isSeparatedRepetitionNode!Cst)
{
    stack = stack ~ typeid(Cst);

    traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");
    ParseResult!Cst result;

    Cst.ElementType[] elements;
    Cst.SeparatorType[] separators;

    ParseResult!(Cst.ElementType) firstElementParseResult = parse!(Cst.ElementType)(tokens, position, stack);
    elements ~= firstElementParseResult.cstNode;
    result.tokensConsumed += firstElementParseResult.tokensConsumed;
    result.failures ~= firstElementParseResult.failures;
    position += firstElementParseResult.tokensConsumed;

    while (true)
    {
        try
        {
            ParseResult!(Cst.SeparatorType) separatorParseResult = parse!(Cst.SeparatorType)(tokens, position, stack);
            separators ~= separatorParseResult.cstNode;
            result.tokensConsumed += separatorParseResult.tokensConsumed;
            result.failures ~= separatorParseResult.failures;
            position += separatorParseResult.tokensConsumed;

            ParseResult!(Cst.ElementType) elementParseResult = parse!(Cst.ElementType)(tokens, position, stack);
            elements ~= elementParseResult.cstNode;
            result.tokensConsumed += elementParseResult.tokensConsumed;
            result.failures ~= elementParseResult.failures;
            position += elementParseResult.tokensConsumed;
        }
        catch (ParseException e)
        {
            result.failures ~= e;
            break;
        }
    }

    result.cstNode = new Cst(elements, separators);

    traceParser(stack.length, "Succeeded: `" ~ result.cstNode.source ~ "`");
    return result;
}


private ParseResult!Cst parse(Cst)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
if (tk.isAToken!Cst)
{
    stack = stack ~ typeid(Cst);
    traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");

    if (position >= tokens.length)
    {
        assert(
            false,
            "Ran out of tokens. This should never happen, as it means the parser " ~
            "is trying to parse beyond the EndOfInput token. The grammar may have a bug."
        );
    }

    ParseResult!Cst result;

    try
    {
        result.cstNode = tokens[position].tryGet!Cst;
    }
    catch (MatchException e)
    {
        import std.uni : toLower;

        traceParser(
            stack.length, "Expected " ~ Cst.stringof.toLower ~ " but received \'" ~ tokens[position].source ~ "'"
        );

        throw new WrongTokenException(
            tokens[position].sourceCoordinates,
            position,
            stack,
            tokens[position],
        );
    }

    result.tokensConsumed = 1;
    traceParser(stack.length, "Succeeded: `" ~ result.cstNode.source ~ "`");
    return result;
}


private ParseResult!Cst parse(Cst : Nullable!Payload, Payload)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
{
    stack = stack ~ typeid(Cst);
    ParseResult!Cst result;

    try
    {
        traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");

        ParseResult!Payload parseNestedCstResult = parse!Payload(tokens, position, stack);
        if (parseNestedCstResult.tokensConsumed > 0)
        {
            result.cstNode = parseNestedCstResult.cstNode;
            result.tokensConsumed += parseNestedCstResult.tokensConsumed;
            result.failures ~= parseNestedCstResult.failures;
        }

        traceParser(stack.length, "Succeeded: `" ~ result.cstNode.source ~ "`");
    }
    catch (ParseException e)
    {
        traceParser(stack.length, "Succeeded " ~ Cst.stringof ~ " by not matching anything");
        result.failures ~= new FailingRuleException(
            e.sourceCoordinates,
            position,
            stack,
            [e],
        );
    }

    return result;
}


private ParseResult!Cst parse(Cst)(
    scope tk.Token[] tokens,
    int position,
    TypeInfo[] stack,
)
if (isArray!Cst)
{
    stack = stack ~ typeid(Cst);
    traceParser(stack.length, "Trying " ~ Cst.stringof ~ "...");

    ParseResult!Cst result;
    alias Element = ElementType!Cst;
    while (true)
    {
        try
        {
            ParseResult!Element parseNestedCstResult = parse!Element(tokens, position, stack);
            result.cstNode ~= parseNestedCstResult.cstNode;
            result.tokensConsumed += parseNestedCstResult.tokensConsumed;
            result.failures ~= parseNestedCstResult.failures;
            position += parseNestedCstResult.tokensConsumed;
        }
        catch (ParseException e)
        {
            result.failures ~= e;
            break;
        }
    }

    traceParser(
        stack.length,
        "Succeeded with " ~ result.cstNode.length.to!string ~ " elements: `" ~
        result.cstNode.source ~ "`"
    );

    return result;
}

public Cst parse(Cst)(scope tk.Token[] tokens)
{
    try
    {
        return parse!Cst(tokens, 0, []).cstNode;
    }
    catch (ParseException e)
    {
        static string formatException(ParseException pe)
        {
            import std.string : lineSplitter;

            if (cast(FailingRuleException) pe)
            {
                return (
                    pe.message ~ "\n" ~
                    (cast(FailingRuleException) pe).potentialCauses.map!(
                        (ParseException bf) =>
                            formatException(bf)
                                .lineSplitter
                                .map!(line => "    " ~ line)
                                .join("\n")
                    ).join("\n")
                ).to!string;
            }

            else if (cast(WrongTokenException) pe)
                return pe.msg;

            else
                assert(false, "Unknown ParseException type: " ~ pe.to!string);
        }

        log.trace(formatException(e));

        ParseException[] bestFailedAttempts = e.mostSuccessfulCauses();
        throw new MultiParseException(bestFailedAttempts);
    }
}
