module dysnomia.phases.to_ast.expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.phases.to_ast.comparison_expression;
import dysnomia.phases.to_ast.logical_expression;
import dysnomia.phases.to_ast.null_coalescing_expression;
import dysnomia.phases.to_ast.product_expression;
import dysnomia.phases.to_ast.sum_expression;
import std.sumtype;


package ast.Expression toAstImpl(cst.Expression cstNode)
{
    if (cstNode.tail.isNull)
    {
        return new ast.Expression(cstNode, toAst(cstNode.head));
    }
    else
    {
        return cstNode.tail.get.alternations.match!(
            (cst.SumExpressionTail _) => new ast.Expression(
                cstNode,
                toSumExpression(cstNode),
            ),
            (cst.ProductExpressionTail _) => new ast.Expression(
                cstNode,
                toProductExpression(cstNode),
            ),
            (cst.ComparisonExpressionTail _) => new ast.Expression(
                cstNode,
                toComparisonExpression(cstNode),
            ),
            (cst.LogicalExpressionTail _) => new ast.Expression(
                cstNode,
                toLogicalExpression(cstNode),
            ),
            (cst.NullCoalescingExpressionTail _) => new ast.Expression(
                cstNode,
                toNullCoalescingExpression(cstNode),
            ),
        );
    }
}
