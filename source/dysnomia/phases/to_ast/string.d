module dysnomia.phases.to_ast.string;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;


package ast.String toAstImpl(cst.String s) => new ast.String(s, s.stringContents);
