module dysnomia.phases.to_ast.namespace;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.algorithm;
import std.array;
import std.sumtype;


package ast.Namespace toAstImpl(cst.Namespace cstNamespace) =>
    new ast.Namespace(
        cstNamespace,
        toAst(cstNamespace.namespaceDeclaration),
        toAst(cstNamespace.namespaceBody),
    );

package ast.NamespaceDeclaration toAstImpl(cst.NamespaceDeclaration cstNamespaceDeclaration) =>
    new ast.NamespaceDeclaration(
        cstNamespaceDeclaration,
        cstNamespaceDeclaration.namespaceIdentifier.elements,
    );

package ast.NamespaceBody toAstImpl(cst.NamespaceBody cstNamespaceBody) =>
    new ast.NamespaceBody(
        cstNamespaceBody,
        cstNamespaceBody
            .namespaceItemEntries
            .map!(namespaceItemEntry => namespaceItemEntry.namespaceItem)
            .filter!(namespaceItem => !namespaceItem.isNull)
            .map!(namespaceItem => namespaceItem.get)
            .map!toAst
            .array
    );

package ast.NamespaceItem toAstImpl(cst.NamespaceItem cstNamespaceItem) =>
    cstNamespaceItem.alternations.match!(
        (item) {
            return new ast.NamespaceItem(
                cstNamespaceItem,
                toAst(item),
            );
        },
    );
