module dysnomia.phases.to_ast.null_coalescing_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;


public ast.NullCoalescingExpression toNullCoalescingExpression(cst.Expression cstNode)
in (!cstNode.tail.isNull)
in (cstNode.tail.get.alternations.get!(cst.NullCoalescingExpressionTail))
{
    cst.NullCoalescingExpressionTail cstTail = cstNode.tail.get.alternations.get!(cst.NullCoalescingExpressionTail);
    ast.NullCoalescingExpressionSegment[] astTail;
    astTail ~= toAstImpl(cstTail.first);
    astTail ~= cstTail.rest.map!toAstImpl.array;

    return new ast.NullCoalescingExpression(
        cstNode,
        toAst(cstNode.head),
        astTail,
    );
}

private ast.NullCoalescingExpressionSegment toAstImpl(cst.NullCoalescingExpressionSegment cstNode) =>
    new ast.NullCoalescingExpressionSegment(
        cstNode,
        cstNode.operator.alternations.match!(
            (s) => new ast.NullCoalescingOperator(cstNode.operator, s),
        ),
        toAst(cstNode.expression),
    );
