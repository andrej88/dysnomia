module dysnomia.phases.to_ast.integer;

import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import tk = dysnomia.data.tokens;


package tk.Integer toAstImpl(cst.Integer i) => i.integer;
