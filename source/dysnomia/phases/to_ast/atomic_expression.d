module dysnomia.phases.to_ast.atomic_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.sumtype;


package ast.AtomicExpression toAstImpl(cst.AtomicExpression cstNode) =>
    cstNode.alternations.match!(
        (alternation) => new ast.AtomicExpression(cstNode, toAst(alternation)),
    );
