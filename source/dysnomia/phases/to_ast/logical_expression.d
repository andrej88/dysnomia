module dysnomia.phases.to_ast.logical_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;


public ast.LogicalExpression toLogicalExpression(cst.Expression cstNode)
in (!cstNode.tail.isNull)
in (cstNode.tail.get.alternations.get!(cst.LogicalExpressionTail))
{
    cst.LogicalExpressionTail cstTail = cstNode.tail.get.alternations.get!(cst.LogicalExpressionTail);
    ast.LogicalExpressionSegment[] astTail;
    astTail ~= toAstImpl(cstTail.first);
    astTail ~= cstTail.rest.map!toAstImpl.array;

    return new ast.LogicalExpression(
        cstNode,
        toAst(cstNode.head),
        astTail,
    );
}

private ast.LogicalExpressionSegment toAstImpl(cst.LogicalExpressionSegment cstNode) =>
    new ast.LogicalExpressionSegment(
        cstNode,
        cstNode.operator.alternations.match!(
            (s) => new ast.LogicalOperator(cstNode.operator, s),
        ),
        toAst(cstNode.expression),
    );
