module dysnomia.phases.to_ast.sum_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;


public ast.SumExpression toSumExpression(cst.Expression cstNode)
in (!cstNode.tail.isNull)
in (cstNode.tail.get.alternations.get!(cst.SumExpressionTail))
{
    cst.SumExpressionTail cstTail = cstNode.tail.get.alternations.get!(cst.SumExpressionTail);
    ast.SumExpressionSegment[] astTail;
    astTail ~= toAstImpl(cstTail.first);
    astTail ~= cstTail.rest.map!toAstImpl.array;

    return new ast.SumExpression(
        cstNode,
        toAst(cstNode.head),
        astTail,
    );
}

private ast.SumExpressionSegment toAstImpl(cst.SumExpressionSegment cstNode) =>
    new ast.SumExpressionSegment(
        cstNode,
        cstNode.operator.alternations.match!(
            (s) => new ast.SumOperator(cstNode.operator, s),
        ),
        toAst(cstNode.expression),
    );
