module dysnomia.phases.to_ast.if_else_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.typecons;


package ast.IfElseExpression toAstImpl(cst.IfElseExpression ifExp) =>
    new ast.IfElseExpression(
        ifExp,
        toAst(ifExp.condition),
        toAst(ifExp.thenBlock),
        ifExp.tail.apply!(
            (cst.IfElseExpressionTail t) => new ast.IfElseExpressionTail(
                t,
                toAst(t.elseBlock),
            ),
        ),
    );
