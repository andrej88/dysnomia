module dysnomia.phases.to_ast.boolean;

import cst = dysnomia.data.cst;
import tk = dysnomia.data.tokens;


package tk.Boolean toAstImpl(cst.Boolean b) => b.boolean;
