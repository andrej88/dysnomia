module dysnomia.phases.to_ast.comparison_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;


public ast.ComparisonExpression toComparisonExpression(cst.Expression cstNode)
in (!cstNode.tail.isNull)
in (cstNode.tail.get.alternations.get!(cst.ComparisonExpressionTail))
{
    cst.ComparisonExpressionTail cstTail = cstNode.tail.get.alternations.get!(cst.ComparisonExpressionTail);
    ast.ComparisonExpressionSegment[] astTail;
    astTail ~= toAstImpl(cstTail.first);
    astTail ~= cstTail.rest.map!toAstImpl.array;

    return new ast.ComparisonExpression(
        cstNode,
        toAst(cstNode.head),
        astTail,
    );
}

private ast.ComparisonExpressionSegment toAstImpl(cst.ComparisonExpressionSegment cstNode)
{
    return new ast.ComparisonExpressionSegment(
        cstNode,
        cstNode.operator.alternations.match!(
            (s) => new ast.ComparisonOperator(cstNode.operator, s),
        ),
        toAst(cstNode.expression),
    );
}
