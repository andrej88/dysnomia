module dysnomia.phases.to_ast.statement;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.sumtype;


package ast.Statement toAstImpl(cst.Statement cstNode) =>
    cstNode.alternations.match!(
        (cst.TypedVariableDefinition d) => new ast.Statement(
            cstNode,
            new ast.TypedVariableDefinition(
                d,
                d.name,
                toAst(d.typeExpression),
                toAst(d.initializer),
            ),
        ),
        (cst.Expression e) => new ast.Statement(cstNode, toAst(e)),
    );
