module dysnomia.phases.to_ast;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast.atomic_expression;
import dysnomia.phases.to_ast.block;
import dysnomia.phases.to_ast.boolean;
import dysnomia.phases.to_ast.comparison_expression;
import dysnomia.phases.to_ast.expression;
import dysnomia.phases.to_ast.file;
import dysnomia.phases.to_ast.functions;
import dysnomia.phases.to_ast.identifier;
import dysnomia.phases.to_ast.if_else_expression;
import dysnomia.phases.to_ast.imports;
import dysnomia.phases.to_ast.integer;
import dysnomia.phases.to_ast.logical_expression;
import dysnomia.phases.to_ast.namespace;
import dysnomia.phases.to_ast.null_;
import dysnomia.phases.to_ast.null_coalescing_expression;
import dysnomia.phases.to_ast.parenthetical_expression;
import dysnomia.phases.to_ast.product_expression;
import dysnomia.phases.to_ast.return_expression;
import dysnomia.phases.to_ast.statement;
import dysnomia.phases.to_ast.string;
import dysnomia.phases.to_ast.sum_expression;
import dysnomia.phases.to_ast.types;
import dysnomia.phases.to_ast.unary_expression;


public auto toAst(C)(C cst)
{
    // Why structure things like this?
    //
    // If each file has its own `toAst`, then it will never resolve the imported
    // `toAst`s from other files. This is because of how D's import rules work:
    // First, it searches the current file for instances of the function. If
    // none were found, it searches imports. If there is ambiguity within one
    // step, that's an error. But in our case, we would like it to consider
    // locally-defined methods with the same priority as imported ones. I am not
    // sure if there's a way to do this.
    //
    // This is the workaround I came up with. There is one `toAst` function
    // (this one). Since it's templated, it automatically dispatches to the
    // right implementation. All the implementations have the same "priority"
    // because, from this file's perspective, they are all imported and there
    // are no local `toAstImpl` functions.
    //
    // The return type is `auto`. If it's a template like `(A toAst(C, A)(C c))`
    // then it fails to compile. I guess it wants an explicit return type.
    //
    // I have been looking at `toAst`` so much now that all I see is "toast".

    return toAstImpl(cst);
}
