module dysnomia.phases.to_ast.parenthetical_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;


package ast.Expression toAstImpl(cst.ParentheticalExpression pe) => toAst(pe.expression);
