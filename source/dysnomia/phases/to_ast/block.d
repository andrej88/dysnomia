module dysnomia.phases.to_ast.block;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;


package ast.Statement[] toAstImpl(cst.Block cstBlock) =>
    cstBlock
        .blockEntries
        .map!(be => be.alternations.getOrElse!(cst.BlockLevelStatementEntry)(null))
        .filter!(it => it !is null)
        .map!(ble => ble.statement)
        .map!toAst
        .array;
