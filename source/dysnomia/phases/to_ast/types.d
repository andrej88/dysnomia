module dysnomia.phases.to_ast.types;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import std.sumtype;
import std.typecons;
import tk = dysnomia.data.tokens;


package ast.TypeExpression toAstImpl(cst.TypeExpression typeExpression) =>
    new ast.TypeExpression(
        typeExpression,
        toAst(typeExpression.payload),
        typeExpression.postfix.apply!toAst,
    );


private ast.AtomicTypeExpression toAst(cst.AtomicTypeExpression atomicTypeExpression) =>
    new ast.AtomicTypeExpression(
        atomicTypeExpression,
        atomicTypeExpression.alternations.match!(
            (tk.Identifier id) => id,
        ),
    );


private ast.TypePostfix toAst(cst.TypePostfix typePostfix) =>
    new ast.TypePostfix(
        typePostfix,
        new ast.TypePostfixOp(
            typePostfix.postfixOperator,
            typePostfix.postfixOperator.alternations.match!(
                (tk.SymbolQuestionMark qm) => qm,
            ),
        ),
    );
