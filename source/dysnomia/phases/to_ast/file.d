module dysnomia.phases.to_ast.file;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.algorithm;
import std.array;


package ast.Namespace[] toAstImpl(cst.File cstFile) =>
    cstFile.namespaces.map!toAst.array;
