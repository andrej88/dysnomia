module dysnomia.phases.to_ast.functions;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.nullable;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;
import std.typecons;
import tk = dysnomia.data.tokens;


package ast.FunctionDeclaration toAstImpl(cst.FunctionDeclaration cstFunctionDeclaration)
{
    Nullable!(cst.FunctionReturnType) returnType = cstFunctionDeclaration.functionReturnType;

    return new ast.FunctionDeclaration(
        cstFunctionDeclaration,
        cstFunctionDeclaration.name,
        (
            cstFunctionDeclaration.parameterBlock
                .parameters
                .map!(ple => ple.alternations.getOrElse!(cst.ParameterWithComma)(null))
                .filter!(it => it !is null)
                .map!(pwc => pwc.parameter)
                .array ~
                (cstFunctionDeclaration.parameterBlock.trailingParameter.getOrElse(null))
        )
            .filter!(it => it !is null)
            .map!toAst
            .array,
        returnType.apply!(rt => toAst(rt.typeExpression)),
        toAst(cstFunctionDeclaration.functionBody),
    );
}

package ast.Parameter toAstImpl(cst.Parameter cstNode) =>
    new ast.Parameter(
        cstNode,
        cstNode.name,
        toAst(cstNode.typeExpression),
    );

package ast.FunctionBody toAstImpl(cst.FunctionBody functionBody) =>
    functionBody.alternations.match!(
        (cst.Block block) => new ast.FunctionBody(functionBody, toAst(block)),
        (tk.KeywordIntrinsic i) => new ast.FunctionBody(functionBody, i),
    );
