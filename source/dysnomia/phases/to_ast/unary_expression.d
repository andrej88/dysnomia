module dysnomia.phases.to_ast.unary_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;
import std.typecons;
import tk = dysnomia.data.tokens;


package ast.UnaryExpression toAstImpl(cst.UnaryExpression cstNode) =>
    new ast.UnaryExpression(
        cstNode,
        cstNode.prefixOp.apply!(
            prefixOp =>
                prefixOp.alternations.match!(
                    (tk.SymbolMinus s) => new ast.PrefixOperator(cstNode.prefixOp.get, s),
                )
        ),
        toAst(cstNode.operand),
        cstNode.postfixOps.map!(
            postfixOp => postfixOp.alternations.match!(
                (cst.ArgumentBlock args) => new ast.PostfixOperator(
                    postfixOp,
                    toAstImpl(args)
                ),
                (cst.MemberAccess memberAccess) => new ast.PostfixOperator(
                    postfixOp,
                    new ast.MemberAccess(
                        memberAccess,
                        memberAccess.identifier,
                    )
                ),
            )
        ).array,
    );


private ast.ArgumentBlock toAstImpl(cst.ArgumentBlock cstNode)
{
    ast.Expression[] arguments =
        cstNode
            .arguments
            .map!(argListEntry => argListEntry.alternations.getOrElse!(cst.ArgumentWithComma)(null))
            .filter!(it => it !is null)
            .map!(argWithComma => argWithComma.expression)
            .map!toAst
            .array;

    if (!cstNode.trailingArgument.isNull)
    {
        arguments ~= toAst(cstNode.trailingArgument.get);
    }

    return new ast.ArgumentBlock(
        cstNode,
        arguments,
    );
}
