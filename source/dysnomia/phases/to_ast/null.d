module dysnomia.phases.to_ast.null_;

import tk = dysnomia.data.tokens;


package tk.Null toAstImpl(tk.Null n) => n;
