module dysnomia.phases.to_ast.return_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import std.typecons;


package ast.ReturnExpression toAstImpl(cst.ReturnExpression r) =>
    new ast.ReturnExpression(
        r,
        r.payload.apply!(payload => toAst(payload.expression)),
    );
