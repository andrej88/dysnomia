module dysnomia.phases.to_ast.imports;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import std.algorithm;
import std.array;


package ast.ImportDeclaration toAstImpl(cst.ImportDeclaration cstImportDeclaration) =>
    new ast.ImportDeclaration(
        cstImportDeclaration,
        cstImportDeclaration.namespace.elements,
    );
