module dysnomia.phases.to_ast.identifier;

import tk = dysnomia.data.tokens;


package tk.Identifier toAstImpl(tk.Identifier id) => id;
