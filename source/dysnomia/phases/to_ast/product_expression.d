module dysnomia.phases.to_ast.product_expression;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.phases.to_ast;
import dysnomia.util.sumtype;
import std.algorithm;
import std.array;
import std.sumtype;


public ast.ProductExpression toProductExpression(cst.Expression cstNode)
in (!cstNode.tail.isNull)
in (cstNode.tail.get.alternations.get!(cst.ProductExpressionTail))
{
    cst.ProductExpressionTail cstTail = cstNode.tail.get.alternations.get!(cst.ProductExpressionTail);
    ast.ProductExpressionSegment[] astTail;
    astTail ~= toAstImpl(cstTail.first);
    astTail ~= cstTail.rest.map!toAstImpl.array;

    return new ast.ProductExpression(
        cstNode,
        toAst(cstNode.head),
        astTail,
    );
}

private ast.ProductExpressionSegment toAstImpl(cst.ProductExpressionSegment cstNode) =>
    new ast.ProductExpressionSegment(
        cstNode,
        cstNode.operator.alternations.match!(
            (s) => new ast.ProductOperator(cstNode.operator, s),
        ),
        toAst(cstNode.expression),
    );
