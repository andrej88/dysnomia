module dysnomia.phases.lex_test;

import dysnomia.data.source_coordinates;
import dysnomia.data.tokens;
import dysnomia.error.lex;
import dysnomia.phases.lex;
import exceeds_expectations;


@("one identifier")
unittest
{
    expect(lex("a", "/dummy.dys")).toEqual([
        Identifier.make("a", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);

    expect(lex("ab", "/dummy.dys")).toEqual([
        Identifier.make("ab", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);

    expect(lex("count", "/dummy.dys")).toEqual([
        Identifier.make("count", SourceCoordinates("/dummy.dys", 1, 1), 0, 5),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 6), 5, 5),
    ]);
}

@("two identifiers with non-ASCII letters, separated by several spaces and tabs")
unittest
{
    expect(lex("Два   \t  \ttanımlayıcılar", "/dummy.dys")).toEqual([
        Identifier.make("Два", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        HSpace.make("   \t  \t", SourceCoordinates("/dummy.dys", 1, 4), 6, 13),
        Identifier.make("tanımlayıcılar", SourceCoordinates("/dummy.dys", 1, 11), 13, 30),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 25), 30, 30),
    ]);
}

@("Identifiers may contain underscores in the middle")
unittest
{
    expect(lex("a_b", "/dummy.dys")).toEqual([
        Identifier.make("a_b", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 4), 3, 3),
    ]);

    expect(lex("take_a_breath", "/dummy.dys")).toEqual([
        Identifier.make("take_a_breath", SourceCoordinates("/dummy.dys", 1, 1), 0, 13),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 14), 13, 13),
    ]);
}

@("Identifiers may not contain leading or trailing underscores")
unittest
{
    expect({ lex("_asdasd", "/dummy.dys"); }).toThrow!InvalidSourceException;
    expect({ lex("asdasd_", "/dummy.dys"); }).toThrow!InvalidSourceException;
}

@("Standalone underscores are not valid")
unittest
{
    expect({ lex("_", "/dummy.dys"); }).toThrow!InvalidSourceException;
    expect({ lex("__", "/dummy.dys"); }).toThrow!InvalidSourceException;
    expect({ lex("___", "/dummy.dys"); }).toThrow!InvalidSourceException;
}

@("Boolean literals")
unittest
{
    expect(lex("true", "/dummy.dys")).toEqual([
        Boolean.make("true", SourceCoordinates("/dummy.dys", 1, 1), 0, 4),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 5), 4, 4),
    ]);

    expect(lex("false", "/dummy.dys")).toEqual([
        Boolean.make("false", SourceCoordinates("/dummy.dys", 1, 1), 0, 5),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 6), 5, 5),
    ]);

    expect(lex("trueish", "/dummy.dys")).toEqual([
        Identifier.make("trueish", SourceCoordinates("/dummy.dys", 1, 1), 0, 7),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 8), 7, 7),
    ]);

    expect(lex("falsehood", "/dummy.dys")).toEqual([
        Identifier.make("falsehood", SourceCoordinates("/dummy.dys", 1, 1), 0, 9),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 10), 9, 9),
    ]);

    expect(lex("misconstrue", "/dummy.dys")).toEqual([
        Identifier.make("misconstrue", SourceCoordinates("/dummy.dys", 1, 1), 0, 11),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 12), 11, 11),
    ]);

    expect(lex("unfalse", "/dummy.dys")).toEqual([
        Identifier.make("unfalse", SourceCoordinates("/dummy.dys", 1, 1), 0, 7),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 8), 7, 7),
    ]);
}

@("Simple decimal integers")
unittest
{
    expect(lex("0", "/dummy.dys")).toEqual([
        Integer.make("0", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);

    expect(lex("4", "/dummy.dys")).toEqual([
        Integer.make("4", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);

    expect(lex("29", "/dummy.dys")).toEqual([
        Integer.make("29", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);

    expect(lex("123 999231", "/dummy.dys")).toEqual([
        Integer.make("123", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
        HSpace.make(" ", SourceCoordinates("/dummy.dys", 1, 4), 3, 4),
        Integer.make("999231", SourceCoordinates("/dummy.dys", 1, 5), 4, 10),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 11), 10, 10),
    ]);
}

@("Integers may contain underscores in between digits")
unittest
{
    expect(lex("1_2", "/dummy.dys")).toEqual([
        Integer.make("1_2", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 4), 3, 3),
    ]);

    expect(lex("999_999_999", "/dummy.dys")).toEqual([
        Integer.make("999_999_999", SourceCoordinates("/dummy.dys", 1, 1), 0, 11),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 12), 11, 11),
    ]);
}

@("Integers may not contain underscores at the start, end, or after another undescore")
unittest
{
    expect({ lex("_123", "/dummy.dys"); }).toThrow!InvalidSourceException;
    expect({ lex("123_", "/dummy.dys"); }).toThrow!InvalidSourceException;
}

@("Integers with leading zeroes are forbidden")
unittest
{
    expect({ lex("0221", "/dummy.dys"); }).toThrow!InvalidSourceException;
    expect({ lex("02_21", "/dummy.dys"); }).toThrow!InvalidSourceException;
}

@("Keyword: fn")
unittest
{
    expect(lex("fn", "/dummy.dys")).toEqual([
        KeywordFn.make("fn", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);

    expect(lex("fnese", "/dummy.dys")).toEqual([
        Identifier.make("fnese", SourceCoordinates("/dummy.dys", 1, 1), 0, 5),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 6), 5, 5),
    ]);

    expect(lex("Annwfn", "/dummy.dys")).toEqual([
        Identifier.make("Annwfn", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 7), 6, 6),
    ]);

    expect(lex("hafnium", "/dummy.dys")).toEqual([
        Identifier.make("hafnium", SourceCoordinates("/dummy.dys", 1, 1), 0, 7),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 8), 7, 7),
    ]);
}

@("Keyword: if")
unittest
{
    expect(lex("if", "/dummy.dys")).toEqual([
        KeywordIf.make("if", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);

    expect(lex("iforgor", "/dummy.dys")).toEqual([
        Identifier.make("iforgor", SourceCoordinates("/dummy.dys", 1, 1), 0, 7),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 8), 7, 7),
    ]);

    expect(lex("gif", "/dummy.dys")).toEqual([
        Identifier.make("gif", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 4), 3, 3),
    ]);

    expect(lex("stiff", "/dummy.dys")).toEqual([
        Identifier.make("stiff", SourceCoordinates("/dummy.dys", 1, 1), 0, 5),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 6), 5, 5),
    ]);
}

@("Keyword: let")
unittest
{
    expect(lex("let", "/dummy.dys")).toEqual([
        KeywordLet.make("let", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 4), 3, 3),
    ]);

    expect(lex("letter", "/dummy.dys")).toEqual([
        Identifier.make("letter", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 7), 6, 6),
    ]);

    expect(lex("outlet", "/dummy.dys")).toEqual([
        Identifier.make("outlet", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 7), 6, 6),
    ]);

    expect(lex("subletting", "/dummy.dys")).toEqual([
        Identifier.make("subletting", SourceCoordinates("/dummy.dys", 1, 1), 0, 10),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 11), 10, 10),
    ]);
}

@("Keyword: return")
unittest
{
    expect(lex("return", "/dummy.dys")).toEqual([
        KeywordReturn.make("return", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 7), 6, 6),
    ]);

    expect(lex("returntosender", "/dummy.dys")).toEqual([
        Identifier.make("returntosender", SourceCoordinates("/dummy.dys", 1, 1), 0, 14),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 15), 14, 14),
    ]);

    expect(lex("pointofnoreturn", "/dummy.dys")).toEqual([
        Identifier.make("pointofnoreturn", SourceCoordinates("/dummy.dys", 1, 1), 0, 15),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 16), 15, 15),
    ]);

    expect(lex("nonreturnable", "/dummy.dys")).toEqual([
        Identifier.make("nonreturnable", SourceCoordinates("/dummy.dys", 1, 1), 0, 13),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 14), 13, 13),
    ]);
}

@("Keyword: namespace")
unittest
{
    expect(lex("namespace", "/dummy.dys")).toEqual([
        KeywordNamespace.make("namespace", SourceCoordinates("/dummy.dys", 1, 1), 0, 9),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 10), 9, 9),
    ]);

    expect(lex("namespaceballs", "/dummy.dys")).toEqual([
        Identifier.make("namespaceballs", SourceCoordinates("/dummy.dys", 1, 1), 0, 14),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 15), 14, 14),
    ]);

    expect(lex("surnamespace", "/dummy.dys")).toEqual([
        Identifier.make("surnamespace", SourceCoordinates("/dummy.dys", 1, 1), 0, 12),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 13), 12, 12),
    ]);

    expect(lex("nicknamespacerace", "/dummy.dys")).toEqual([
        Identifier.make("nicknamespacerace", SourceCoordinates("/dummy.dys", 1, 1), 0, 17),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 18), 17, 17),
    ]);
}

@("Keyword: import")
unittest
{
    expect(lex("import", "/dummy.dys")).toEqual([
        KeywordImport.make("import", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 7), 6, 6),
    ]);

    expect(lex("important", "/dummy.dys")).toEqual([
        Identifier.make("important", SourceCoordinates("/dummy.dys", 1, 1), 0, 9),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 10), 9, 9),
    ]);

    expect(lex("reimport", "/dummy.dys")).toEqual([
        Identifier.make("reimport", SourceCoordinates("/dummy.dys", 1, 1), 0, 8),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 9), 8, 8),
    ]);

    expect(lex("unimportant", "/dummy.dys")).toEqual([
        Identifier.make("unimportant", SourceCoordinates("/dummy.dys", 1, 1), 0, 11),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 12), 11, 11),
    ]);
}

@("Symbol: =")
unittest
{
    expect(lex("=", "/dummy.dys")).toEqual([
        SymbolEquals.make("=", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbols: { }")
unittest
{
    expect(lex("{}", "/dummy.dys")).toEqual([
        SymbolBraceLeft.make("{", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        SymbolBraceRight.make("}", SourceCoordinates("/dummy.dys", 1, 2), 1, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);
}

@("Symbols: ( )")
unittest
{
    expect(lex("()", "/dummy.dys")).toEqual([
        SymbolParenLeft.make("(", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        SymbolParenRight.make(")", SourceCoordinates("/dummy.dys", 1, 2), 1, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);
}

@("Symbol: ,")
unittest
{
    expect(lex(",", "/dummy.dys")).toEqual([
        SymbolComma.make(",", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbol: :")
unittest
{
    expect(lex(":", "/dummy.dys")).toEqual([
        SymbolColon.make(":", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbol: +")
unittest
{
    expect(lex("+", "/dummy.dys")).toEqual([
        SymbolPlus.make("+", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbol: -")
unittest
{
    expect(lex("-", "/dummy.dys")).toEqual([
        SymbolMinus.make("-", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbol: *")
unittest
{
    expect(lex("*", "/dummy.dys")).toEqual([
        SymbolAsterisk.make("*", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Symbol: ==")
unittest
{
    expect(lex("==", "/dummy.dys")).toEqual([
        SymbolDoubleEquals.make("==", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);
}

@("Symbol: .")
unittest
{
    expect(lex(".", "/dummy.dys")).toEqual([
        SymbolDot.make(".", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);
}

@("Single-line comments")
unittest
{
    expect(lex(";   \t  This is a simple comment", "/dummy.dys")).toEqual([
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        HSpace.make("   \t  ", SourceCoordinates("/dummy.dys", 1, 2), 1, 7),
        CommentPlaintext.make("This is a simple comment", SourceCoordinates("/dummy.dys", 1, 8), 7, 31),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 32), 31, 31),
    ]);

    expect(lex(";", "/dummy.dys")).toEqual([
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 2), 1, 1),
    ]);

    expect(lex(";NoSpaceDontCare", "/dummy.dys")).toEqual([
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        CommentPlaintext.make("NoSpaceDontCare", SourceCoordinates("/dummy.dys", 1, 2), 1, 16),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 17), 16, 16),
    ]);
}

@("Two comment lines in a row")
unittest
{
    expect(lex("; This comment\n; spans two lines", "/dummy.dys")).toEqual([
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        HSpace.make(" ", SourceCoordinates("/dummy.dys", 1, 2), 1, 2),
        CommentPlaintext.make("This comment", SourceCoordinates("/dummy.dys", 1, 3), 2, 14),
        VSpace.make("\n", SourceCoordinates("/dummy.dys", 1, 15), 14, 15),
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 2, 1), 15, 16),
        HSpace.make(" ", SourceCoordinates("/dummy.dys", 2, 2), 16, 17),
        CommentPlaintext.make("spans two lines", SourceCoordinates("/dummy.dys", 2, 3), 17, 32),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 2, 18), 32, 32),
    ]);
}

@("Lots of vertical spacing")
unittest
{
    expect(lex("long\n\n\n\n\n\n\n\n\n\ncat", "/dummy.dys")).toEqual([
        Identifier.make("long", SourceCoordinates("/dummy.dys", 1, 1), 0, 4),
        VSpace.make("\n\n\n\n\n\n\n\n\n\n", SourceCoordinates("/dummy.dys", 1, 5), 4, 14),
        Identifier.make("cat", SourceCoordinates("/dummy.dys", 11, 1), 14, 17),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 11, 4), 17, 17),
    ]);
}

@("Lines containing only horizontal spacing are merged into a single VSpace token")
unittest
{
    expect(lex("secret    \n    \t    \n\t\n  \n    message", "/dummy.dys")).toEqual([
        Identifier.make("secret", SourceCoordinates("/dummy.dys", 1, 1), 0, 6),
        HSpace.make("    ", SourceCoordinates("/dummy.dys", 1, 7), 6, 10),
        VSpace.make("\n    \t    \n\t\n  \n", SourceCoordinates("/dummy.dys", 1, 11), 10, 26),
        HSpace.make("    ", SourceCoordinates("/dummy.dys", 5, 1), 26, 30),
        Identifier.make("message", SourceCoordinates("/dummy.dys", 5, 5), 30, 37),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 5, 12), 37, 37),
    ]);
}

@("Simple strings")
unittest
{
    expect(lex(`""`, "/dummy.dys")).toEqual([
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 2), 1, 2),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 3), 2, 2),
    ]);

    expect(lex(`"Hello, world!"`, "/dummy.dys")).toEqual([
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        StringPlaintext.make("Hello, world!", SourceCoordinates("/dummy.dys", 1, 2), 1, 14),
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 15), 14, 15),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 16), 15, 15),
    ]);
}

@("A string in a comment")
unittest
{
    expect(lex(`; "Murder", she wrote.`, "/dummy.dys")).toEqual([
        SymbolSemicolon.make(";", SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        HSpace.make(" ", SourceCoordinates("/dummy.dys", 1, 2), 1, 2),
        CommentPlaintext.make(`"Murder", she wrote.`, SourceCoordinates("/dummy.dys", 1, 3), 2, 22),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 23), 22, 22),
    ]);
}

@("A comment in a string")
unittest
{
    expect(lex(`"Semicolons; how peculiar they are"`, "/dummy.dys")).toEqual([
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 1), 0, 1),
        StringPlaintext.make("Semicolons; how peculiar they are", SourceCoordinates("/dummy.dys", 1, 2), 1, 34),
        SymbolQuotes.make(`"`, SourceCoordinates("/dummy.dys", 1, 35), 34, 35),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 1, 36), 35, 35),
    ]);
}

@("A function definition")
unittest
{
    expect(lex(
`fn doSomething(withMe: String, andMe: Int) {
    ; a work in progress
    return
}`,
    "/dummy.dys"))
    .toEqual([
        KeywordFn.make(`fn`, SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 1, 3), 2, 3),
        Identifier.make(`doSomething`, SourceCoordinates("/dummy.dys", 1, 4), 3, 14),
        SymbolParenLeft.make(`(`, SourceCoordinates("/dummy.dys", 1, 15), 14, 15),
        Identifier.make(`withMe`, SourceCoordinates("/dummy.dys", 1, 16), 15, 21),
        SymbolColon.make(`:`, SourceCoordinates("/dummy.dys", 1, 22), 21, 22),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 1, 23), 22, 23),
        Identifier.make(`String`, SourceCoordinates("/dummy.dys", 1, 24), 23, 29),
        SymbolComma.make(`,`, SourceCoordinates("/dummy.dys", 1, 30), 29, 30),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 1, 31), 30, 31),
        Identifier.make(`andMe`, SourceCoordinates("/dummy.dys", 1, 32), 31, 36),
        SymbolColon.make(`:`, SourceCoordinates("/dummy.dys", 1, 37), 36, 37),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 1, 38), 37, 38),
        Identifier.make(`Int`, SourceCoordinates("/dummy.dys", 1, 39), 38, 41),
        SymbolParenRight.make(`)`, SourceCoordinates("/dummy.dys", 1, 42), 41, 42),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 1, 43), 42, 43),
        SymbolBraceLeft.make(`{`, SourceCoordinates("/dummy.dys", 1, 44), 43, 44),
        VSpace.make("\n", SourceCoordinates("/dummy.dys", 1, 45), 44, 45),
        HSpace.make(`    `, SourceCoordinates("/dummy.dys", 2, 1), 45, 49),
        SymbolSemicolon.make(`;`, SourceCoordinates("/dummy.dys", 2, 5), 49, 50),
        HSpace.make(` `, SourceCoordinates("/dummy.dys", 2, 6), 50, 51),
        CommentPlaintext.make(`a work in progress`, SourceCoordinates("/dummy.dys", 2, 7), 51, 69),
        VSpace.make("\n", SourceCoordinates("/dummy.dys", 2, 25), 69, 70),
        HSpace.make(`    `, SourceCoordinates("/dummy.dys", 3, 1), 70, 74),
        KeywordReturn.make(`return`, SourceCoordinates("/dummy.dys", 3, 5), 74, 80),
        VSpace.make("\n", SourceCoordinates("/dummy.dys", 3, 11), 80, 81),
        SymbolBraceRight.make(`}`, SourceCoordinates("/dummy.dys", 4, 1), 81, 82),
        EndOfInput.make("", SourceCoordinates("/dummy.dys", 4, 2), 82, 82),
    ]);
}
