module dysnomia.phases.static_analysis.type_check_test;

import ast = dysnomia.data.ast;
import dysnomia.data.source_coordinates;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import exceeds_expectations;
import tk = dysnomia.data.tokens;
import dysnomia.data.types;


@("Type check an integer")
unittest
{
    ast.AtomicExpression input = new ast.AtomicExpression(
        null,
        tk.Integer("123", SourceCoordinates("/dummy.dys", 1, 1), 0, 3),
    );

    expect({ typeCheck(input, new TypeScope(["root"], null), null, null); }).not.toThrow();
    expect(input.type).toEqual(typeInt);
}

@("Type check a string")
unittest
{
    ast.AtomicExpression input = new ast.AtomicExpression(
        null,
        new ast.String(
            null,
            tk.StringPlaintext("hello world", SourceCoordinates("/dummy.dys", 1, 1), 0, 11),
        ),
    );

    expect({ typeCheck(input, new TypeScope(["root"], null), null, null); }).not.toThrow();
    expect(input.type).toEqual(typeString);
}

@("Type check a variable in the nearest scope")
unittest
{
    ast.AtomicExpression input = new ast.AtomicExpression(
        null,
        tk.Identifier("pi", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
    );

    TypeScope rootTypeScope = new TypeScope(["root"], null);
    rootTypeScope["pi"] = typeInt;

    expect({ typeCheck(input, rootTypeScope, null, null); }).not.toThrow();
    expect(input.type).toEqual(typeInt);
}

@("Type check a variable in a parent scope")
unittest
{
    ast.AtomicExpression input = new ast.AtomicExpression(
        null,
        tk.Identifier("pi", SourceCoordinates("/dummy.dys", 1, 1), 0, 2),
    );

    TypeScope rootTypeScope = new TypeScope(["root"], null);
    TypeScope localTypeScope = new TypeScope(["local"], rootTypeScope);

    rootTypeScope["pi"] = typeInt;

    expect({ typeCheck(input, localTypeScope, null, null); }).not.toThrow();
    expect(input.type).toEqual(typeInt);
}
