module dysnomia.phases.static_analysis.type_scope;

import dysnomia.data.types;


package class TypeScope
{
    struct GetResult
    {
        Type type;
        TypeScope ownerTypeScope;
    }

    private string[] _scopeName;
    private TypeScope _parentScope;
    private Type[string] _symbolTypes;

    this(string[] scopeName, TypeScope parentScope)
    {
        this._scopeName = scopeName;
        this._parentScope = parentScope;
    }

    public string[] scopeName() => _scopeName.dup;

    public GetResult opIndex(string symbolName)
    {
        if (symbolName in _symbolTypes)
        {
            return GetResult(_symbolTypes[symbolName], this);
        }
        else if (_parentScope !is null)
        {
            return _parentScope[symbolName];
        }

        return GetResult.init;
    }

    public void opIndexAssign(Type symbolType, string symbolName)
    {
        _symbolTypes[symbolName] = symbolType;
    }
}
