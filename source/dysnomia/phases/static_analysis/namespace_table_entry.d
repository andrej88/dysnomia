module dysnomia.phases.static_analysis.namespace_table_entry;

import ast = dysnomia.data.ast;
import dysnomia.phases.static_analysis.type_scope;


package struct NamespaceTableEntry
{
    string[] namespaceName;
    ast.Namespace namespace;
    TypeScope namespaceTypeScope;
}
