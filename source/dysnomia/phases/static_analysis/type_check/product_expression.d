module dysnomia.phases.static_analysis.type_check.product_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.algorithm;
import std.range;


package void typeCheckImpl(
    ast.ProductExpression productExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(productExpression.head, typeScope, namespaceScopes, typeOfCurrentFunction);

    foreach (ast.ProductExpressionSegment tailSegment; productExpression.tail)
    {
        typeCheck(tailSegment.operand, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    if (productExpression.operands.map!(operand => operand.type).uniq().walkLength != 1)
    {
        throw new MismatchedOperandTypesException(
            "product expression",
            productExpression.operands,
            productExpression.sourceCoordinates,
        );
    }

    productExpression.type = productExpression.head.type;
}
