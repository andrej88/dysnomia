module dysnomia.phases.static_analysis.type_check.null_coalescing_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.types;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.algorithm;
import std.array;


package void typeCheckImpl(
    ast.NullCoalescingExpression nullCoalescingExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(nullCoalescingExpression.head, typeScope, namespaceScopes, typeOfCurrentFunction);

    foreach (ast.NullCoalescingExpressionSegment tailSegment; nullCoalescingExpression.tail)
    {
        typeCheck(tailSegment.operand, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    Type[] nonNullHeadTypes =
        nullCoalescingExpression.operands[0 .. $-1].map!(
            operand =>
                cast(NullableType) operand.type !is null ?
                (cast(NullableType) operand.type).payloadType :
                operand.type
        ).array;

    nullCoalescingExpression.type =
        new UnionType(
            (nonNullHeadTypes ~ nullCoalescingExpression.operands[$ - 1].type),
        ).normalized();
}
