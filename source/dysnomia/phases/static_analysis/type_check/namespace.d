module dysnomia.phases.static_analysis.type_check.namespace;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.util.table;
import std.algorithm;
import std.array;
import tk = dysnomia.data.tokens;


package void typeCheckImpl(
    ast.Namespace astNamespace,
    Table!NamespaceTableEntry namespaceScopes,
)
{
    string[] namespaceName =
        astNamespace
            .namespaceDeclaration
            .namespaceName
            .map!((tk.Identifier id) => id.source)
            .array;

    NamespaceTableEntry[] namespaceDataCandidates = namespaceScopes.findAll(
        (NamespaceTableEntry entry) => entry.namespaceName == namespaceName
    );

    assert(
        namespaceDataCandidates.length != 0,
        "Expected to find a namespace named " ~ namespaceName.join(".") ~ " but didn't.",
    );

    if (namespaceDataCandidates.length > 1)
    {
        throw new ReusedNamespaceNameException(
            namespaceName,
            namespaceDataCandidates.map!(e => e.namespace.namespaceDeclaration.sourceCoordinates).array,
            namespaceDataCandidates[0].namespace.namespaceDeclaration.sourceCoordinates,
        );
    }

    typeCheck(
        namespaceDataCandidates[0].namespace.namespaceBody,
        namespaceDataCandidates[0].namespaceTypeScope,
        namespaceScopes,
    );
}
