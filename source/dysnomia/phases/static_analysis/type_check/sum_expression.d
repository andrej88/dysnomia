module dysnomia.phases.static_analysis.type_check.sum_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.algorithm;
import std.range;


package void typeCheckImpl(
    ast.SumExpression sumExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(sumExpression.head, typeScope, namespaceScopes, typeOfCurrentFunction);

    foreach (ast.SumExpressionSegment tailSegment; sumExpression.tail)
    {
        typeCheck(tailSegment.operand, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    if (sumExpression.operands.map!(operand => operand.type).uniq().walkLength != 1)
    {
        throw new MismatchedOperandTypesException(
            "sum expression",
            sumExpression.operands,
            sumExpression.sourceCoordinates,
        );
    }

    sumExpression.type = sumExpression.head.type;
}
