module dysnomia.phases.static_analysis.type_check.expression;

import ast = dysnomia.data.ast;
import dysnomia.data.types;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.sumtype;


package void typeCheckImpl(
    ast.Expression astExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    astExpression.alternations.match!(
        (e) => typeCheck(e, typeScope, namespaceScopes, typeOfCurrentFunction),
    );
}
