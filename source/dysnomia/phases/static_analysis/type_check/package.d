module dysnomia.phases.static_analysis.type_check;

import ast = dysnomia.data.ast;
import dysnomia.data.types;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check.atomic_expression;
import dysnomia.phases.static_analysis.type_check.comparison_expression;
import dysnomia.phases.static_analysis.type_check.expression;
import dysnomia.phases.static_analysis.type_check.function_declaration;
import dysnomia.phases.static_analysis.type_check.if_else_expression;
import dysnomia.phases.static_analysis.type_check.logical_expression;
import dysnomia.phases.static_analysis.type_check.namespace;
import dysnomia.phases.static_analysis.type_check.namespace_body;
import dysnomia.phases.static_analysis.type_check.null_coalescing_expression;
import dysnomia.phases.static_analysis.type_check.product_expression;
import dysnomia.phases.static_analysis.type_check.return_expression;
import dysnomia.phases.static_analysis.type_check.statement;
import dysnomia.phases.static_analysis.type_check.sum_expression;
import dysnomia.phases.static_analysis.type_check.typed_variable_definition;
import dysnomia.phases.static_analysis.type_check.unary_expression;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.sumtype;
import dysnomia.util.table;
import std.algorithm;
import std.array;
import std.sumtype;
import tk = dysnomia.data.tokens;


// Following the same structure as in to_ast. The difference is that there are
// multiple overloads for the typeCheck/Impl function. The Namespace[] override
// is special.

public void typeCheck(ast.Namespace[] namespaces)
{
    Table!NamespaceTableEntry namespaceScopes = new Table!NamespaceTableEntry();

    populateNamespaceTable(namespaces, namespaceScopes);
    populateNamespaceTypeScopes(namespaceScopes);

    foreach (ast.Namespace namespace; namespaces)
    {
        typeCheck(namespace, namespaceScopes);
    }
}

private void populateNamespaceTable(ast.Namespace[] namespaces, Table!NamespaceTableEntry namespaceScopes)
in (namespaceScopes !is null)
{
    // TODO: What if there are multiple namespaces with the same name?
    foreach(ast.Namespace astNamespace; namespaces)
    {
        string[] namespaceName =
            astNamespace
                .namespaceDeclaration
                .namespaceName
                .map!((tk.Identifier id) => id.source)
                .array;

        TypeScope namespaceTypeScope = new TypeScope(
            namespaceName,
            null,
        );

        namespaceScopes.insert(NamespaceTableEntry(namespaceName, astNamespace, namespaceTypeScope));
    }
}

private void populateNamespaceTypeScopes(Table!NamespaceTableEntry namespaceScopes)
{
    foreach (NamespaceTableEntry entry; namespaceScopes)
    {
        auto fnDecls =
            entry
                .namespace
                .namespaceBody
                .namespaceItems
                .filterByType!(ast.FunctionDeclaration);

        foreach (ast.FunctionDeclaration fnDecl; fnDecls)
        {
            Type returnType =
                fnDecl.returnTypeExpression.isNull ?
                typeVoid :
                resolveType(fnDecl.returnTypeExpression.get, entry.namespaceTypeScope);

            Type[] parameterTypes =
                fnDecl
                .parameters
                .map!(param => param.typeExpression)
                .map!(typeExp => resolveType(typeExp, entry.namespaceTypeScope))
                .array;

            FunctionType typeOfCurrentFunction = new FunctionType(returnType, parameterTypes);

            entry.namespaceTypeScope[fnDecl.name.source] = typeOfCurrentFunction;
        }
    }
}

package void typeCheck(Ast)(
    Ast ast,
    Table!NamespaceTableEntry namespaceScopes,
)
{
    typeCheckImpl(ast, namespaceScopes);
}


package void typeCheck(Ast)(
    Ast ast,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
)
{
    typeCheckImpl(
        ast,
        typeScope,
        namespaceScopes,
    );
}

package void typeCheck(Ast)(
    Ast ast,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheckImpl(
        ast,
        typeScope,
        namespaceScopes,
        typeOfCurrentFunction,
    );
}

package Type resolveType(
    ast.TypeExpression type,
    TypeScope typeScope,
)
{
    Type atomicType = resolveType(type.payload, typeScope);

    if (type.postfix.isNull)
    {
        return atomicType;
    }

    return type.postfix.get.postfixOperator.alternations.match!(
        (tk.SymbolQuestionMark _) => new NullableType(atomicType),
    );
}

package Type resolveType(
    ast.AtomicTypeExpression type,
    TypeScope typeScope,
)
{
    return type.alternations.match!(
        (tk.Identifier id) {
            switch (id.source)
            {
                case "Void":
                    return typeVoid;

                case "Bool":
                    return typeBool;

                case "Int":
                    return typeInt;

                case "String":
                    return typeString;

                case "Never":
                    return typeNever;

                case "Any":
                    return typeAny;

                default:
                    assert(false, "Unhandled type: " ~ id.source);
            }
        },
    );
}
