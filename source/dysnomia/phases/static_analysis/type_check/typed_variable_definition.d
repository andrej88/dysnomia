module dysnomia.phases.static_analysis.type_check.typed_variable_definition;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.conv;


package void typeCheckImpl(
    ast.TypedVariableDefinition astTypedVariableDefinition,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    // typecheck right hand side
    typeCheck(astTypedVariableDefinition.initializer, typeScope, namespaceScopes, typeOfCurrentFunction);

    // get type of each side
    Type initializerType = astTypedVariableDefinition.initializer.type;

    astTypedVariableDefinition.declaredType = resolveType(
        astTypedVariableDefinition.typeExpression,
        typeScope,
    );

    // compare
    if (astTypedVariableDefinition.declaredType.canReceive(initializerType)) {
        typeScope[astTypedVariableDefinition.name.source] = astTypedVariableDefinition.declaredType;
        return;
    }

    throw new TypeCheckException(
        astTypedVariableDefinition.name.source,
        astTypedVariableDefinition.declaredType.to!string,
        initializerType.to!string,
        astTypedVariableDefinition.initializer.sourceCoordinates,
    );
}
