module dysnomia.phases.static_analysis.type_check.return_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;


package void typeCheckImpl(
    ast.ReturnExpression astReturnStatement,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    if (astReturnStatement.payload.isNull)
    {
        if (typeOfCurrentFunction.returnType != typeVoid)
        {
            throw new FunctionReturnTypeException(
                typeOfCurrentFunction.returnType,
                typeVoid,
                astReturnStatement.sourceCoordinates,
            );
        }
    }
    else
    {
        typeCheck(astReturnStatement.payload.get, typeScope, namespaceScopes, typeOfCurrentFunction);

        if (!typeOfCurrentFunction.returnType.canReceive(astReturnStatement.payload.get.type))
        {
            throw new FunctionReturnTypeException(
                typeOfCurrentFunction.returnType,
                astReturnStatement.payload.get.type,
                astReturnStatement.payload.get.sourceCoordinates,
            );
        }
    }
}
