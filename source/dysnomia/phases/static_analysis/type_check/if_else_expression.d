module dysnomia.phases.static_analysis.type_check.if_else_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.sumtype;
import dysnomia.util.table;


package void typeCheckImpl(
    ast.IfElseExpression ifExp,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(ifExp.condition, typeScope, namespaceScopes, typeOfCurrentFunction);

    if (ifExp.condition.type != typeBool) {
        throw new NonBooleanIfConditionException(
            ifExp.condition.type,
            ifExp.condition.sourceCoordinates,
        );
    }

    // This would be a cool place to allow something like `if let x: Int = 3 { ... }`, could create a new scope with x in it.
    foreach (ast.Statement astStatement; ifExp.thenBlock)
    {
        typeCheck(astStatement, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    if (!ifExp.tail.isNull)
    {
        foreach (ast.Statement astStatement; ifExp.tail.get.elseBlock)
        {
            typeCheck(astStatement, typeScope, namespaceScopes, typeOfCurrentFunction);
        }

        // TODO: Check that both blocks end with Expressions.
        ast.Expression thenExp = ifExp.thenBlock[$-1].alternations.get!(ast.Expression);
        ast.Expression elseExp = ifExp.tail.get.elseBlock[$-1].alternations.get!(ast.Expression);

        if (thenExp.type != elseExp.type) {
            throw new MismatchedIfElseBlockTypesException(
                thenExp,
                elseExp,
                ifExp.sourceCoordinates,
            );
        }
    }

    ifExp.type = (ifExp.thenBlock[$-1].alternations).get!(ast.Expression).type;
}
