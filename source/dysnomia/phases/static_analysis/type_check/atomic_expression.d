module dysnomia.phases.static_analysis.type_check.atomic_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.algorithm;
import std.array;
import std.sumtype;
import tk = dysnomia.data.tokens;


package void typeCheckImpl(
    ast.AtomicExpression atomicExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    atomicExpression.alternations.match!(
        (ast.Expression e) {
            typeCheck(e, typeScope, namespaceScopes, typeOfCurrentFunction);
            atomicExpression.type = e.type;
        },
        (tk.Boolean b) { atomicExpression.type = typeBool; },
        (tk.Integer t) { atomicExpression.type = typeInt; },
        (ast.String s) { atomicExpression.type = typeString; },
        (tk.Null n) { atomicExpression.type = typeNull; },
        (ast.IfElseExpression ifExp) {
            typeCheck(ifExp, typeScope, namespaceScopes, typeOfCurrentFunction);
            atomicExpression.type = ifExp.type;
        },
        (ast.ReturnExpression r) {
            atomicExpression.type = typeNever;
            typeCheck(r, typeScope, namespaceScopes, typeOfCurrentFunction);
        },
        (tk.Identifier i) {
            Type symbolType = typeScope[i.source].type;

            if (symbolType is null)
            {
                // The symbol is not in this type scope, but it may be the name of another namespace.

                NamespaceTableEntry[] candidateNamespaces =
                    namespaceScopes.findAll(
                        (NamespaceTableEntry entry) => entry.namespaceName.endsWith(i.source),
                    );

                if (candidateNamespaces.length == 1)
                {
                    atomicExpression.type = new NamespaceType(candidateNamespaces[0].namespaceName);
                    return;
                }
                else if (candidateNamespaces.length > 1)
                {
                    // TODO: Test this
                    throw new AmbiguousImportException(
                        i.source,
                        candidateNamespaces.map!(entry => entry.namespaceName).array,
                        i.sourceCoordinates,
                    );
                }
                else
                {
                    // No namespace was found
                    throw new UndeclaredIdentifierException(i.source, i.sourceCoordinates);
                }
            }
            else
            {
                atomicExpression.type = symbolType;
            }
        },
    );
}
