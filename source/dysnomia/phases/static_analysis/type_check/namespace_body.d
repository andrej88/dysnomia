module dysnomia.phases.static_analysis.type_check.namespace_body;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.sumtype;
import dysnomia.util.table;
import std.algorithm;
import std.array;
import tk = dysnomia.data.tokens;


package void typeCheckImpl(
    ast.NamespaceBody astNamespaceBody,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
)
{
    auto importDecls = astNamespaceBody.namespaceItems.filterByType!(ast.ImportDeclaration);

    Table!NamespaceTableEntry filteredScopes = new Table!NamespaceTableEntry();

    foreach (ast.ImportDeclaration importDecl; importDecls)
    {
        string[] importedNamespaceName = importDecl.namespaceName.map!((tk.Identifier id) => id.source).array;

        bool namespaceExists = namespaceScopes.contains(
            (NamespaceTableEntry entry) => entry.namespaceName == importedNamespaceName
        );

        if (!namespaceExists)
        {
            throw new UndeclaredNamespaceException(
                importedNamespaceName,
                importDecl.namespaceName[0].sourceCoordinates,
            );
        }

        NamespaceTableEntry namespaceToAdd = namespaceScopes.findOne(
            (NamespaceTableEntry entry) => entry.namespaceName == importedNamespaceName
        );

        filteredScopes.insert(
            NamespaceTableEntry(
                namespaceToAdd.namespaceName,
                namespaceToAdd.namespace,
                namespaceToAdd.namespaceTypeScope,
            )
        );
    }

    auto fnDecls = astNamespaceBody.namespaceItems.filterByType!(ast.FunctionDeclaration);

    foreach (ast.FunctionDeclaration fnDecl; fnDecls)
    {
        typeCheck(fnDecl, typeScope, filteredScopes);
    }
}
