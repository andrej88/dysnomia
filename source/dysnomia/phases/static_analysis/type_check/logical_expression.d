module dysnomia.phases.static_analysis.type_check.logical_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.algorithm;
import std.range;


package void typeCheckImpl(
    ast.LogicalExpression logicalExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(logicalExpression.head, typeScope, namespaceScopes, typeOfCurrentFunction);

    foreach (ast.LogicalExpressionSegment tailSegment; logicalExpression.tail)
    {
        typeCheck(tailSegment.operand, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    if (logicalExpression.operands.map!(operand => operand.type).uniq().walkLength != 1)
    {
        throw new MismatchedOperandTypesException(
            "logical expression",
            logicalExpression.operands,
            logicalExpression.sourceCoordinates,
        );
    }

    logicalExpression.type = typeBool;
}
