module dysnomia.phases.static_analysis.type_check.comparison_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.range;


package void typeCheckImpl(
    ast.ComparisonExpression comparisonExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(comparisonExpression.head, typeScope, namespaceScopes, typeOfCurrentFunction);

    foreach (ast.ComparisonExpressionSegment tailSegment; comparisonExpression.tail)
    {
        typeCheck(tailSegment.operand, typeScope, namespaceScopes, typeOfCurrentFunction);
    }

    auto operandPairs = slide(comparisonExpression.operands, 2);

    foreach (ast.UnaryExpression[] operandPair; operandPairs)
    {
        // Comparison rules are different than assignment rules. Two expression
        // A and B can be compared if any of the following are true:
        // - A is a subtype of B
        // - B is a subtype of A
        // - Either A or B have the type Never?
        //
        // This allows for code like the following:
        //
        // if 3 == null {
        //     ; ...
        // }
        //
        // This is a bit weird, but who's to say the type of the integer literal
        // 3 is `Int` rather than `Int?`? Instead of considering all possible
        // types an expression may have, we solve it by adding a special case
        // here essentially allowing anything to be compared against null, even
        // if the other side of the comparison is non-nullable. This seems to
        // match Kotlin's behavior so at least there's that.

        bool canCompare =
            operandPair[0].type.canReceive(operandPair[1].type) ||
            operandPair[1].type.canReceive(operandPair[0].type) ||
            operandPair[0].type == typeNull ||
            operandPair[1].type == typeNull;

        if (!canCompare)
        {
            throw new MismatchedOperandTypesException(
                "comparison expression",
                comparisonExpression.operands,
                operandPair[0].sourceCoordinates,
            );
        }
    }

    comparisonExpression.type = typeBool;
}
