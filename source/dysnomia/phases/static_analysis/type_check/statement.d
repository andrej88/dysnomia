module dysnomia.phases.static_analysis.type_check.statement;

import ast = dysnomia.data.ast;
import dysnomia.data.types;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.sumtype;


package void typeCheckImpl(
    ast.Statement astStatement,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    astStatement.alternations.match!(
        (s) => typeCheck(s, typeScope, namespaceScopes, typeOfCurrentFunction),
    );
}
