module dysnomia.phases.static_analysis.type_check.unary_expression;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.conv;
import std.sumtype;


package void typeCheckImpl(
    ast.UnaryExpression astUnaryExpression,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
    FunctionType typeOfCurrentFunction,
)
{
    typeCheck(astUnaryExpression.operand, typeScope, namespaceScopes, typeOfCurrentFunction);

    if (astUnaryExpression.postfixOperators.length == 0)
    {
        astUnaryExpression.type = astUnaryExpression.operand.type;
        return;
    }

    // TODO: This would definitely be more sensible as a recursive structure rather than an array...

    Type typeSoFar = astUnaryExpression.operand.type;

    foreach (ast.PostfixOperator postfixOp; astUnaryExpression.postfixOperators)
    {
        postfixOp.alternations.match!(
            (ast.ArgumentBlock args) {
                FunctionType functionType = cast(FunctionType) typeSoFar;
                if (functionType is null)
                {
                    // TODO: This won't show the right coordinates if it's not the first postfix operator
                    throw new FunctionCallTypeException(
                        typeSoFar.to!string,
                        astUnaryExpression.operand.sourceCoordinates,
                    );
                }

                if (functionType.parameterTypes.length != args.arguments.length)
                {
                    // TODO: This won't show the right coordinates if it's not the first postfix operator
                    throw new FunctionArgumentCountException(
                        args.arguments.length.to!int,
                        functionType.parameterTypes,
                        astUnaryExpression.operand.sourceCoordinates,
                    );
                }

                foreach (size_t i, Type parameterType; functionType.parameterTypes)
                {
                    ast.Expression argument = args.arguments[i];
                    typeCheck(argument, typeScope, namespaceScopes, typeOfCurrentFunction);

                    if (!parameterType.canReceive(argument.type))
                    {
                        throw new FunctionArgumentTypeException(
                            argument.type,
                            functionType.parameterTypes,
                            argument.sourceCoordinates,
                        );
                    }
                }

                typeSoFar = functionType.returnType;
            },
            (ast.MemberAccess ma) {
                if (cast(NamespaceType)(typeSoFar) !is null)
                {
                    NamespaceType operandType = cast(NamespaceType)(typeSoFar);
                    NamespaceTableEntry parentNamespace = namespaceScopes.findAll(entry => entry.namespaceName == operandType.qualifiedName)[0];
                    typeSoFar = parentNamespace.namespaceTypeScope[ma.identifier.source].type;
                    if (typeSoFar is null)
                    {
                        throw new UndeclaredIdentifierException(
                            ma.identifier.source,
                            ma.identifier.sourceCoordinates,
                        );
                    }
                }
                else
                {
                    assert(false, "Not yet implemented: type checking members of things that aren't namespaces");
                }
            },
        );
    }

    astUnaryExpression.type = typeSoFar;
}
