module dysnomia.phases.static_analysis.type_check.function_declaration;

import ast = dysnomia.data.ast;
import dysnomia.data.common;
import dysnomia.data.control_flow;
import dysnomia.data.types;
import dysnomia.error.static_analysis;
import dysnomia.phases.static_analysis.namespace_table_entry;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.static_analysis.type_scope;
import dysnomia.util.table;
import std.sumtype;
import tk = dysnomia.data.tokens;


package void typeCheckImpl(
    ast.FunctionDeclaration astFunctionDeclaration,
    TypeScope typeScope,
    Table!NamespaceTableEntry namespaceScopes,
)
{
    FunctionType typeOfCurrentFunction = cast(FunctionType) typeScope[astFunctionDeclaration.name.source].type;
    assert(typeOfCurrentFunction !is null, "Type of current function is not a FunctionType");

    TypeScope functionTypeScope = new TypeScope([astFunctionDeclaration.name.source], typeScope);

    foreach (ast.Parameter parameter; astFunctionDeclaration.parameters)
    {
        functionTypeScope[parameter.name.source] = resolveType(parameter.typeExpression, functionTypeScope);
    }

    astFunctionDeclaration.functionBody.alternations.match!(
        (ast.Statement[] statements) {
            foreach (ast.Statement astStatement; statements)
            {
                typeCheck(astStatement, functionTypeScope, namespaceScopes, typeOfCurrentFunction);
            }

            if (typeOfCurrentFunction.returnType != typeVoid)
            {
                ast.Statement lastStatement = statements[$ - 1];

                if (lastStatement.controlFlow != ControlFlow.returns)
                {
                    throw new FunctionMissingReturnException(
                        typeOfCurrentFunction.returnType,
                        astFunctionDeclaration.sourceCoordinates,
                    );
                }
            }
        },
        (tk.KeywordIntrinsic ki) {
            string[] namespaceOfCurrentFunction = typeScope[astFunctionDeclaration.name.source].ownerTypeScope.scopeName;

            if (namespaceOfCurrentFunction[0] != "Dys")
            {
                throw new IllegalIntrinsicFunctionException(ki.sourceCoordinates);
            }
        },
    );
}
