module dysnomia.phases.to_bytecode_test;

import ast = dysnomia.data.ast;
import dysnomia.data.bytecode;
import dysnomia.phases.to_bytecode;
import exceeds_expectations;
import std.conv : to;


ast.Namespace[] sourceToAst(string[] sources...)
{
    import cst = dysnomia.data.cst;
    import dysnomia.phases.lex : lex;
    import dysnomia.phases.parse : parse;
    import dysnomia.phases.static_analysis.type_check;
    import dysnomia.phases.static_analysis.type_scope;
    import dysnomia.phases.to_ast : toAst;
    import std.algorithm.iteration : joiner, map;
    import std.array : array;

    ast.Namespace[] astNs =
        sources
        .map!(
            source =>
                source
                    .lex("/dummy.dys")
                    .parse!(cst.File)
                    .toAst
        )
        .joiner
        .array;

    typeCheck(astNs);

    return astNs;
}

@("Compiles an empty namespace")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Nothing.To.See.Here
`
);

    expect(toBc(namespaces)).toEqual([
        BcObject(["Nothing", "To", "See", "Here"], []),
    ]);
}

@("Compiles a file with an empty function")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() {}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles negative integers")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() Int {
    return -3 - 8
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcIntConst(3).to!BcInstr,
                    BcIntNegate().to!BcInstr,
                    BcIntConst(8).to!BcInstr,
                    BcIntSub().to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a function that returns an int")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() Int {
    return 3
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcIntConst(3).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a function that does some addition and subtraction")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() {
    1 + 2
    88 + 77 - 66
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcIntConst(1).to!BcInstr,
                    BcIntConst(2).to!BcInstr,
                    BcIntAdd().to!BcInstr,
                    BcIntConst(88).to!BcInstr,
                    BcIntConst(77).to!BcInstr,
                    BcIntAdd().to!BcInstr,
                    BcIntConst(66).to!BcInstr,
                    BcIntSub().to!BcInstr,
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a function that does some multiplication")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() {
    32 * 3
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcIntConst(32).to!BcInstr,
                    BcIntConst(3).to!BcInstr,
                    BcIntMultiply().to!BcInstr,
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles nullable variable declarations")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() {
    let x: Int = 3
    let y: Int? = 4
    let z: Int? = null
    let y2: Int? = y
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcIntConst(3).to!BcInstr,
                    BcSetInFrame(0).to!BcInstr,

                    BcIntConst(4).to!BcInstr,
                    BcWrapWithNullable().to!BcInstr,
                    BcSetInFrame(1).to!BcInstr,

                    BcNullConst().to!BcInstr,
                    BcWrapWithNullable().to!BcInstr,
                    BcSetInFrame(2).to!BcInstr,

                    BcGetInFrame(1).to!BcInstr,
                    BcWrapWithNullable().to!BcInstr,
                    BcSetInFrame(3).to!BcInstr,

                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a function that calls another")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

fn foo() {
    bar()
}

fn bar() {}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcGetInObject(0, 1).to!BcInstr,
                    BcFnCall(0).to!BcInstr,
                    BcReturn(false).to!BcInstr,
                ]),
                BcFunc([
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles the identity function")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Tautology

fn identity(x: Int) Int {
    return x
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Tautology"],
            [
                BcFunc([
                    BcGetInFrame(0).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a function that calls an identity function")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Stuff

fn foo() Int {
    return identity(59)
}

fn identity(x: Int) Int {
    return x
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Stuff"],
            [
                BcFunc([
                    BcGetInObject(0, 1).to!BcInstr,
                    BcIntConst(59).to!BcInstr,
                    BcFnCall(1).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
                BcFunc([
                    BcGetInFrame(0).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles an if/else expression")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace IfElse

fn foo() {
    let x: Int = if 2 == 3 {
        20
    } else {
        10
    }
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["IfElse"],
            [
                BcFunc([
                    BcIntConst(2).to!BcInstr,
                    BcIntConst(3).to!BcInstr,
                    BcEqual().to!BcInstr,
                    BcJumpIfFalse(2).to!BcInstr,    // 2 + the usual increment of 1
                    BcIntConst(20).to!BcInstr,
                    BcJump(1).to!BcInstr,           // 1 + the usual increment of 1
                    BcIntConst(10).to!BcInstr,
                    BcSetInFrame(0).to!BcInstr,
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles a file with a long namespace declaration")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Some.Namespace.Name

fn foo() {}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["Some", "Namespace", "Name"],
            [
                BcFunc([
                    BcReturn(false).to!BcInstr,
                ]),
            ]
        ),
    ]);
}

@("Compiles two namespaces")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace Foo

import Bar

fn foo() Int {
    return Bar.bar()
}
`,
`namespace Bar

fn bar() Int {
    return 65
}
`
    );

    BcObject[] result = toBc(namespaces);

    expect(result).toEqual([
        BcObject(
            ["Foo"],
            [
                BcFunc([
                    BcObjectIdConst(1).to!BcInstr,
                    BcGetInObjectFromStack(0).to!BcInstr,
                    BcFnCall(0).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ],
        ),
        BcObject(
            ["Bar"],
            [
                BcFunc([
                    BcIntConst(65).to!BcInstr,
                    BcReturn(true).to!BcInstr,
                ]),
            ],
        ),
    ]);
}

@("Compiles the null coalescing operator `??`")
unittest
{
    ast.Namespace[] namespaces = sourceToAst(
`namespace NullCoalescing

fn foo() {
    let a: Int? = 1
    let b: Int? = null
    let c: Int = a ?? b ?? 3
}
`);

    expect(toBc(namespaces)).toEqual([
        BcObject(
            ["NullCoalescing"],
            [
                BcFunc([
                    // Set up a and b
                    BcIntConst(1).to!BcInstr,
                    BcWrapWithNullable().to!BcInstr,
                    BcSetInFrame(0).to!BcInstr,
                    BcNullConst().to!BcInstr,
                    BcWrapWithNullable().to!BcInstr,
                    BcSetInFrame(1).to!BcInstr,

                    // Check if a is non-null
                    BcGetInFrame(0).to!BcInstr,    // One for the return
                    BcDuplicateTip().to!BcInstr,   // And one for the comparison
                    BcNullConst().to!BcInstr,
                    BcEqual().to!BcInstr,
                    BcJumpIfFalse(6).to!BcInstr,

                    // Check if b is non-null
                    BcGetInFrame(1).to!BcInstr,    // One for the return
                    BcDuplicateTip().to!BcInstr,   // And one for the comparison
                    BcNullConst().to!BcInstr,
                    BcEqual().to!BcInstr,
                    BcJumpIfFalse(1).to!BcInstr,

                    // Return 3 by default
                    BcIntConst(3).to!BcInstr,

                    // Unwrap whatever the result was
                    BcUnwrapNullable().to!BcInstr,

                    // Assign to c
                    BcSetInFrame(2).to!BcInstr(),

                    BcReturn(false).to!BcInstr,
                ]),
            ],
        ),
    ]);
}
