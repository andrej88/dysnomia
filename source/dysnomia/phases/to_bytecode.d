module dysnomia.phases.to_bytecode;

import ast = dysnomia.data.ast;
import dysnomia.data.bytecode;
import dysnomia.data.intrinsics;
import dysnomia.data.types;
import dysnomia.phases.to_bytecode;
import dysnomia.util.sumtype;
import dysnomia.util.table;
import std.algorithm;
import std.array;
import std.conv : to;
import std.sumtype;
import tk = dysnomia.data.tokens;


private class BcScope
{
    enum Type
    {
        fn,
        file,
    }

    struct GetResult
    {
        Type type;
        BcScope owner;
        size_t index;
    }

    Type type;
    BcScope parent;
    size_t[string] names;

    this(Type type, BcScope parent = null)
    {
        this.type = type;
        this.parent = parent;
    }

    bool isInScope(string name)
    {
        return name in names || (parent !is null && parent.isInScope(name));
    }

    GetResult get(string name)
    {
        if (name in names)
            return GetResult(type, this, names[name]);

        if (parent !is null)
            return parent.get(name);

        assert(false, "Symbol \"" ~ name ~ "\" not found in scope.");
    }

    GetResult put(string name)
    {
        if (name in names)
            assert(false, "Symbol \"" ~ name ~ "\" is being re-declared.");

        names[name] = names.length;

        return GetResult(type, this, names[name]);
    }
}

private struct ScopeData
{
    size_t index;
    string[] namespaceName;
    BcScope bcScope;
}


BcObject[] toBc(ast.Namespace[] namespaces)
{
    Table!ScopeData namespaceScopes = new Table!ScopeData();

    populateNamespaceTable(namespaces, namespaceScopes);

    return
        namespaces
        .map!(ns => toBc(ns, namespaceScopes))
        .array;
}

private void populateNamespaceTable(ast.Namespace[] namespaces, Table!ScopeData namespaceScopes)
{
    foreach (ast.Namespace namespace; namespaces)
    {
        BcScope objScope = new BcScope(BcScope.Type.file, null);

        string[] namespaceName =
            namespace.namespaceDeclaration.namespaceName
            .map!((tk.Identifier id) => id.source)
            .array;

        foreach (ast.NamespaceItem namespaceItem; namespace.namespaceBody.namespaceItems)
        {
            namespaceItem.alternations.match!(
                (ast.FunctionDeclaration fd) {
                    objScope.put(fd.name.source);
                },
                (ast.ImportDeclaration importDecl) {/* pass */},
            );
        }

        namespaceScopes.insert(ScopeData(namespaceScopes.count(), namespaceName, objScope));
    }
}

private BcObject toBc(ast.Namespace namespace, Table!ScopeData namespaceScopes)
{
    string[] namespaceName =
        namespace.namespaceDeclaration.namespaceName
        .map!((tk.Identifier id) => id.source)
        .array;

    ScopeData thisScopeDataEntry = namespaceScopes.findOne((ScopeData entry) => entry.namespaceName == namespaceName);
    BcScope objScope = thisScopeDataEntry.bcScope;

    Table!ScopeData filteredScopes = new Table!ScopeData();

    filteredScopes.insert(thisScopeDataEntry);

    foreach (ast.NamespaceItem namespaceItem; namespace.namespaceBody.namespaceItems)
    {
        namespaceItem.alternations.match!(
            (ast.FunctionDeclaration fn) {/* pass */},
            (ast.ImportDeclaration importDecl) {
                string[] importedNamespaceName = importDecl.namespaceName.map!((tk.Identifier id) => id.source).array;

                filteredScopes.insert(
                    namespaceScopes.findOne(
                        (ScopeData entry) => entry.namespaceName == importedNamespaceName,
                    )
                );
            },
        );
    }

    BcObject result;
    result.namespace = namespaceName;

    foreach (ast.NamespaceItem namespaceItem; namespace.namespaceBody.namespaceItems)
    {
        namespaceItem.alternations.match!(
            (ast.FunctionDeclaration fn) {
                result.funcs ~= toBc(fn, objScope, filteredScopes);
            },
            (ast.ImportDeclaration importDecl) {/* pass */},
        );
    }

    return result;
}

private BcFunc toBc(ast.FunctionDeclaration fn, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcFunc result;
    BcScope fnScope = new BcScope(BcScope.Type.fn, scop);

    foreach (ast.Parameter param; fn.parameters)
    {
        fnScope.put(param.name.source);
    }

    fn.functionBody.alternations.match!(
        (ast.Statement[] statements) {
            foreach (ast.Statement stmt; statements)
            {
                result.instrs ~= toBc(stmt, fnScope, namespaceScopes);
            }
        },
        (tk.KeywordIntrinsic _) {

            foreach (ast.Parameter param; fn.parameters)
            {
                result.instrs ~= BcGetInFrame(fnScope.get(param.name.source).index).to!BcInstr;
            }

            result.instrs ~= BcCallFnIntrinsic(fn.parameters.length, getIntrinsicFunctionId(fn.name.source)).to!BcInstr;
        },
    );

    if (
        result.instrs.length == 0 ||
        !result.instrs[$-1].has!BcReturn
    )
    {
        // TODO: This assumes that the function's return type is Void. Add an
        //       explicit check.
        result.instrs ~= BcReturn().to!BcInstr;
    }

    return result;
}

private BcInstr[] toBc(ast.Statement stmt, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    stmt.alternations.match!(
        (ast.TypedVariableDefinition var) {
            // This doesn't allow for variables holding recursive functions,
            // but does allow shadowing a variable and using the about-to-be-shadowed
            // version in the initializer.
            result ~= toBc(var.initializer, scop, namespaceScopes);

            if ((cast(NullableType) var.declaredType) !is null)
            {
                result ~= BcWrapWithNullable().to!BcInstr;
            }

            BcScope.GetResult idIdx = scop.put(var.name.source);
            result ~= BcSetInFrame(idIdx.index).to!BcInstr;
        },
        (ast.Expression expr) {
            result ~= toBc(expr, scop, namespaceScopes);
        },
    );

    return result;
}

private BcInstr[] toBc(ast.Expression expr, BcScope scop, Table!ScopeData namespaceScopes)
{
    return expr.alternations.match!(
        (e) => toBc(e, scop, namespaceScopes)
    );
}

private BcInstr[] toBc(ast.UnaryExpression unaryExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    result ~= toBc(unaryExpr.operand, scop, namespaceScopes);

    if (!unaryExpr.prefixOperator.isNull)
    {
        result ~= unaryExpr.prefixOperator.get.alternations.match!(
            (tk.SymbolMinus _) => BcIntNegate().to!BcInstr,
        );
    }

    foreach (ast.PostfixOperator postfixOp; unaryExpr.postfixOperators)
    {
        result ~= postfixOp.alternations.match!(
            (ast.ArgumentBlock argBlock) =>
                argBlock.arguments.map!((ast.Expression arg) => toBc(arg, scop, namespaceScopes)).join ~
                BcFnCall(argBlock.arguments.length).to!BcInstr,

            (ast.MemberAccess memberAccess) {
                ScopeData parentNamespace = namespaceScopes.findOne(
                    (ScopeData entry) => entry.index == result[$-1].get!BcObjectIdConst.value
                );

                BcScope.GetResult symbolData = parentNamespace.bcScope.get(memberAccess.identifier.source);
                return [
                    BcGetInObjectFromStack(symbolData.index).to!BcInstr,
                ];
            },
        );
    }

    return result;
}

private BcInstr[] toBc(ast.AtomicExpression atomicExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    atomicExpr.alternations.match!(
        (ast.Expression e) {
            result ~= toBc(e, scop, namespaceScopes);
        },
        (tk.Boolean b) {
            result ~= BcBoolConst(b.source.to!bool).to!BcInstr;
        },
        (tk.Integer i) {
            result ~= BcIntConst(i.source.to!long).to!BcInstr;
        },
        (ast.String s) {
            result ~= BcStringConst(s.str.source).to!BcInstr;
        },
        (tk.Null n) {
            result ~= BcNullConst().to!BcInstr;
        },
        (ast.IfElseExpression ifExp) {
            BcInstr[] thenInstrs = ifExp.thenBlock.map!(statement => toBc(statement, scop, namespaceScopes)).join;

            result ~= toBc(ifExp.condition, scop, namespaceScopes);
            result ~= BcJumpIfFalse(thenInstrs.length + 1).to!BcInstr;
            result ~= thenInstrs;

            if (!ifExp.tail.isNull)     // if else, else
            {
                BcInstr[] elseInstrs = ifExp.tail.get.elseBlock.map!(statement => toBc(statement, scop, namespaceScopes)).join;
                result ~= BcJump(elseInstrs.length).to!BcInstr;
                result ~= elseInstrs;
            }
            else                        // else, no else
            {
                // If there's no explicit else block, behave as if the else block were `else { null }`.
                result ~= BcJump(1).to!BcInstr;
                result ~= BcNullConst().to!BcInstr;
            }
        },
        (ast.ReturnExpression ret) {
            if (!ret.payload.isNull)
            {
                result ~= toBc(ret.payload.get, scop, namespaceScopes);
                result ~= BcReturn(true).to!BcInstr;
            }
            else
            {
                result ~= BcReturn(false).to!BcInstr;
            }
        },
        (tk.Identifier id) {
            result ~= toBc(id, scop, namespaceScopes);
        },
    );

    return result;
}

private BcInstr[] toBc(tk.Identifier id, BcScope scop, Table!ScopeData namespaceScopes)
{
    if (scop.isInScope(id.source))
    {
        BcScope.GetResult idIdx = scop.get(id.source);

        final switch (idIdx.type) {
            case BcScope.Type.file:
                size_t thisScopeIdx = namespaceScopes.findOne(it => it.bcScope is idIdx.owner).index;
                return [
                    BcGetInObject(thisScopeIdx, idIdx.index).to!BcInstr,
                ];

            case BcScope.Type.fn:
                return [
                    BcGetInFrame(idIdx.index).to!BcInstr,
                ];
        }
    }

    // If it's not in this scope, check if it's the name of another namespace.
    ScopeData[] candidateScopes =
        namespaceScopes.findAll(
            (ScopeData entry) => entry.namespaceName.endsWith(id.source),
        );

    assert(
        candidateScopes.length == 1,
        "Did not find exactly one namespace named " ~ id.source ~ " in to_bytecode. Static analysis should have detected this.",
    );

    return [
        BcObjectIdConst(candidateScopes[0].index).to!BcInstr,
    ];
}

private BcInstr[] toBc(ast.SumExpression sumExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    result ~= toBc(sumExpr.head, scop, namespaceScopes);

    foreach (ast.SumExpressionSegment tailSegment; sumExpr.tail)
    {
        result ~= toBc(tailSegment.operand, scop, namespaceScopes);
        result ~= tailSegment.operator.alternations.match!(
            (tk.SymbolPlus _plus) => BcIntAdd().to!BcInstr,
            (tk.SymbolMinus _minus) => BcIntSub().to!BcInstr,
        );
    }

    return result;
}

private BcInstr[] toBc(ast.ProductExpression productExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    result ~= toBc(productExpr.head, scop, namespaceScopes);

    foreach (ast.ProductExpressionSegment tailSegment; productExpr.tail)
    {
        result ~= toBc(tailSegment.operand, scop, namespaceScopes);
        result ~= tailSegment.operator.alternations.match!(
            (tk.SymbolAsterisk _asterisk) => BcIntMultiply().to!BcInstr,
        );
    }

    return result;
}

private BcInstr[] toBc(ast.ComparisonExpression comparisonExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    result ~= toBc(comparisonExpr.head, scop, namespaceScopes);

    foreach (ast.ComparisonExpressionSegment tailSegment; comparisonExpr.tail)
    {
        result ~= toBc(tailSegment.operand, scop, namespaceScopes);
        result ~= tailSegment.operator.alternations.match!(
            (tk.SymbolDoubleEquals _) => BcEqual().to!BcInstr,
        );
    }

    return result;
}

private BcInstr[] toBc(ast.LogicalExpression logicalExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    BcInstr[] result;

    result ~= toBc(logicalExpr.head, scop, namespaceScopes);

    // TODO: Implement short-circuiting when it becomes necessary
    foreach (ast.LogicalExpressionSegment tailSegment; logicalExpr.tail)
    {
        result ~= toBc(tailSegment.operand, scop, namespaceScopes);
        result ~= tailSegment.operator.alternations.match!(
            (tk.SymbolAmpersand _) => BcAnd().to!BcInstr,
        );
    }

    return result;
}

private BcInstr[] toBc(ast.NullCoalescingExpression nullCoalescingExpr, BcScope scop, Table!ScopeData namespaceScopes)
{
    // TODO: Maybe there is a way to take advantage of this:
    // `a ?? b` is just syntactic sugar for `if a != null { a } else { b }`
    // ^ If evaluating `a` has side effects, it's not the same.
    // The result of `a` needs to be stored somewhere beforehand.

    BcInstr[] result;

    ast.UnaryExpression[] operands = nullCoalescingExpr.operands;
    BcInstr[][] operandsBytecode = operands.map!(operand => toBc(operand, scop, namespaceScopes)).array;

    foreach (ptrdiff_t idx, BcInstr[] operandBytecode; operandsBytecode[0 .. $ - 1])
    {
        // e.g. for the array [a, b, c, d],
        // c's index is 3 and its indexFromBackExcludingTail is 0.
        // d's indexFromBackExcludingTail will never be calculated since
        // this loop skips the last element.

        ptrdiff_t indexFromBackExcludingTail = (operands.length.to!ptrdiff_t - idx - 2);

        // (4 instructions per remaining left-hand operand)
        // + (the number of instructions to actually evaluate all remaining operands including the last)
        // - 1 because the VM always increments the program counter by 1 so there is no need
        ptrdiff_t instructionsRemaining =
            (indexFromBackExcludingTail * 4) +
            operandsBytecode[idx .. $].fold!((ptrdiff_t acc, BcInstr[] e) => acc + e.length)(0.to!ptrdiff_t)
            - 1;

        result ~= operandBytecode;
        result ~= BcDuplicateTip().to!BcInstr,
        result ~= BcNullConst().to!BcInstr;
        result ~= BcEqual().to!BcInstr;
        result ~= BcJumpIfFalse(instructionsRemaining).to!BcInstr;
    }

    result ~= operandsBytecode[$ - 1];
    result ~= BcUnwrapNullable().to!BcInstr;

    return result;
}
