module dysnomia.phases.lex;

import dysnomia.data.source_coordinates;
import dysnomia.error.lex;
import exceeds_expectations;
import std.algorithm : count, countUntil;
import std.conv;
import std.range : retro, walkLength;
import std.regex;
import std.sumtype;
import std.uni : byCodePoint;
import tk = dysnomia.data.tokens;


/// Converts a string into a list of tokens.
///
/// Params:
///   input = the source code to lex.
///   inputFilePath = the path to include in each token's metadata.
///                   This path will never be read by this function.
///                   It may be an absolute path, a relative path, or
///                   not a path at all.
///
/// Throws:
///     [InvalidSourceException] if unexpected symbols are encountered.
///
tk.Token[] lex(string input, string inputPath)
out (result; result[$ - 1].match!(
    (tk.EndOfInput _) => true, // Last token is always EndOfInput
    (_) => false,
))
{
    tk.Token[] result;
    size_t charsConsumed = 0;
    string remainingInput = input;
    int line = 1;
    int column = 1;

    bool consumeToken(T, string regex)()
    {
        Captures!string c = matchFirst(remainingInput, ctRegex!("^(" ~ regex ~ ")"));

        // Captures may be empty and the length 0 if the given pattern
        // was something like a "zero-or-more" type of thing that
        // succeeds on an empty input.
        if (!c.empty && c.hit.length > 0)
        {
            result ~= T.make(c.hit, SourceCoordinates(inputPath, line, column), charsConsumed, charsConsumed + c.hit.length);

            charsConsumed += c.hit.length;
            line += c.hit.count('\n');
            remainingInput = remainingInput[c.hit.length .. $];

            string consumedInput = input[0 .. charsConsumed];
            column = consumedInput.retro.byCodePoint.countUntil('\n').to!int;
            if (column == -1) column = consumedInput.byCodePoint.walkLength.to!int;
            column++;

            return true;
        }

        return false;
    }

    while (charsConsumed < input.length)
    {
        if (consumeToken!(tk.SymbolBraceLeft,          `\{`                                                              )) continue;
        if (consumeToken!(tk.SymbolBraceRight,         `\}`                                                              )) continue;
        if (consumeToken!(tk.SymbolColon,              `:`                                                               )) continue;
        if (consumeToken!(tk.SymbolComma,              `,`                                                               )) continue;
        if (consumeToken!(tk.SymbolDoubleEquals,       `==`                                                              )) continue;
        if (consumeToken!(tk.SymbolEquals,             `=`                                                               )) continue;
        if (consumeToken!(tk.SymbolParenLeft,          `\(`                                                              )) continue;
        if (consumeToken!(tk.SymbolParenRight,         `\)`                                                              )) continue;
        if (consumeToken!(tk.SymbolPlus,               `\+`                                                              )) continue;
        if (consumeToken!(tk.SymbolMinus,              `-`                                                               )) continue;
        if (consumeToken!(tk.SymbolAsterisk,           `\*`                                                              )) continue;
        if (consumeToken!(tk.SymbolDot,                `\.`                                                              )) continue;
        if (consumeToken!(tk.SymbolAmpersand,          `&`                                                               )) continue;
        if (consumeToken!(tk.SymbolDoubleQuestionMark, `\?\?`                                                            )) continue;
        if (consumeToken!(tk.SymbolQuestionMark,       `\?`                                                              )) continue;

        if (consumeToken!(tk.SymbolSemicolon,          `;`                                                               ))
        {
            consumeToken!(tk.HSpace, `[ \t]*`);
            consumeToken!(tk.CommentPlaintext, `[^\n]*`);
            continue;
        }

        if (consumeToken!(tk.SymbolQuotes,             `"`                                                               ))
        {
            consumeToken!(tk.StringPlaintext, `[^"]*`);
            consumeToken!(tk.SymbolQuotes, `"`);
            continue;
        }

        if (consumeToken!(tk.Boolean,                  `(true|false)\b`                                                  )) continue;
        if (consumeToken!(tk.Null,                     `(null)\b`                                                        )) continue;
        if (consumeToken!(tk.KeywordFn,                `fn\b`                                                            )) continue;
        if (consumeToken!(tk.KeywordIf,                `if\b`                                                            )) continue;
        if (consumeToken!(tk.KeywordElse,              `else\b`                                                          )) continue;
        if (consumeToken!(tk.KeywordLet,               `let\b`                                                           )) continue;
        if (consumeToken!(tk.KeywordReturn,            `return\b`                                                        )) continue;
        if (consumeToken!(tk.KeywordNamespace,         `namespace\b`                                                     )) continue;
        if (consumeToken!(tk.KeywordImport,            `import\b`                                                        )) continue;
        if (consumeToken!(tk.KeywordIntrinsic,         `intrinsic\b`                                                     )) continue;

        if (consumeToken!(tk.HSpace,                   `[ \t]+`                                                          )) continue;
        if (consumeToken!(tk.VSpace,                   `\n([ \t]*\n)*`                                                   )) continue;

        if (consumeToken!(tk.Identifier,               `\p{Alphabetic}((\p{Alphabetic}|[0-9]|_)*(\p{Alphabetic}|[0-9]))?`)) continue;
        if (consumeToken!(tk.Integer,                  `(0|[1-9](_?[0-9])*)\b`                                           )) continue;

        string offendingText = remainingInput.matchFirst(`.+?\b`).hit;
        throw new InvalidSourceException(offendingText, SourceCoordinates(inputPath, line, column));
    }

    return result ~ tk.EndOfInput.make("", SourceCoordinates(inputPath, line, column), charsConsumed, charsConsumed);
}
