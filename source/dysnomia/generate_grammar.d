module dysnomia.generate_grammar;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.data.control_flow;
import log = dysnomia.log;
import std.algorithm;
import std.array;
import std.conv;
import std.file;
import std.path;
import std.range;
import std.regex;
import std.traits;
import std.typecons;
import tk = dysnomia.data.tokens;


int main(string[] args)
{
    if (args.length < 3)
    {
        log.errorGeneric(
            "generate-grammar configuration expects to receive the type (cst or ast)" ~
            " as its first argument and destination path as its second argument."
        );
        return 1;
    }

    string grammarType = args[1];
    string destination = asAbsolutePath(args[2]).to!string;

    switch (grammarType)
    {
        case "cst":
            printGrammar!cst(destination);
            break;

        case "ast":
            printGrammar!ast(destination);
            break;

        default:
            log.errorGeneric("Unknown grammar type \"" ~ grammarType ~ "\". Expected \"cst\" or \"ast\".");
            break;
    }

    log.info("Grammar printed to: " ~ destination);

    return 0;
}

private void printGrammar(alias modul)(string destination)
{
    struct Line
    {
        size_t ruleNameLength;
        string contents;
    }

    Line[] lines;
    size_t longestRuleName;

    static foreach (string member; __traits(allMembers, modul))
    {{
        string line;

        if (member.length > longestRuleName)
            longestRuleName = member.length;

        static if (is(__traits(getMember, modul, member) == class))
        {
            alias Rule = __traits(getMember, modul, member);
            line ~= member;
            line ~= "  =  ";

            static if (modul.isConcatenationNode!Rule)
            {
                alias fieldNames = Rule.childrenFieldNames;

                static foreach (size_t idx, term; Rule.ChildrenFieldTypes)
                {{
                    line ~= toAbnf!(modul, term);
                    if (idx + 1 < Rule.ChildrenFieldTypes.length)
                        line ~= " ";
                }}
            }

            else static if (modul.isAlternationNode!Rule)
            {
                static foreach (size_t idx, term; Rule.alternations.Types)
                {
                    line ~= toAbnf!(modul, term);
                    if (idx + 1 < Rule.alternations.Types.length)
                        line ~= " / ";
                }
            }

            else static if (cst.isSeparatedRepetitionNode!Rule)
            {
                line ~=
                    toAbnf!(modul, Rule.ElementType) ~
                    " (" ~ toAbnf!(modul, Rule.SeparatorType) ~ " " ~ toAbnf!(modul, Rule.ElementType) ~ ")*";
            }

            lines ~= Line(member.length, line);
        }
    }}

    string[] indentedLines = lines.map!(
        line => repeat(' ', longestRuleName - line.ruleNameLength).to!string ~ line.contents
    ).array;

    string[] sectionedLines;
    foreach (size_t idx, string line; indentedLines)
    {
        bool tryAddSectionHeading(string beforeRuleName)(string headingLabel)
        {
            if (line.matchFirst(ctRegex!(`^\s+` ~ beforeRuleName ~ `\b`)).length == 0)
                return false;

            string label = headingLabel;
            string labelFull = (' '.repeat(longestRuleName - label.length - 2).array ~ "; " ~ label).to!string;

            if (idx != 0)
            {
                sectionedLines ~= "";
                sectionedLines ~= "";
            }

            sectionedLines ~= labelFull;
            sectionedLines ~= "";

            return true;
        }

        bool tryAddSubsectionHeading(string beforeRuleName)()
        {
            if (line.matchFirst(ctRegex!(`^\s+` ~ beforeRuleName ~ `\b`)).length == 0)
                return false;

            sectionedLines ~= "";

            return true;
        }

        bool _ =
            tryAddSectionHeading!"File"("Top Level") ||
            tryAddSectionHeading!"Block"("Statements") ||
            tryAddSectionHeading!"Expression"("Expressions") ||
            tryAddSectionHeading!"AnySpace"("Spacing and Comments") ||
            tryAddSectionHeading!"TypeExpression"("Types") ||
            tryAddSubsectionHeading!"NamespaceDeclaration" ||
            tryAddSubsectionHeading!"FunctionDeclaration" ||
            tryAddSubsectionHeading!"ReturnExpression" ||
            tryAddSubsectionHeading!"UnaryExpression" ||
            tryAddSubsectionHeading!"AtomicExpression" ||
            tryAddSubsectionHeading!"PolyadicExpressionTail" ||
            tryAddSubsectionHeading!"SumExpressionTail" ||
            tryAddSubsectionHeading!"ArgumentBlock";

        sectionedLines ~= line;
    }

    string buffer = sectionedLines.join('\n') ~ "\n";

    write(destination, buffer);
}

private enum string toAbnf(alias modul, Type) = () {
    static if (modul.isConcatenationNode!Type || modul.isAlternationNode!Type || cst.isSeparatedRepetitionNode!Type)
        return Type.stringof;
    else static if (isDynamicArray!Type)
        return "*" ~ toAbnf!(modul, ElementType!Type);
    else static if (is(Type : Nullable!Payload, Payload))
        return "[" ~ toAbnf!(modul, Payload) ~ "]";
    else static if (tk.isAToken!Type)
        return "tk." ~ Type.stringof;
    else
        static assert(false, "Invalid grammar node type: " ~ Type.stringof);
}();
