module dysnomia.error.static_analysis;

import ast = dysnomia.data.ast;
import colorize;
import dysnomia.data.common;
import dysnomia.data.source_coordinates;
import dysnomia.data.types;
import dysnomia.error.common;
import std.algorithm;
import std.array;
import std.conv;
import std.range;


public class UndeclaredIdentifierException : ProblemInSourceException
{
    this(
        string identifier,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    pure @safe
    {
        super(
            "Cannot find identifier " ~ identifier.color(mode.bold) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class UndeclaredNamespaceException : ProblemInSourceException
{
    this(
        string[] namespaceName,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    pure @safe
    {
        super(
            "Cannot find namespace " ~ namespaceName.join(".").color(mode.bold) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class TypeCheckException : ProblemInSourceException
{
    this(
        string variableName,
        string declaredType,
        string receivedType,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    pure @safe
    {
        super(
            "The declared type of variable " ~ variableName.color(mode.bold) ~
            " is " ~  declaredType.color(mode.bold) ~
            " but received " ~ receivedType.color(mode.bold) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class FunctionCallTypeException : ProblemInSourceException
{
    this(
        string receivedType,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    ) {
        super(
            "Cannot call expression of type " ~ receivedType.color(mode.bold) ~
            " as a function.",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class FunctionArgumentTypeException : ProblemInSourceException
{
    this(
        Type firstWrongType,
        Type[] parameters,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    ) {
        string functionParameters = "(" ~ parameters.map!(it => it.to!string).join(", ") ~ ")";

        super(
            "Cannot pass " ~ firstWrongType.to!string.color(mode.bold) ~
            " as the first argument to a function with parameters " ~ functionParameters.color(mode.bold) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class FunctionArgumentCountException : ProblemInSourceException
{
    this(
        int numberOfArguments,
        Type[] parameters,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    ) {
        string argString;

        if (numberOfArguments == 1)
        {
            if (parameters.length == 0)
            {
                argString = "an argument";
            }
            else
            {
                argString = "1".color(mode.bold) ~ " argument";
            }
        }
        else
        {
            argString = numberOfArguments.to!string.color(mode.bold) ~ " arguments";
        }

        string parameterString;
        if (parameters.length == 0)
        {
            parameterString = "no parameters";
        }
        else
        {
            string parameterTupleString = "(" ~ parameters.map!(it => it.to!string).join(", ") ~ ")";
            parameterString =
                parameters.length.to!string.color(mode.bold) ~ " parameters " ~
                parameterTupleString.color(mode.bold);
        }

        super(
            "Cannot pass " ~ argString ~ " to a function with " ~ parameterString ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class IllegalIntrinsicFunctionException : ProblemInSourceException
{
    this(
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        super(
            "Only standard library functions may have an " ~ "intrinsic".color(mode.bold) ~ " function body.",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class AmbiguousImportException : ProblemInSourceException
{
    this(
        string identifierName,
        string[][] candidateNamespaceNames,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    ) {
        string namespaceList =
            candidateNamespaceNames
            .map!((string[] namespaceNameParts) => namespaceNameParts.join("."))
            .map!((string namespaceName) => "- " ~ namespaceName)
            .join("\n");

        super(
            "Identifier " ~ identifierName.color(mode.bold) ~ " is defined in multiple namespaces:\n" ~ namespaceList,
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class ReusedNamespaceNameException : ProblemInSourceException
{
    this(
        string[] namespaceName,
        SourceCoordinates[] allSources,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        string message =
            "Detected multiple namespaces named " ~ namespaceName.join(".").color(mode.bold) ~ ":\n" ~
            allSources.map!(e => "- " ~ e.to!string).join("\n");


        super(
            message,
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class FunctionReturnTypeException : ProblemInSourceException
{
    this(
        Type declaredReturnType,
        Type actualReturnType,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        super(
            "The function signature declared a return type of " ~ declaredReturnType.to!string.color(mode.bold) ~
            " but this statement returns " ~ (
                actualReturnType == typeVoid ?
                "no value" :
                "a value of type " ~ actualReturnType.to!string.color(mode.bold)
            ) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class FunctionMissingReturnException : ProblemInSourceException
{
    this(
        Type declaredReturnType,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        super(
            "The function signature declared a return type of " ~ declaredReturnType.to!string.color(mode.bold) ~
            " but the function's final statement does not always return.",
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class MismatchedOperandTypesException : ProblemInSourceException
{
    this(
        string expressionType,
        ast.UnaryExpression[] operands,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        string message =
            "Incompatible operand types for " ~ expressionType ~ ":\n" ~
            operands
                .map!(
                    (ast.UnaryExpression operand) =>
                        "- " ~ operand.source.color(mode.bold) ~
                        " is of type " ~ operand.type.to!string.color(mode.bold)
                    )
                .join("\n");

        super(
            message,
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class NonBooleanIfConditionException : ProblemInSourceException
{
    this(
        Type actualType,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        string message =
            "The type of an " ~ "if".color(mode.bold) ~ "'s condition expression must be " ~
            "Bool".color(mode.bold) ~ " but received " ~ actualType.to!string.color(mode.bold) ~ ".";

        super(
            message,
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}

public class MismatchedIfElseBlockTypesException : ProblemInSourceException
{
    this(
        ast.Expression thenExp,
        ast.Expression elseExp,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        string message =
            "The blocks of the if/else expression do not evaluate to the same type:\n" ~
            "- The first block ends with " ~ thenExp.source.color(mode.bold) ~ " of type " ~ thenExp.type.to!string.color(mode.bold) ~ "\n" ~
            "- The second block ends with " ~ elseExp.source.color(mode.bold) ~ " of type " ~ elseExp.type.to!string.color(mode.bold);

        super(
            message,
            sourceCoordinates,
            file,
            line,
            nextInChain,
        );
    }
}
