module dysnomia.error.lex;

import colorize;
import dysnomia.data.source_coordinates;
import dysnomia.error.common;


class InvalidSourceException : ProblemInSourceException
{
    this(
        string offendingTextExcerpt,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null
    )
    pure @safe
    {
        super(
            "Cannot understand the text " ~ offendingTextExcerpt.color(mode.bold) ~ ".",
            sourceCoordinates,
            file,
            line,
            nextInChain
        );
    }
}
