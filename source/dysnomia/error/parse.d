module dysnomia.error.parse;

import colorize;
import cst = dysnomia.data.cst;
import dysnomia.data.common;
import dysnomia.data.source_coordinates;
import dysnomia.error.common;
import dysnomia.util.algorithm;
import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.typecons;
import tk = dysnomia.data.tokens;


public class MultiParseException : Exception
{
    ParseException[] exceptions;

    this(
        ParseException[] exceptions,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null
    )
    pure nothrow @nogc @safe
    {
        this.exceptions = exceptions;
        super("Received multiple ParseExceptions", file, line, nextInChain);
    }

    override string message() const @safe pure nothrow
    {
        return this.exceptions
            .map!(it => "- " ~ it.msg)
            .join("\n");
    }
}

public abstract class ParseException : ProblemInSourceException
{
    int tokensConsumed;
    TypeInfo[] ruleStackTrace;

    private this(
        SourceCoordinates sourceCoordinates,
        int tokensConsumed,
        TypeInfo[] ruleStackTrace,
        string msg,
        string file = __FILE__,
        int line = __LINE__,
        Throwable nextInChain = null,
    )
    pure nothrow @nogc @safe
    {
        this.tokensConsumed = tokensConsumed;
        this.ruleStackTrace = ruleStackTrace;

        super(msg, sourceCoordinates, file, line, nextInChain);
    }

    public abstract ParseException[] mostSuccessfulCauses();
}

class FailingRuleException : ParseException
{
    ParseException[] potentialCauses;

    this(
        SourceCoordinates sourceCoordinates,
        int tokensConsumed,
        TypeInfo[] ruleStackTrace,
        ParseException[] potentialCauses,
        string file = __FILE__,
        int line = __LINE__,
        Throwable nextInChain = null,
    )
    @safe
    {
        this.potentialCauses = potentialCauses;

        super(
            sourceCoordinates,
            tokensConsumed,
            ruleStackTrace,
            "Failed to parse rule " ~ ruleStackTrace[$ - 1].to!string ~
            " at " ~ sourceCoordinates.line.to!string ~ ":" ~ sourceCoordinates.column.to!string,
            file,
            line,
            nextInChain
        );
    }

    private ParseException[] mostSuccessfulCausesCache;

    override ParseException[] mostSuccessfulCauses()
    out(
        result;

        result.length != 0 &&
        result.all!(e => e.sourceCoordinates == result[0].sourceCoordinates) &&
        result.all!(e => e.tokensConsumed == result[0].tokensConsumed),

        "MultiParseException.mostSuccessfullCauses() did not not satisfy the post-condition.\n" ~
        "sourceCoordinates: " ~ result.map!(e => e.sourceCoordinates).array.to!string ~ "\n" ~
        "tokensConsumed: " ~ result.map!(e => e.tokensConsumed).array.to!string ~ "\n" ~
        "Return value: " ~ result.to!string,
    )
    {
        if (mostSuccessfulCausesCache !is null)
        {
            return mostSuccessfulCausesCache;
        }

        mostSuccessfulCausesCache = potentialCauses
            .filter!((pe) {
                if (cast(FailingRuleException) pe)
                {
                    // If the rule stack ends with one of these sequences, it's probably a false positive
                    enum TypeInfo[][] probableFalsePositives = [
                        cast(TypeInfo[]) [typeid(Nullable!(tk.HSpace))],
                        cast(TypeInfo[]) [typeid(Nullable!(cst.Comment))],
                        cast(TypeInfo[]) [typeid(cst.AnySpace), typeid(cst.BlankLines)],
                    ];
                    FailingRuleException fre = (cast(FailingRuleException) pe);

                    bool match = probableFalsePositives.canFind!(
                        (TypeInfo[] falsePositive, TypeInfo[] stackTrace) =>
                            stackTrace.endsWith(falsePositive)
                    )(fre.ruleStackTrace);
                    return !match;
                }
                else
                    return true;
            })
            .map!(pe => pe.mostSuccessfulCauses())
            .joiner
            .maxElements!((ParseException pe) => pe.tokensConsumed);

        return mostSuccessfulCausesCache;
    }
}

class WrongTokenException : ParseException
{
    tk.Token received;

    invariant {
        import std.string : startsWith;
        assert(
            ruleStackTrace[$ - 1].to!string.startsWith("dysnomia.data.tokens."),
            "WrongTokenException received " ~ ruleStackTrace[$ - 1].to!string ~ " as the expected \"token\"."
        );
    }

    this(
        SourceCoordinates sourceCoordinates,
        int tokensConsumed,
        TypeInfo[] ruleStackTrace,
        tk.Token received,
        string file = __FILE__,
        int line = __LINE__,
        Throwable nextInChain = null,
    )
    {
        this.received = received;

        super(
            sourceCoordinates,
            tokensConsumed,
            ruleStackTrace,
            "Expected " ~ tk.singularName(ruleStackTrace[$ - 1]) ~
            " but received " ~ received.source.color(mode.bold) ~ ".",
            file,
            line,
            nextInChain
        );
    }

    protected override ParseException[] mostSuccessfulCauses()
    {
        return [this];
    }
}
