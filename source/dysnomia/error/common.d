module dysnomia.error.common;

import dysnomia.data.source_coordinates;


public abstract class ProblemInSourceException : Exception
{
    immutable SourceCoordinates sourceCoordinates;

    this(
        string msg,
        SourceCoordinates sourceCoordinates,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null
    )
    pure nothrow @nogc @safe
    {
        this.sourceCoordinates = sourceCoordinates;

        super(msg, file, line, nextInChain);
    }
}
