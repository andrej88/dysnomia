module dysnomia.sdl_representation.ast_to_sdl;

import ast = dysnomia.data.ast;
import dysnomia.sdl_representation.tokens_to_sdl;
import dysnomia.util.debugging;
import sdl = sdlite;
import std.algorithm;
import std.array;
import std.conv;
import std.sumtype;
import std.traits;
import std.typecons;
import tk = dysnomia.data.tokens;


public string astToSdl(Ast)(Ast astNode)
{
    sdl.SDLNode astNodeAsSdl = astToSdlNode(astNode);
    Appender!string dest;
    sdl.generateSDLang(dest, astNodeAsSdl);
    return dest.data.to!string.replace("\t", "    ");
}

private sdl.SDLNode astToSdlNode(Ast)(Ast astNode)
if (ast.isConcatenationNode!Ast)
{
    sdl.SDLNode[] children;

    static foreach (size_t idx, string fieldName; Ast.childrenFieldNames)
    {{

        sdl.SDLNode child = astToSdlNode(__traits(getMember, astNode, fieldName));

        static if (isArray!(Ast.ChildrenFieldTypes[idx]))
        {
            child.qualifiedName = fieldName;
        }
        else
        {
            child.qualifiedName = child.qualifiedName ~ ":" ~ fieldName;
        }

        children ~= child;
    }}

    return sdl.SDLNode(
        "ast." ~ Ast.stringof,
        null,
        null,
        children,
    );
}

private sdl.SDLNode astToSdlNode(Ast)(Ast astNode)
if (ast.isAlternationNode!Ast)
{
    sdl.SDLNode result;

    astNode.alternations.match!(
        (payload) { result = astToSdlNode(payload); },
    );

    return result;
}

private sdl.SDLNode astToSdlNode(Ast)(Ast astNode)
if (tk.isAToken!Ast)
{
    return tokenToSdlNode(tk.Token(astNode));
}

private sdl.SDLNode astToSdlNode(Ast)(Ast astNode)
if (isArray!Ast)
{
    return sdl.SDLNode(
        "array",
        null,
        null,
        astNode.map!astToSdlNode.array,
    );
}

private sdl.SDLNode astToSdlNode(Ast : Nullable!Payload, Payload)(Ast astNode)
{
    if (astNode.isNull)
    {
        static if (tk.isAToken!Payload)
        {
            return sdl.SDLNode(
                "tk." ~ Payload.stringof,
                [sdl.SDLValue(sdl.SDLValue.null_())],
                null,
                null
            );
        }
        else static if (ast.isConcatenationNode!Payload || ast.isAlternationNode!Payload)
        {
            return sdl.SDLNode(
                "ast." ~ Payload.stringof,
                [sdl.SDLValue(sdl.SDLValue.null_())],
                null,
                null
            );
        }
        else
        {
            static assert(false, "Invalid payload type for nullable AST node: " ~ Payload.stringof);
        }
    }
    else
    {
        sdl.SDLNode result = astToSdlNode(astNode.get);
        return result;
    }
}
