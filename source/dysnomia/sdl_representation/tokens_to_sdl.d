module dysnomia.sdl_representation.tokens_to_sdl;

import dysnomia.data.source_coordinates;
import exceeds_expectations;
import sdl = sdlite;
import std.algorithm : map;
import std.conv : to;
import std.range : Appender, ElementType, isInputRange;
import std.sumtype;
import std.traits : isArray;
import tk = dysnomia.data.tokens;


/// Returns a string containing an SDL representation of the given
/// range of tokens.
///
/// See_Also:
///     - [dysnomia.data.tokens]
///     - https://sdlang.org/
public string tokensToSdl(TokenRange)(TokenRange tokens)
if (isInputRange!TokenRange && is(ElementType!TokenRange : const(tk.Token)))
{
    auto nodes = tokens.map!tokenToSdlNode;
    Appender!string dest;
    sdl.generateSDLang(dest, nodes);
    return dest.data.to!string;
}

package sdl.SDLNode tokenToSdlNode(tk.Token token)
{
    sdl.SDLNode result;

    token.match!(
        (token) {
            result.qualifiedName = "tk." ~ typeof(token).stringof;
            result.values = [sdl.SDLValue(token.source)];
            result.attributes = [
                sdl.SDLAttribute("path", token.sourceCoordinates.path.to!(sdl.SDLValue)),
                sdl.SDLAttribute("start", token.start.to!int.to!(sdl.SDLValue)),
                sdl.SDLAttribute("end", token.end.to!int.to!(sdl.SDLValue)),
                sdl.SDLAttribute("line", token.sourceCoordinates.line.to!int.to!(sdl.SDLValue)),
                sdl.SDLAttribute("column", token.sourceCoordinates.column.to!int.to!(sdl.SDLValue)),
            ];
        },
    );

    return result;
}

@("print a token in SDL") unittest
{
    expect(tokensToSdl([
        tk.KeywordReturn.make("return", SourceCoordinates("/dummy.dys", 2, 1), 3, 9)
    ])).toEqual(
`tk.KeywordReturn "return" path="/dummy.dys" start=3 end=9 line=2 column=1
`
    );
}

@("print many tokens in SDL") unittest
{
    expect(tokensToSdl([
        tk.KeywordFn.make(        "fn",                 SourceCoordinates("/dummy.dys", 1,  1),  0,  2),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 1,  3),  2,  3),
        tk.Identifier.make(       "doSomething",        SourceCoordinates("/dummy.dys", 1,  4),  3, 14),
        tk.SymbolParenLeft.make(  "(",                  SourceCoordinates("/dummy.dys", 1, 15), 14, 15),
        tk.Identifier.make(       "withMe",             SourceCoordinates("/dummy.dys", 1, 16), 15, 21),
        tk.SymbolColon.make(      ":",                  SourceCoordinates("/dummy.dys", 1, 22), 21, 22),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 1, 23), 22, 23),
        tk.Identifier.make(       "String",             SourceCoordinates("/dummy.dys", 1, 24), 23, 29),
        tk.SymbolComma.make(      ",",                  SourceCoordinates("/dummy.dys", 1, 30), 29, 30),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 1, 31), 30, 31),
        tk.Identifier.make(       "andMe",              SourceCoordinates("/dummy.dys", 1, 32), 31, 36),
        tk.SymbolColon.make(      ":",                  SourceCoordinates("/dummy.dys", 1, 37), 36, 37),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 1, 38), 37, 38),
        tk.Identifier.make(       "Int",                SourceCoordinates("/dummy.dys", 1, 39), 38, 41),
        tk.SymbolParenRight.make( ")",                  SourceCoordinates("/dummy.dys", 1, 42), 41, 42),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 1, 43), 42, 43),
        tk.SymbolBraceLeft.make(  "{",                  SourceCoordinates("/dummy.dys", 1, 44), 43, 44),
        tk.VSpace.make(           "\n",                 SourceCoordinates("/dummy.dys", 1, 45), 44, 45),
        tk.HSpace.make(           "    ",               SourceCoordinates("/dummy.dys", 2,  1), 45, 49),
        tk.SymbolSemicolon.make(  ";",                  SourceCoordinates("/dummy.dys", 2,  5), 49, 50),
        tk.HSpace.make(           " ",                  SourceCoordinates("/dummy.dys", 2,  6), 50, 51),
        tk.CommentPlaintext.make( "a work in progress", SourceCoordinates("/dummy.dys", 2,  7), 51, 69),
        tk.VSpace.make(           "\n",                 SourceCoordinates("/dummy.dys", 2, 25), 69, 70),
        tk.HSpace.make(           "    ",               SourceCoordinates("/dummy.dys", 3,  1), 70, 74),
        tk.KeywordReturn.make(    "return",             SourceCoordinates("/dummy.dys", 3,  5), 74, 80),
        tk.VSpace.make(           "\n",                 SourceCoordinates("/dummy.dys", 3, 11), 80, 81),
        tk.SymbolBraceRight.make( "}",                  SourceCoordinates("/dummy.dys", 4,  1), 81, 82),
    ])).toEqual(
`tk.KeywordFn "fn" path="/dummy.dys" start=0 end=2 line=1 column=1
tk.HSpace " " path="/dummy.dys" start=2 end=3 line=1 column=3
tk.Identifier "doSomething" path="/dummy.dys" start=3 end=14 line=1 column=4
tk.SymbolParenLeft "(" path="/dummy.dys" start=14 end=15 line=1 column=15
tk.Identifier "withMe" path="/dummy.dys" start=15 end=21 line=1 column=16
tk.SymbolColon ":" path="/dummy.dys" start=21 end=22 line=1 column=22
tk.HSpace " " path="/dummy.dys" start=22 end=23 line=1 column=23
tk.Identifier "String" path="/dummy.dys" start=23 end=29 line=1 column=24
tk.SymbolComma "," path="/dummy.dys" start=29 end=30 line=1 column=30
tk.HSpace " " path="/dummy.dys" start=30 end=31 line=1 column=31
tk.Identifier "andMe" path="/dummy.dys" start=31 end=36 line=1 column=32
tk.SymbolColon ":" path="/dummy.dys" start=36 end=37 line=1 column=37
tk.HSpace " " path="/dummy.dys" start=37 end=38 line=1 column=38
tk.Identifier "Int" path="/dummy.dys" start=38 end=41 line=1 column=39
tk.SymbolParenRight ")" path="/dummy.dys" start=41 end=42 line=1 column=42
tk.HSpace " " path="/dummy.dys" start=42 end=43 line=1 column=43
tk.SymbolBraceLeft "{" path="/dummy.dys" start=43 end=44 line=1 column=44
tk.VSpace "\n" path="/dummy.dys" start=44 end=45 line=1 column=45
tk.HSpace "    " path="/dummy.dys" start=45 end=49 line=2 column=1
tk.SymbolSemicolon ";" path="/dummy.dys" start=49 end=50 line=2 column=5
tk.HSpace " " path="/dummy.dys" start=50 end=51 line=2 column=6
tk.CommentPlaintext "a work in progress" path="/dummy.dys" start=51 end=69 line=2 column=7
tk.VSpace "\n" path="/dummy.dys" start=69 end=70 line=2 column=25
tk.HSpace "    " path="/dummy.dys" start=70 end=74 line=3 column=1
tk.KeywordReturn "return" path="/dummy.dys" start=74 end=80 line=3 column=5
tk.VSpace "\n" path="/dummy.dys" start=80 end=81 line=3 column=11
tk.SymbolBraceRight "}" path="/dummy.dys" start=81 end=82 line=4 column=1
`
    );
}
