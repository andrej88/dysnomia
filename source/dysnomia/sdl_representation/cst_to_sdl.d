module dysnomia.sdl_representation.cst_to_sdl;

import cst = dysnomia.data.cst;
import dysnomia.data.source_coordinates;
import dysnomia.sdl_representation.tokens_to_sdl;
import exceeds_expectations;
import sdl = sdlite;
import std.algorithm : map;
import std.array : Appender, array, replace;
import std.conv : to;
import std.sumtype;
import std.traits : FieldNameTuple, isArray, Unqual;
import std.typecons : nullable, Nullable;
import tk = dysnomia.data.tokens;


/// Returns a string containing an SDL representation of the given
/// concrete syntax tree.
///
/// See_Also:
///     - [dysnomia.data.cst]
///     - https://sdlang.org/
string cstToSdl(Cst)(Cst cstNode)
{
    sdl.SDLNode cstNodeAsSdl = cstToSdlNode(cstNode);
    Appender!string dest;
    sdl.generateSDLang(dest, cstNodeAsSdl);
    return dest.data.to!string.replace("\t", "    ");
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (cst.isConcatenationNode!Cst)
{
    sdl.SDLNode[] children;

    static foreach (string fieldName; FieldNameTuple!Cst)
    {
        children ~= cstToSdlNode(__traits(getMember, cstNode, fieldName));
    }

    return sdl.SDLNode("cst." ~ Cst.stringof, null, null, children);
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (cst.isAlternationNode!Cst)
{
    sdl.SDLNode result;
    cstNode.alternations.match!(
        (payload) { result = cstToSdlNode(payload); },
    );

    return result;
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (cst.isSeparatedRepetitionNode!Cst)
{
    sdl.SDLNode result = sdl.SDLNode("cst." ~ Cst.stringof);

    sdl.SDLNode elements = cstToSdlNode(cstNode.elements);
    elements.qualifiedName = "elements";
    result.children ~= elements;

    sdl.SDLNode separators = cstToSdlNode(cstNode.separators);
    separators.qualifiedName = "separators";
    result.children ~= separators;

    return result;
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (isArray!Cst)
{
    sdl.SDLNode[] children = cstNode.map!cstToSdlNode.array;
    sdl.SDLNode result = sdl.SDLNode("array", null, null, children);

    return result;
}

private sdl.SDLNode cstToSdlNode(Cst : Nullable!Payload, Payload)(Cst cstNode)
{
    static if (tk.isAToken!Payload)
        string name = "tk." ~ Payload.stringof;
    else
        string name = "cst." ~ Payload.stringof;

    if (cstNode.isNull)
    {
        return sdl.SDLNode(
            name,
            [sdl.SDLValue(sdl.SDLValue.null_())],
            null,
            null,
        );
    }
    else
    {
        return cstToSdlNode(cstNode.get);
    }
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (tk.isAToken!Cst)
{
    return tokenToSdlNode(tk.Token(cstNode));
}

private sdl.SDLNode cstToSdlNode(Cst)(Cst cstNode)
if (is(Cst == tk.Token))
{
    return tokenToSdlNode(cstNode);
}


@("print a token node in SDL")
unittest
{
    expect(
        cstToSdl(tk.KeywordReturn("return", SourceCoordinates("/dummy.dys", 1, 4), 3, 9))
    ).toEqual(
`tk.KeywordReturn "return" path="/dummy.dys" start=3 end=9 line=1 column=4
`
    );
}

@("print an array node in SDL")
unittest
{
    tk.Token[] input = [
        tk.KeywordFn.make("fn", SourceCoordinates("/dummy.dys", 2, 1), 8, 10),
        tk.SymbolParenLeft.make("(", SourceCoordinates("/dummy.dys", 2, 3), 10, 11),
    ];

    expect(cstToSdl(input)).toEqual(
`array {
    tk.KeywordFn "fn" path="/dummy.dys" start=8 end=10 line=2 column=1
    tk.SymbolParenLeft "(" path="/dummy.dys" start=10 end=11 line=2 column=3
}
`
    );
}

@("print a null optional node in SDL")
unittest
{
    Nullable!(tk.SymbolBraceLeft) input;
    expect(cstToSdl(input)).toEqual(
`tk.SymbolBraceLeft null
`
    );
}

@("print a non-null optional node in SDL")
unittest
{
    Nullable!(tk.KeywordIf) input = tk.KeywordIf("if", SourceCoordinates("/dummy.dys", 1, 4), 3, 5).nullable;
    expect(cstToSdl(input)).toEqual(
`tk.KeywordIf "if" path="/dummy.dys" start=3 end=5 line=1 column=4
`
    );
}

@("print a concatenation node in SDL")
unittest
{
    cst.Comment input = new cst.Comment(
        tk.SymbolSemicolon(";", SourceCoordinates("/dummy.dys", 1, 4), 3, 4),
        tk.HSpace(" ", SourceCoordinates("/dummy.dys", 1, 5), 4, 5).nullable,
        tk.CommentPlaintext("hi", SourceCoordinates("/dummy.dys", 1, 6), 5, 7).nullable,
    );

    expect(cstToSdl(input)).toEqual(
`cst.Comment {
    tk.SymbolSemicolon ";" path="/dummy.dys" start=3 end=4 line=1 column=4
    tk.HSpace " " path="/dummy.dys" start=4 end=5 line=1 column=5
    tk.CommentPlaintext "hi" path="/dummy.dys" start=5 end=7 line=1 column=6
}
`
    );
}

@("print an alternation node in SDL")
unittest
{
    cst.AtomicExpression input = new cst.AtomicExpression(
        new cst.ReturnExpression(
            tk.KeywordReturn("return", SourceCoordinates("/dummy.dys", 1, 8), 7, 13),
            Nullable!(cst.ReturnExpressionPayload)(),
        )
    );

    expect(cstToSdl(input)).toEqual(
`cst.ReturnExpression {
    tk.KeywordReturn "return" path="/dummy.dys" start=7 end=13 line=1 column=8
    cst.ReturnExpressionPayload null
}
`
    );
}

@("print a separated repetition node in SDL")
unittest
{
    cst.NamespaceIdentifier input = new cst.NamespaceIdentifier(
        [
            tk.Identifier("Foo", SourceCoordinates("/dummy.dys", 1, 11), 10, 13),
            tk.Identifier("Bar", SourceCoordinates("/dummy.dys", 1, 15), 14, 17),
            tk.Identifier("Baz", SourceCoordinates("/dummy.dys", 1, 19), 18, 21),
        ],
        [
            new cst.NamespaceIdentifierSeparator(
                Nullable!(cst.AnySpace)(),
                tk.SymbolDot(".", SourceCoordinates("/dummy.dys", 1, 14), 13, 14),
                Nullable!(cst.AnySpace)(),
            ),
            new cst.NamespaceIdentifierSeparator(
                Nullable!(cst.AnySpace)(),
                tk.SymbolDot(".", SourceCoordinates("/dummy.dys", 1, 18), 17, 18),
                Nullable!(cst.AnySpace)(),
            ),
        ],
    );

    expect(cstToSdl(input)).toEqual(
`cst.NamespaceIdentifier {
    elements {
        tk.Identifier "Foo" path="/dummy.dys" start=10 end=13 line=1 column=11
        tk.Identifier "Bar" path="/dummy.dys" start=14 end=17 line=1 column=15
        tk.Identifier "Baz" path="/dummy.dys" start=18 end=21 line=1 column=19
    }
    separators {
        cst.NamespaceIdentifierSeparator {
            cst.AnySpace null
            tk.SymbolDot "." path="/dummy.dys" start=13 end=14 line=1 column=14
            cst.AnySpace null
        }
        cst.NamespaceIdentifierSeparator {
            cst.AnySpace null
            tk.SymbolDot "." path="/dummy.dys" start=17 end=18 line=1 column=18
            cst.AnySpace null
        }
    }
}
`
    );
}

@("print a null alternation node in SDL")
unittest
{
    Nullable!(cst.Statement) input;
    expect(cstToSdl(input)).toEqual(
`cst.Statement null
`
    );
}
