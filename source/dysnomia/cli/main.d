module dysnomia.cli.main;

import ast = dysnomia.data.ast;
import colorize;
import cst = dysnomia.data.cst;
import dysnomia.cli.options;
import dysnomia.data.bytecode;
import dysnomia.data.common;
import dysnomia.error.common;
import dysnomia.error.lex;
import dysnomia.error.parse;
import dysnomia.phases.lex;
import dysnomia.phases.parse;
import dysnomia.phases.static_analysis.type_check;
import dysnomia.phases.to_ast;
import dysnomia.phases.to_bytecode;
import dysnomia.sdl_representation.ast_to_sdl;
import dysnomia.sdl_representation.cst_to_sdl;
import dysnomia.sdl_representation.tokens_to_sdl;
import dysnomia.util.sumtype;
import dysnomia.util.try_or_else;
import dysnomia.vm.run;
import log = dysnomia.log;
import std.algorithm;
import std.array;
import std.conv : to;
import std.datetime.stopwatch;
import std.file : dirEntries, getcwd, readText, SpanMode, write;
import std.getopt;
import std.path;
import std.path : asRelativePath;
import std.string;
import std.typecons;
import tk = dysnomia.data.tokens;


/// Not really an exception or anything, just my ugly way of working around the
/// lack non-local returns in D.
class Exit : Throwable
{
    int exitCode;

    this(int exitCode) pure nothrow @safe
    {
        super("Requested exit with code " ~ exitCode.to!string);

        this.exitCode = exitCode;
    }

    static Exit opCall(int exitCode) pure nothrow @safe
    {
        return new Exit(exitCode);
    }
}



int main(string[] mainArgs)
// TODO: nothrow. Gotta catch em all
{
    struct Options
    {
        @Description("If specified, writes the list of tokens in SDL format to this file.")
        string debugPrintTokens;

        @Description("If specified, writes the concrete syntax tree in SDL format to this file.")
        string debugPrintCst;

        @Description("If specified, writes the abstract syntax tree in SDL format to this file.")
        string debugPrintAst;

        @Description("If specified, writes the program's runtime in seconds to this file.")
        string debugPrintRuntime;

        @Description("Path to a directory containing the standard library. The directory should contain a sub-directory called 'dys'.")
        string stdlibPath;

        @ShortAlias('h')
        @Description("Print this help text.")
        bool help;
    }


    Options options;

    StopWatch stopwatch = StopWatch(AutoStart.yes);
    scope (exit)
    {
        stopwatch.stop();

        if (options.debugPrintRuntime != "")
        {
            write(options.debugPrintRuntime, (stopwatch.peek.total!"nsecs".to!double / 1_000_000_000.0).to!string);
        }
    }

    string[] inputFiles = parseOptions(mainArgs, options);

    if (options.help || mainArgs.length == 1)
    {
        import std.stdio : writeln;
        writeln("Compiler for the dysnomia programming language.");
        writeln();
        writeln("Usage:");
        writeln();
        writeln("  ", mainArgs[0], " [options] <path to input file>");
        writeln();
        writeln("Options:");
        writeln();
        writeln(getHelpText!(Options, 2));
        return 0;
    }

    if (inputFiles.length == 0)
    {
        log.errorGeneric("Expected at least one file path argument but didn't get any.");
        return 1;
    }

    string stdlibPath = options.stdlibPath;

    if (stdlibPath is null)
    {
        // In the future, it should find the standard library in a standard path
        // (e.g. /usr/lib or something).
        log.errorGeneric(
            "No standard library path given. Add `--stdlib-path=/path/to/standard/library`.\n" ~
            "See --help for more details."
        );

        return 1;
    }

    try
    {
        ast.Namespace[] namespaces;

        string[] stdlibFiles =
            dirEntries(stdlibPath, SpanMode.depth)
            .filter!(e => e.isFile)
            .filter!(e => e.name.extension == ".dys")
            .map!(e => e.name)
            .array;

        string[] allSourceFilePaths = inputFiles ~ stdlibFiles;

        foreach (string filePath; allSourceFilePaths)
        {
            // Lex the source into a list of tokens
            tk.Token[] tokens = tryOrElse(
                lex(
                    readText(filePath),
                    filePath.asRelativePath(getcwd()).to!string,
                ),
                delegate noreturn(InvalidSourceException e) {
                    log.errorInCompile(e.message, e.sourceCoordinates);
                    throw Exit(1);
                },
            );

            if (options.debugPrintTokens != "" && !startsWith(filePath, stdlibPath))
            {
                string sdl = tokensToSdl(tokens);
                write(options.debugPrintTokens, sdl);
            }

            // Parse the tokens into a Concrete Syntax Tree
            cst.File fileCst = tryOrElse(
                parse!(cst.File)(tokens),
                delegate noreturn(MultiParseException e) {
                    log.errorInCompile(
                        e.message,
                        e.exceptions[0].sourceCoordinates,
                    );
                    throw Exit(1);
                },
            );

            if (options.debugPrintCst != "" && !startsWith(filePath, stdlibPath))
            {
                string sdl = cstToSdl(fileCst);
                write(options.debugPrintCst, sdl);
            }

            // Convert the file's CST into multiple Abstract Syntax Trees, one for each namespace
            namespaces ~= toAst(fileCst);
        }

        if (options.debugPrintAst != "")
        {
            string sdl = astToSdl(namespaces.filter!((ast.Namespace ns) => ns.namespaceDeclaration.namespaceName[0].source != "Dys").array);
            write(options.debugPrintAst, sdl);
        }

        // Perform static analysis on the AST
        tryOrElse(
            typeCheck(namespaces),
            delegate noreturn(ProblemInSourceException e) {
                log.errorInCompile(e.message, e.sourceCoordinates);
                throw Exit(1);
            },
        );

        // Compile to bytecode
        BcObject[] bcObjects = toBc(namespaces);

        // Get index of main function. Temporary until a better CLI is developed.
        Nullable!size_t indexOfMainObject;
        Nullable!size_t indexOfMainFunc;

        foreach (size_t nsIdx, ast.Namespace namespace; namespaces)
        {
            ast.FunctionDeclaration[] funcs =
                namespace.namespaceBody.namespaceItems
                .filterByType!(ast.FunctionDeclaration)
                .array;

            foreach (size_t fnIdx, ast.FunctionDeclaration func; funcs)
            {
                if (func.name.source == "main")
                {
                    indexOfMainObject = nsIdx;
                    indexOfMainFunc = fnIdx;
                    break;
                }
            }

            if (!indexOfMainObject.isNull && indexOfMainFunc.isNull) break;
        }

        // Run
        if (!indexOfMainFunc.isNull)
        {
            assert(
                !indexOfMainObject.isNull,
                "Found an index for the main function but didn't set the index of the object that contains it."
            );

            long exitCode = run(bcObjects, indexOfMainObject.get, indexOfMainFunc.get);
            return exitCode.to!int;
        }
    }
    catch (Exit exit)
    {
        return exit.exitCode;
    }

    return 0;
}
