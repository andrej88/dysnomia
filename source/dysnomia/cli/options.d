module dysnomia.cli.options;

import dysnomia.util.conv;
import exceeds_expectations;
import in_any_case;
import std.algorithm;
import std.conv;
import std.range;
import std.string;
import std.traits;
import std.uni;


/// Returns remaining positional arguments, and places options into
/// `destination`.
public string[] parseOptions(Options)(in string[] mainArgs, scope ref Options destination)
if (is(Options == struct))
in (mainArgs.length >= 1)
{
    string[] arguments = mainArgs[1..$].dup;
    string[] remainingArgs;
    string expectingValueForField;

    ArgumentsLoop:
    foreach (size_t argIdx, string arg; arguments)
    {
        switch (expectingValueForField)
        {
            static foreach (size_t fieldIdx, string fieldName; FieldNameTuple!Options)
            {{
                alias FieldType = Fields!(Options)[fieldIdx];

                case fieldName:
                    static if (is(FieldType == enum))
                    {
                        __traits(getMember, destination, fieldName) = arg.stringToEnumCaseInsensitive!FieldType;
                        expectingValueForField = string.init;
                        continue ArgumentsLoop;
                    }
                    else static if (
                        isSomeString!FieldType ||
                        isIntegral!FieldType ||
                        isFloatingPoint!FieldType
                    )
                    {
                        __traits(getMember, destination, fieldName) = arg.to!FieldType;
                        expectingValueForField = string.init;
                        continue ArgumentsLoop;
                    }
                    else assert(
                        false,
                        "Unreachable: Tried to place a space-separate value into field " ~
                        fieldName ~ " of type " ~ FieldType.stringof
                    );

            }}

            case string.init:
                break;

            default:
                assert(false, "Unexpected value for expectingValueForField: " ~ expectingValueForField);
        }

        if (arg == "--")
        {
            remainingArgs ~= arguments[argIdx + 1 .. $];
            break;
        }

        if (arg == "-" || !arg.startsWith("-"))
        {
            remainingArgs ~= arg;
            continue;
        }

        bool isShortForm = !arg.startsWith("--");
        assert(
            isShortForm ? arg.startsWith("-") : arg.startsWith("--"),
            "isShortForm does not match what the arg looks like. isShortForm = " ~
            isShortForm.to!string ~ ", arg = " ~ arg
        );

        string[3] argPieces =
            arg
            .stripLeft("-")
            .findSplit("=")
            .array;

        string destFieldName;
        string argValue = argPieces[2];


        if (isShortForm)
        {
            if (arg.length > 2)
            {
                if (arg[2] == '=')
                {
                    throw new InvalidShortFormException(
                        "Short-form options must separate their values with a space, not an equals sign. Received: " ~
                        arg
                    );
                }
                else
                {
                    throw new InvalidShortFormException("Short-form options may not be stacked. Received: " ~ arg);
                }
            }

            char shortAlias = arg[1];

            static foreach (string fieldName; FieldNameTuple!Options)
            {
                static if (hasUDA!(__traits(getMember, Options, fieldName), ShortAlias))
                {
                    if (getUDAs!(__traits(getMember, Options, fieldName), ShortAlias)[0].shortAlias == shortAlias)
                    {
                        destFieldName = fieldName;
                    }
                }
            }

            if (destFieldName == string.init)
            {
                throw new UnknownOptionException(arg);
            }
        }
        else
        {
            destFieldName = argPieces[0].toCase(Case.camel);
        }

        FieldNameForArgSwitch:
        switch (destFieldName)
        {
            static foreach (size_t fieldIdx, string fieldName; FieldNameTuple!Options)
            {{
                alias FieldType = Fields!(Options)[fieldIdx];

                case fieldName:

                    static if (is(FieldType : bool))
                    {
                        switch (toLower(argValue))
                        {
                            case "true", "":
                                __traits(getMember, destination, fieldName) = true;
                                break;

                            case "false":
                                __traits(getMember, destination, fieldName) = false;
                                break;

                            default:
                                throw new InvalidOptionValueException("--" ~ argPieces[0], arg);
                        }
                    }
                    else static if (is(FieldType == enum))
                    {
                        if (argValue != string.init)
                        {
                            __traits(getMember, destination, fieldName) = argValue.stringToEnumCaseInsensitive!FieldType;
                        }
                        else
                        {
                            expectingValueForField = fieldName;
                        }
                    }
                    else static if (
                        isSomeString!FieldType ||
                        isIntegral!FieldType ||
                        isFloatingPoint!FieldType
                    )
                    {
                        if (argValue != string.init)
                            __traits(getMember, destination, fieldName) = argValue.to!FieldType;
                        else
                        {
                            expectingValueForField = fieldName;
                        }
                    }
                    else
                        static assert(
                            false,
                            "Don't know what to do with option of type " ~ Fields!(Options)[fieldIdx].stringof
                        );

                    break FieldNameForArgSwitch;
            }}

            default:
                throw new UnknownOptionException("--" ~ argPieces[0]);
        }
    }

    return remainingArgs;
}

public struct ShortAlias
{
    char shortAlias;
}

public struct Description
{
    string description;
}

public class UnknownOptionException : Exception
{
    this(
        string optionName,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null
    )
    pure nothrow @safe
    in (
        optionName.startsWith("-"),
        "UnknownOptionException expected optionName to start with \"-\" but got: \"" ~ optionName ~ "\"."
    )
    {
        super("Unknown option: " ~ optionName, file, line, nextInChain);
    }
}

public class InvalidOptionValueException : Exception
{
    this(
        string optionName,
        string received,
        string file = __FILE__,
        size_t line = __LINE__,
        Throwable nextInChain = null
    )
    pure nothrow @safe
    {
        super("Boolean flag " ~ optionName ~ " does not take a value. Received: " ~ received, file, line, nextInChain);
    }
}

public class InvalidShortFormException : Exception
{
    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null)
    pure nothrow @nogc @safe
    {
        super(msg, file, line, nextInChain);
    }
}

public enum string getHelpText(Options, size_t indent = 0) = _getHelpText!Options(indent);

private string _getHelpText(Options)(size_t indent)
if (is(Options == struct))
{
    struct Entry
    {
        string longArg;
        string shortArg;
        string description;

        string typeStringof;

        string fullArgColumn() const
        {
            Appender!string result;

            result ~= longArg;

            if (typeStringof != bool.stringof)
            {
                result ~= "=";
                result ~= "<value>";                // TODO: Annotation to customize value name
            }

            if (shortArg != string.init)
            {
                result ~= ", ";
                result ~= shortArg;

                if (typeStringof != bool.stringof)
                {
                    result ~= " ";
                    result ~= "<value>";            // TODO: Annotation to customize value name
                }
            }

            return result.data;
        }
    }

    enum string separator = "  ";

    Entry[] entries;

    static foreach (size_t idx, string fieldName; FieldNameTuple!Options)
    {{
        alias FieldType = FieldTypeTuple!(Options)[idx];

        Entry entry;
        entry.longArg = "--" ~ fieldName.toCase(Case.kebab);
        entry.typeStringof = FieldType.stringof;

        static if (hasUDA!(__traits(getMember, Options, fieldName), ShortAlias))
        {
            enum char shortAlias = getUDAs!(__traits(getMember, Options, fieldName), ShortAlias)[0].shortAlias;
            entry.shortArg = "-" ~ shortAlias;
        }

        static if (hasUDA!(__traits(getMember, Options, fieldName), Description))
        {
            entry.description = getUDAs!(__traits(getMember, Options, fieldName), Description)[0].description;
        }
        // else description is blank

        // TODO: Print what the default value is


        entries ~= entry;
    }}

    size_t longestArgColumn;
    foreach (Entry entry; entries)
    {
        if (entry.fullArgColumn.length > longestArgColumn)
            longestArgColumn = entry.fullArgColumn.length;
    }

    int descriptionColumnWidth = 80 - indent.to!int - longestArgColumn.to!int - separator.length.to!int;
    if (descriptionColumnWidth < 20)
        descriptionColumnWidth = 20;

    Appender!string result;
    foreach (Entry entry; entries)
    {
        result ~= ' '.repeat(indent);
        result ~= entry.fullArgColumn.padLeft(' ', longestArgColumn);
        result ~= separator;

        string[] descriptionLines =
            entry
            .description
            .wrap(descriptionColumnWidth)
            .splitLines();

        foreach (size_t idx, string line; descriptionLines)
        {
            if (idx > 0)
            {
                result ~= ' '.repeat(indent + longestArgColumn + separator.length);
            }

            result ~= line;
            result ~= '\n';
        }

        result ~= "\n";
    }

    return result.data.stripRight("\n");
}


version(unittest) private struct Empty {}
@("parse nothing")
unittest
{
    Empty options;
    string[] remainingArgs = parseOptions(["./dysnomia"], options);
    string[] expected;
    expect(remainingArgs).toEqual(expected);
    expect(options).toEqual(Empty.init);
}

@("error when first arg is missing")
unittest
{
    import core.exception : AssertError;
    struct Empty {}
    Empty options;
    expect({ parseOptions([], options); }).toThrow!AssertError;
}

@("return remaining position arguments")
unittest
{
    struct Empty {}
    Empty options;
    string[] remainingArgs = parseOptions(["./dysnomia", "do", "this", "do", "that"], options);
    expect(remainingArgs).toEqual(["do", "this", "do", "that"]);
    expect(options).toEqual(Empty.init);
}

@("store a boolean option in the options struct")
unittest
{
    struct Options
    {
        bool verbose;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "--verbose"], options);
    expect(options).toEqual(Options(true));
    expect(remainingArgs).toSatisfy(it => it.length == 0);
}

@("interpret kebab-case options as camelCase fields")
unittest
{
    struct Options
    {
        bool helloWorld;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "--hello-world"], options);
    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options(true));
}

@("store a string option split with an equals sign in the options struct")
unittest
{
    struct Options
    {
        string output;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "--output=hi"], options);
    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options("hi"));
}

@("store a string option split with spaces in the options struct")
unittest
{
    struct Options
    {
        string output;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "--output", "hi"], options);
    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options("hi"));
}

@("return arguments that have not been consumed")
unittest
{
    struct Options
    {
        string output;
        bool thisIsABool;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "--output", "hi", "unused", "--this-is-a-bool", "also-unused"],
        options
    );

    expect(remainingArgs).toEqual(["unused", "also-unused"]);
    expect(options).toEqual(Options("hi", true));
}

@("treat everything after a double hyphen a regular argument, not an option")
unittest
{
    struct Options
    {
        bool aRealOption;
        bool anotherRealOption;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "--a-real-option", "--", "--another-real-option"],
        options
    );

    expect(remainingArgs).toEqual(["--another-real-option"]);
    expect(options).toEqual(Options(true, false));
}

@("a trailing double hyphen has no effect")
unittest
{
    struct Options
    {
        bool aRealOption;
        bool anotherRealOption;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "--a-real-option", "--"],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options(true, false));
}

@("throw UnknownOptionException when encountering an unknown flag")
unittest
{
    Empty options;
    expect({
        parseOptions(["./dysnomia", "--i-dont-do-anything"], options);
    }).toThrow!UnknownOptionException("Unknown option: --i-dont-do-anything");
}

@("throw UnknownOptionException when encountering an unknown flag with a value separate by '='")
unittest
{
    Empty options;
    expect({
        parseOptions(["./dysnomia", "--i-dont-do-anything=hi"], options);
    }).toThrow!UnknownOptionException("Unknown option: --i-dont-do-anything");
}

@("throw UnknownOptionException when encountering an unknown short-form flag")
unittest
{
    Empty options;
    expect({
        parseOptions(["./dysnomia", "-x"], options);
    }).toThrow!UnknownOptionException("Unknown option: -x");
}

@("throw InvalidOptionValueException when encountering a boolean flag with a value attached")
unittest
{
    struct Options
    {
        bool verbose;
    }

    Options options;

    expect({
        parseOptions(["./dysnomia", "--verbose=verbosity"], options);
    }).toThrow!InvalidOptionValueException(
        "Boolean flag --verbose does not take a value. Received: --verbose=verbosity"
    );
}

@("accept a short-form flag")
unittest
{
    struct Options
    {
        @ShortAlias('v')
        bool verbose;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "-v"], options);

    expect(options).toEqual(Options(true));
    expect(remainingArgs).toSatisfy(it => it.length == 0);
}

@("treat a single dash as a regular argument")
unittest
{
    struct Options
    {
        @ShortAlias('v')
        bool verbose;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "-v", "-", "--", "-"], options);

    expect(options).toEqual(Options(true));
    expect(remainingArgs).toEqual(["-", "-"]);
}

@("throw InvalidShortFormException if trying to pass multiple short-form options as one argument")
unittest
{
    // Technically the same test as "throw if trying to pass a value without a space separator (e.g. gcc's -Wall)"

    struct Options
    {
        @ShortAlias('v')
        bool verbose;

        @ShortAlias('a')
        bool all;

        @ShortAlias('X')
        bool execute;

        @ShortAlias('o')
        string outputFile;
    }

    Options options;
    expect({
        parseOptions(["./dysnomia", "-Xav"], options);
    }).toThrow!InvalidShortFormException("Short-form options may not be stacked. Received: -Xav");
}

@("store a short-form string option split with a space")
unittest
{
    struct Options
    {
        @ShortAlias('o')
        string output;
    }

    Options options;
    string[] remainingArgs = parseOptions(["./dysnomia", "-o", "file.txt"], options);

    expect(options).toEqual(Options("file.txt"));
    expect(remainingArgs).toSatisfy(it => it.length == 0);
}

@("throw InvalidShortFormException if short-form string option uses '=' to separate the value")
unittest
{
    struct Options
    {
        @ShortAlias('o')
        string output;
    }

    Options options;
    expect({
         parseOptions(["./dysnomia", "-o=file.txt"], options);
    }).toThrow!InvalidShortFormException(
        "Short-form options must separate their values with a space, not an equals sign. Received: -o=file.txt"
    );
}

@("allow `true` and `false` as case-insensitive values separated by '=' for long boolean options")
unittest
{
    struct Options
    {
        bool smallTrue;
        bool bigTrue;
        bool smallFalse;
        bool bigFalse;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        [
            "./dysnomia",
            "--small-true=true",
            "--big-true=TRUE",
            "--small-false=false",
            "--big-false=FALSE",
        ],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options(true, true, false, false));
}

@("Treat `true` and `false` as positional arguments if space-separated")
unittest
{
    // TODO: Warn in ambigious case of `./foo --some-boolean true` that the `true`` is being treated as a positional argument and to use `--some--boolean=true` instead.

    struct Options
    {
        bool first;

        @ShortAlias('s')
        bool second;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        [
            "./dysnomia",
            "--first",
            "true",
            "-s",
            "true",
        ],
        options
    );

    expect(remainingArgs).toEqual(["true", "true"]);
    expect(options).toEqual(Options(true, true));
}

@("Use the latest occurrence of an option if several are present")
unittest
{
    struct Options
    {
        string name;
        bool doStuff;
    }

    Options options;
    string[] remainingArgs = parseOptions([
            "./dysnomia",
            "--name",
            "betty",
            "--do-stuff",
            "--name",
            "al",
            "--do-stuff=false",
        ],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options("al", false));
}

@("Store an integer in the options struct")
unittest
{
    struct Options
    {
        @ShortAlias('x')
        int x;

        @ShortAlias('y')
        long y;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "-x", "22", "-y", "-8888"],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options(22, -8888));
}

@("Store an floating point number in the options struct")
unittest
{
    struct Options
    {
        @ShortAlias('u')
        double u;

        @ShortAlias('v')
        float v;

        @ShortAlias('w')
        real w;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "-u", "3.1415926535", "-v", "-8888", "-w", "2.3e8"],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options.u).toApproximatelyEqual(3.1415926535);
    expect(options.v).toApproximatelyEqual(-8888);
    expect(options.w).toApproximatelyEqual(230_000_000.0);
}

@("Store an enum in the options struct given a case-insensitive enum member name")
unittest
{
    enum Weekday { monday, tuesday, wednesday, thursday, friday, saturday, sunday }

    struct Options
    {
        Weekday start;
        Weekday end;
    }

    Options options;
    string[] remainingArgs = parseOptions(
        ["./dysnomia", "--start=Monday", "--end", "FRIDAY"],
        options
    );

    expect(remainingArgs).toSatisfy(it => it.length == 0);
    expect(options).toEqual(Options(Weekday.monday, Weekday.friday));
}

// TODO: help text
// TODO: better error messages
// TODO: Reduce duplication, somehow allow "incrementing" the loop to get rid of the "expectingValueForField" variable
// TODO: Error if given a non-boolean option without a value
