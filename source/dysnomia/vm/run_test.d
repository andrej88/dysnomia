module dysnomia.vm.run_test;

import dysnomia.data.bytecode;
import dysnomia.vm.run;
import exceeds_expectations;
import std.conv : to;


@("Runs a no-op function")
unittest
{
    run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcReturn(false).to!BcInstr,
                    ])
                ],
            ),
        ],
        0,
        0,
    );
}

@("Uses the return value of the executed function as the exit code")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(3).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ])
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(3);
}

@("Returns the result of some addition and subtraction")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(88).to!BcInstr,
                        BcIntConst(77).to!BcInstr,
                        BcIntAdd().to!BcInstr,
                        BcIntConst(66).to!BcInstr,
                        BcIntSub().to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ])
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(99);
}

@("Runs a function that calls another")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcGetInObject(0, 1).to!BcInstr,
                        BcFnCall(0).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                    BcFunc([
                        BcIntConst(23).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(23);
}

@("Runs a function that calls an identity function")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcGetInObject(0, 1).to!BcInstr,
                        BcIntConst(74).to!BcInstr,
                        BcFnCall(1).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                    BcFunc([
                        BcGetInFrame(0).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(74);
}

@("Does not return a value from a void function")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(23).to!BcInstr,
                        BcReturn(false).to!BcInstr,
                    ]),
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(0);
}

@("Runs a function with variables")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(4).to!BcInstr,
                        BcSetInFrame(0).to!BcInstr,
                        BcIntConst(7).to!BcInstr,
                        BcSetInFrame(1).to!BcInstr,
                        BcGetInFrame(0).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(4);
}

@("Returns at the first return instruction encountered")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(5).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                        BcIntConst(6).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ])
                ],
            )
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(5);
}

@("Runs a function that calls another function from a different namespaces")
unittest
{
    size_t exitCode = run(
        [
            BcObject(
                [],
                [
                    BcFunc([
                        BcGetInObject(1, 0).to!BcInstr,
                        BcFnCall(0).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                ],
            ),
            BcObject(
                [],
                [
                    BcFunc([
                        BcIntConst(54).to!BcInstr,
                        BcReturn(true).to!BcInstr,
                    ]),
                ],
            ),
        ],
        0,
        0,
    );

    expect(exitCode).toEqual(54);
}
