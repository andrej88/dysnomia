module dysnomia.vm.run;

import dysnomia.data.bytecode;
import dysnomia.data.intrinsics;
import dysnomia.util.stack;
import dysnomia.util.sumtype;
import std.conv : to;
import std.format;
import std.meta;
import std.path;
import std.sumtype;
import std.typecons;


private alias Value = SumType!(
    bool,
    long,
    string,
    DysNullable,
    BcFunc,
);

// No nested nullables. I'm ok with that, stuff like Option[Option[T]] is a code
// smell to me.
private alias ValueSansNullable = SumType!(Erase!(DysNullable, Value.Types));

struct DysNullable {
    bool isDefined;
    ValueSansNullable payload;
}

private struct Frame
{
    BcObject bcObject;
    Value[] variables;
    Stack!Value stack;

    this(BcObject bcObject, Value[] variables)
    {
        this.bcObject = bcObject;
        this.variables = variables;
    }
}

class Vm
{
    private BcObject[] objs;
    private Stack!Frame frames;
    private ref Frame currentFrame() => frames.peek;

    this(BcObject[] objs, Frame initialFrame)
    {
        this.objs = objs;
        this.frames.push(initialFrame);
    }

    Nullable!Value interpretBytecode(BcInstr[] instrs)
    {
        Nullable!Value result;
        bool shouldReturn;

        for (size_t instrIdx = 0; instrIdx < instrs.length; instrIdx++)
        {
            BcInstr instr = instrs[instrIdx];

            instr.match!(
                (BcReturn op) {
                    shouldReturn = true;

                    if (op.shouldReturnAValue)
                    {
                        assert(
                            !currentFrame.stack.empty,
                            "Function needs to return a value but the stack is empty!"
                        );
                        result = currentFrame.stack.pop().nullable;
                    }
                },

                (BcBoolConst op) {
                    currentFrame.stack.push(op.value.to!Value);
                },

                (BcIntConst op) {
                    currentFrame.stack.push(op.value.to!Value);
                },

                (BcStringConst op) {
                    currentFrame.stack.push(op.value.to!Value);
                },

                (BcNullConst op) {
                    currentFrame.stack.push(DysNullable(false).to!Value);
                },

                (BcObjectIdConst op) {
                    currentFrame.stack.push(op.value.to!Value);
                },

                (BcIntAdd _) {
                    long rhs = currentFrame.stack.pop().get!long;
                    long lhs = currentFrame.stack.pop().get!long;
                    currentFrame.stack.push(Value(lhs + rhs));
                },

                (BcIntSub _) {
                    long rhs = currentFrame.stack.pop().get!long;
                    long lhs = currentFrame.stack.pop().get!long;
                    currentFrame.stack.push(Value(lhs - rhs));
                },

                (BcIntMultiply _) {
                    long rhs = currentFrame.stack.pop().get!long;
                    long lhs = currentFrame.stack.pop().get!long;
                    currentFrame.stack.push(Value(lhs * rhs));
                },

                (BcEqual _) {
                    Value rhs = currentFrame.stack.pop();
                    Value lhs = currentFrame.stack.pop();

                    // Non-nullables can be compared against nullables, so a simple lhs == rhs is not enough.
                    // I wish D had proper sum types and match statements...

                    if (lhs.has!DysNullable && !rhs.has!DysNullable)
                    {
                        if (!lhs.get!DysNullable.isDefined)
                        {
                            currentFrame.stack.push(Value(false));
                        }
                        else
                        {
                            Value lhsPayload = lhs.get!DysNullable.payload.match!(it => Value(it));
                            currentFrame.stack.push(Value(lhsPayload == rhs));
                        }
                    }
                    else if (!lhs.has!DysNullable && rhs.has!DysNullable)
                    {
                        if (!rhs.get!DysNullable.isDefined)
                        {
                            currentFrame.stack.push(Value(false));
                        }
                        else
                        {
                            Value rhsPayload = rhs.get!DysNullable.payload.match!(it => Value(it));
                            currentFrame.stack.push(Value(lhs == rhsPayload));
                        }
                    }
                    else
                    {
                        currentFrame.stack.push(Value(lhs == rhs));
                    }
                },

                (BcAnd _) {
                    Value rhs = currentFrame.stack.pop();
                    Value lhs = currentFrame.stack.pop();
                    currentFrame.stack.push(Value(lhs == rhs));
                },

                (BcIntNegate _) {
                    long operand = currentFrame.stack.pop().get!long;
                    currentFrame.stack.push(Value(-operand));
                },

                (BcWrapWithNullable _) {
                    Value payload = currentFrame.stack.pop();

                    // If it's already a nullable, don't re-wrap it.
                    currentFrame.stack.push(
                        payload.match!(
                            (DysNullable dn) => dn,
                            (other) => DysNullable(true, other.to!ValueSansNullable),
                        ).to!Value,
                    );
                },

                (BcUnwrapNullable _) {
                    Value value = currentFrame.stack.pop();

                    currentFrame.stack.push(
                        value.match!(
                            (DysNullable dn) {
                                if (dn.isDefined)
                                {
                                    return dn.payload.match!(it => it.to!Value);
                                }
                                else
                                {
                                    assert(
                                        false,
                                        "Attempted to unwrap a null value. This should have been caught at compile time."
                                    );
                                }
                            },
                            (other) => other.to!Value,
                        ),
                    );
                },

                (BcGetInFrame op) {
                    currentFrame.stack.push(currentFrame.variables[op.idxOfVariableInFrame]);
                },

                (BcSetInFrame op) {
                    currentFrame.variables[op.idxOfVariableInFrame] = currentFrame.stack.pop();
                },

                (BcGetInObject op) {
                    BcFunc func = objs[op.objIdx].funcs[op.idxOfVariableInObject];
                    currentFrame.stack.push(func.to!Value);
                },

                (BcGetInObjectFromStack op) {
                    long objectId = currentFrame.stack.pop().get!long;
                    BcFunc func = objs[objectId].funcs[op.idxOfVariableInObject];
                    currentFrame.stack.push(func.to!Value);
                },

                (BcFnCall op) {
                    Value[] arguments = currentFrame.stack.popN(op.numArgs);
                    BcFunc func = currentFrame.stack.pop().get!BcFunc;

                    Frame nextStackFrame = Frame(
                        currentFrame.bcObject,
                        arguments,
                    );

                    nextStackFrame.variables.length = func.countLocals();

                    frames.push(nextStackFrame);
                    Nullable!Value resultOfCall = interpretBytecode(func.instrs);
                    frames.pop();

                    if (!resultOfCall.isNull)
                    {
                        currentFrame.stack.push(resultOfCall.get);
                    }
                },

                (BcCallFnIntrinsic op) {
                    Value[] arguments = currentFrame.stack.popN(op.numArgs);

                    switch (op.intrinsicFunctionId)
                    {
                        case getIntrinsicFunctionId("println"):

                            import std.stdio : writeln;

                            arguments[0].match!(
                                (DysNullable dn) {
                                    if (dn.isDefined)
                                    {
                                        dn.payload.match!writeln;
                                    }
                                    else
                                    {
                                        writeln("null");
                                    }
                                },
                                (other) { writeln(other); }
                            );

                            break;

                        case getIntrinsicFunctionId("append"):
                            result = (arguments[0].get!string ~ arguments[1].get!string).to!Value;
                            break;

                        default:
                            assert(false, format("Invalid instrinsic function ID: %x", op.intrinsicFunctionId));
                    }
                },

                (BcJump jump) {
                    instrIdx += jump.offset;
                },

                (BcJumpIfFalse jumpIfFalse) {
                    bool condition = currentFrame.stack.pop.get!bool;
                    if (!condition) {
                        instrIdx += jumpIfFalse.offset;
                    }
                },

                (BcDuplicateTip _) {
                    currentFrame.stack.push(currentFrame.stack.peek);
                },
            );

            if (shouldReturn) return result;
        }

        assert(false, "No return statement was encountered.");
    }
}


long run(BcObject[] bcObjs, size_t objIdx, size_t fnIdx)
{
    BcObject bcObj = bcObjs[objIdx];
    BcFunc fn = bcObj.funcs[fnIdx];
    Vm vm = new Vm(bcObjs, Frame(bcObj, new Value[fn.countLocals()]));
    Nullable!Value result = vm.interpretBytecode(fn.instrs);

    return result.get(Value(0)).get!long;
}
