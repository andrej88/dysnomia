module dysnomia.util.sumtype;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import std.algorithm;
import std.conv;
import std.meta;
import std.range;
import std.sumtype;


/// Short wrapper for [std.sumtype.SumType.tryMatch] which returns the
/// payload if it really is of the requested type, and throws an
/// [Error] otherwise. Useful in situations where you are certain of
/// the type of the payload, but the compiler isn't.
R get(R, S : SumType!Args, Args...)(S s)
{
    try
    {
        return s.tryMatch!((R r) => r);
    }
    catch (MatchException matchException)
    {
        throw new Error(
            `Tried to unwrap a SumType as "` ~ R.stringof ~ `"` ~
            ` but it turned out to be: ` ~ s.to!string,
            matchException
        );
    }
}

/// Short wrapper for [std.sumtype.SumType.tryMatch] which returns the
/// payload if it really is of the requested type, and throws a
/// [std.sumtype.MatchException] otherwise.
R tryGet(R, S : SumType!Args, Args...)(S s)
{
    return s.tryMatch!((R r) => r);
}

/// Short wrapper for [std.sumtype.SumType.tryMatch] which returns the
/// payload if it really is of the requested type, otherwise it
/// returns `defaultValue`.
R getOrElse(R, S : SumType!Args, Args...)(S s, R defaultValue)
{
    try
    {
        return s.tryMatch!((R r) => r);
    }
    catch (MatchException matchException)
    {
        return defaultValue;
    }
}

bool has(T, S : SumType!Args, Args...)(S s)
{
    static if (staticIndexOf!(T, Args) != -1)
    {
        return s.match!(
            (T _) => true,
            (_) => false,
        );
    }
    else
    {
        return false;
    }
}

auto filterByType(Type, Range)(Range range)
if (
    isInputRange!Range &&
    is(ElementType!Range : SumType!Args, Args...) &&
    staticIndexOf!(Type, Args) != -1
)
out (result; is(ElementType!(typeof(result))))
{
    return
        range
            .filter!(it => it.has!Type)
            .map!(it => it.get!Type);
}

/// Sneaky little overload so it can be used with alternation nodes, since
/// `alias this` is no longer allowed in classes.
auto filterByType(Type, Range)(Range range)
if (
    isInputRange!Range &&
    (
        ast.isAlternationNode!(ElementType!Range) ||
        cst.isAlternationNode!(ElementType!Range)
    )
)
out (result; is(ElementType!(typeof(result))))
{
    return
        range
            .map!(it => it.alternations)
            .filterByType!Type;
}
