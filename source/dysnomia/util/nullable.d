module dysnomia.util.nullable;

import std.typecons;


/// Return the payload or `null` if isNull. Only works if the payload
/// type is a reference type, i.e. `is(typeof(null) : Payload)`
public Payload getOrElse(D, T : Nullable!Payload, Payload)(T t, D defaultValue)
if (is(D : Payload))
{
    if (t.isNull) return defaultValue;
    else return t.get;
}
