module dysnomia.util.leb128;

import exceeds_expectations;
import std.traits;


ubyte[] toUleb128(U)(U number)
pure
if (isUnsigned!U)
{
    ubyte[] result;

    do
    {
        ubyte first7bits = number & 0b01111111;
        number >>= 7;

        ubyte nextUbyte =
            number == 0 ?
            first7bits :
            first7bits | 0b10000000;

        result ~= nextUbyte;
    }
    while (number != 0);

    return result;
}

@("Encode uint 624485 in ULEB128")
unittest
{
    expect(toUleb128(uint(624_485))).toEqual([0xe5, 0x8e, 0x26]);
}

@("Encode uint 13_034 (multiple of 7 bytes long) in ULEB128")
unittest
{
    //    11001011101010
    //  1100101  1101010
    // 01100101 11101010
    // 11101010 01100101 (Little Endian)
    expect(toUleb128(uint(13_034))).toEqual([0b1110_1010, 0b01100101]);
}

@("Encode ulong 0x8ab4826e_8992716e in ULEB128")
unittest
{
    //                          1000101010110100100000100110111010001001100100100111000101101110
    //  0000001  0001010  1011010  0100000  1001101  1101000  1001100  1001001  1100010  1101110
    // 00000001 10001010 11011010 10100000 11001101 11101000 11001100 11001001 11100010 11101110
    // 11101110 11100010 11001001 11001100 11101000 11001101 10100000 11011010 10001010 00000001 (Little Endian)
    expect(toUleb128(ulong(0x8ab4826e_8992716e))).toEqual([
        0b11101110,
        0b11100010,
        0b11001001,
        0b11001100,
        0b11101000,
        0b11001101,
        0b10100000,
        0b11011010,
        0b10001010,
        0b00000001,
    ]);
}

@("Encode uint 0 in ULEB128")
unittest
{
    expect(toUleb128(uint(0))).toEqual([
        0b00000000,
    ]);
}

@("Encode ubyte 88 in ULEB128")
unittest
{
    expect(toUleb128(ubyte(88))).toEqual([
        0b01011000,
    ]);
}
