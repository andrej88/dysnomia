module dysnomia.util.conv;

import std.meta;
import std.traits;
import std.uni;


/// std.conv.to can convert strings to enums, but is case-sensitive.
/// Makes sense, since D identifiers are case-sensitive. Still,
/// sometimes it's useful to do it case-insensitively (for example if
/// the string is is the input of a user who doesn't know or care
/// about D's casing conventions).
public E stringToEnumCaseInsensitive(E, S)(S input)
if (isSomeString!S && is(E == enum))
{
    switch (input.toLower)
    {
        static foreach (member; EnumMembers!E)
        {
            case __traits(identifier, member).toLower():
                return member;
        }

        default:
            throw new Exception("Enum " ~ E.stringof ~ " does not have a member case-insensitively named " ~ input ~ "");
    }
}
