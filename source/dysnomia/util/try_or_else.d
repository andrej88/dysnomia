module dysnomia.util.try_or_else;


T tryOrElse(T, E : Exception, ElseReturn)(lazy T expression, ElseReturn delegate(E e) elseBlock)
if (is(ElseReturn : T) || is(ElseReturn == noreturn))
{
    try
    {
        return expression;
    }
    catch (E e)
    {
        return elseBlock(e);
    }
}
