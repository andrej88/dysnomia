module dysnomia.util.stack;


struct Stack(T)
{
    private T[] contents;

    this(T[] contents)
    {
        this.contents = contents;
    }

    size_t length() => contents.length;
    bool empty() => contents.length == 0;

    void push(T value)
    {
        contents ~= value;
    }

    T pop()
    {
        T result = contents[$ - 1];
        contents.length -= 1;
        return result;
    }

    /// Resulting array contains elements in the order they were pushed.
    T[] popN(size_t numElementsToPop)
    {
        T[] result = contents[$ - numElementsToPop .. $];
        contents.length -= numElementsToPop;
        return result;
    }

    void drop()
    {
        contents.length -= 1;
    }

    void dropN(size_t numElementsToDrop)
    {
        contents.length -= numElementsToDrop;
    }

    /// Result is an lvalue.
    ref T peek()
    {
        return contents[$ - 1];
    }

    /// Resulting array contains elements in the order they were pushed.
    /// The result is not to be trusted after the stack is modified.
    T[] peekN(size_t numElementsToPeek)
    {
        return contents[$ - numElementsToPeek .. $];
    }
}
