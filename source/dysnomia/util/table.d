module dysnomia.util.table;

import std.algorithm.iteration;
import std.algorithm.searching;
import std.array;
import std.conv : to;


class TableQueryError : Error
{
    this(string msg, Throwable nextInChain = null) pure nothrow @nogc @safe
    {
        super(msg, nextInChain);
    }
}


/// A very simple database-like Table for in-memory use.
class Table(Record)
if (is(Record == struct))
{
    private Record[] data;

    size_t count() const => data.length;

    void insert(Record row)
    {
        data ~= row;
    }

    // These take their predicates as regular parameters instead of template
    // aliases because of the dual-context thing.

    Record[] findAll(bool delegate(Record) predicate)
    {
        return data.filter!predicate.array;
    }

    Record findOne(bool delegate(Record) predicate)
    {
        Record[] result = data.filter!predicate.array;

        switch (result.length) {
            case 0:
                throw new TableQueryError("Table does not contain an entry that matches the predicate.");

            case 1:
                return result[0];

            default:
                throw new TableQueryError(
                    "Table contains multiple entries that match the predicate:\n" ~
                    result.map!(to!string).join("\n") ~ "\n"
                );
        }
    }

    bool contains(bool delegate(Record) dg)
    {
        return data.canFind!dg;
    }

    int opApply(scope int delegate(ref Record) dg)
    {
        int result = 0;

        foreach (ref Record record; data)
        {
            result = dg(record);
            if (result)
                break;
        }

        return result;
    }
}
