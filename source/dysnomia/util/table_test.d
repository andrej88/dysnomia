module dysnomia.util.table_test;

// No, not that kind of table test

import dysnomia.util.table;
import exceeds_expectations.expect;
import std.conv : to;
import std.variant;


@("An empty table")
unittest
{
    struct Record {}

    Table!Record table = new Table!Record();

    expect(table).toSatisfy(it => it.count() == 0);
}

@("Add an entry to a single-column table")
unittest
{
    struct Record { int id; }

    Table!Record table = new Table!Record();

    table.insert(Record(23));

    expect(table).toSatisfy(it => it.count() == 1);
}

@("Add multiple entries to a multi-column table")
unittest
{
    struct Record { string name; string surname; }

    Table!Record table = new Table!Record();

    table.insert(Record("Michael", "Mouse"));
    table.insert(Record("Minerva", "Mouse"));

    expect(table).toSatisfy(it => it.count() == 2);
}

@("Select an arbitrary number of entries from a table using findAll")
unittest
{
    struct Record { string name; string surname; }

    Table!Record table = new Table!Record();

    table.insert(Record("Michael", "Mouse"));
    table.insert(Record("Minerva", "Mouse"));

    Record[] michaels = table.findAll((Record record) => record.name == "Michael");
    expect(michaels).toSatisfy(it => it.length == 1);

    Record[] minervae = table.findAll((Record record) => record.name == "Minerva");
    expect(minervae).toSatisfy(it => it.length == 1);

    Record[] mice = table.findAll((record) => record.surname == "Mouse");
    expect(mice).toSatisfy(it => it.length == 2);

    Record[] geralds = table.findAll((record) => record.name == "Gerald");
    expect(geralds).toSatisfy(it => it.length == 0);
}

@("Select one entry from a table using findOne, and throw an error unless exactly one entry was found")
unittest
{
    enum Color { Red, Yellow, Blue }
    struct Vehicle { Color color; string vehicleClass; }

    Table!Vehicle table = new Table!Vehicle();

    table.insert(Vehicle(Color.Red, "racecar"));
    table.insert(Vehicle(Color.Yellow, "tram"));
    table.insert(Vehicle(Color.Yellow, "submarine"));

    Vehicle red = table.findOne((Vehicle v) => v.color == Color.Red);
    expect(red).toEqual(Vehicle(Color.Red, "racecar"));

    expect({
        table.findOne((Vehicle v) => v.color == Color.Blue);
    }).toThrow!TableQueryError("Table does not contain an entry that matches the predicate.");

    expect({
        table.findOne((Vehicle v) => v.color == Color.Yellow);
    }).toThrow!TableQueryError(
        "Table contains multiple entries that match the predicate:\n" ~
        Vehicle(Color.Yellow, "tram").to!string ~ "\n" ~
        Vehicle(Color.Yellow, "submarine").to!string ~ "\n"
    );
}

@("Iterate over the entries in a table in order of insertion")
unittest
{
    struct Record { string name; string surname; }

    Table!Record table = new Table!Record();

    table.insert(Record("Michael", "Mouse"));
    table.insert(Record("Minerva", "Mouse"));
    table.insert(Record("Jerry", null));
    table.insert(Record("Pinky", null));
    table.insert(Record("Brain", null));
    table.insert(Record("Danger", "Mouse"));

    Record[] seen;

    foreach (Record record; table)
    {
        seen ~= record;
    }

    expect(seen).toEqual([
        Record("Michael", "Mouse"),
        Record("Minerva", "Mouse"),
        Record("Jerry", null),
        Record("Pinky", null),
        Record("Brain", null),
        Record("Danger", "Mouse"),
    ]);
}

@("Iterate over the entries in a table by reference")
unittest
{
    struct Record { string name; string surname; }

    Table!Record table = new Table!Record();

    table.insert(Record("Michael", "Mouse"));
    table.insert(Record("Minerva", "Mouse"));
    table.insert(Record("Jerry", null));
    table.insert(Record("Pinky", null));
    table.insert(Record("Brain", null));
    table.insert(Record("Danger", "Mouse"));

    expect(table.findAll(it => it.name == "Gerald").length).toEqual(0);

    foreach (ref Record record; table)
    {
        if (record.name == "Jerry")
        {
            record.name = "Gerald";
        }
    }

    expect(table.findAll(it => it.name == "Gerald").length).toEqual(1);
}
