module dysnomia.util.auto_index;

import std.conv : to;


/// A container that keeps track of the order in which items were added.
class AutoIndex(E)
{
    private E[string] indices;
    private E count;

    public void add(string name)
    {
        assert(name !in indices, name ~ " is already in the indices map: " ~ indices.to!string);
        indices[name] = count;
        count += 1;
    }

    public E get(string name) const @safe pure
    {
        assert(name in indices, name ~ " is not in the indices map: " ~ indices.to!string);
        return indices[name];
    }

    public bool contains(string name) const @safe pure
    {
        return (name in indices) !is null;
    }

    override string toString() const @safe pure
    {
        return indices.to!string;
    }
}
