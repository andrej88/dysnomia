module dysnomia.util.algorithm;

import std.range;
import std.traits;


template maxElements(alias predicate, R)
if (isSomeFunction!predicate && isInputRange!R)
{
    alias E = ElementType!R;
    alias B = ReturnType!predicate;

    public E[] maxElements(R r)
    {
        B maxMeasure;
        E[] result;

        foreach (E e; r)
        {
            B measure = predicate(e);
            if (measure > maxMeasure)
            {
                maxMeasure = measure;
                result = [e];
            }
            else if (measure == maxMeasure)
            {
                result ~= e;
            }
        }

        return result;
    }

}
