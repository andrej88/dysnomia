module dysnomia.util.debugging;


debug
{
    /// Functional-style writeln, writes the argument to stdout and
    /// returns it. Inspired by Elixir's IO.inspect.
    public T inspect(alias transform = (t) => t, T)(
        T t,
        string before = null,
        string after = null
    )
    {
        import std.stdio : writeln;
        if (before) writeln(before);
        writeln(transform(t));
        if (after) writeln(after);
        return t;
    }
}
else
{
    public T inspect(alias transform = (t) => t, T)(
        T t,
        string before = null,
        string after = null
    )
    {
        return t;
    }
}
