module dysnomia.data.bytecode;

import dysnomia.util.algorithm;
import dysnomia.util.sumtype;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.conv : to;
import std.range;
import std.sumtype;
import std.typecons;
import std.variant;


struct BcObject
{
    string[] namespace;
    BcFunc[] funcs;
}

struct BcFunc
{
    BcInstr[] instrs;

    private Nullable!size_t countLocalsCached;

    size_t countLocals()
    {
        if (countLocalsCached.isNull)
        {
            auto localIndicesUsed =
                instrs
                .filter!((BcInstr instr) => instr.has!BcSetInFrame || instr.has!BcGetInFrame)
                .map!(
                    (BcInstr instr) => instr.match!(
                        (BcSetInFrame instr) => instr.idxOfVariableInFrame,
                        (BcGetInFrame instr) => instr.idxOfVariableInFrame,
                        (_else) => assert(
                            false,
                            "Unreachable: only BcSetInFrame and BcGetInFrame " ~
                            "should be in the range, but got: " ~ _else.to!string,
                        ),
                    )
                );

            if (localIndicesUsed.walkLength() == 0)
            {
                countLocalsCached = 0;
            }
            else
            {
                countLocalsCached = localIndicesUsed.maxElement + 1;
            }
        }

        assert(!countLocalsCached.isNull);

        return countLocalsCached.get;
    }
}

alias BcInstr = SumType!(
    BcReturn,
    BcBoolConst,
    BcIntConst,
    BcStringConst,
    BcNullConst,
    BcObjectIdConst,
    BcIntAdd,
    BcIntSub,
    BcIntMultiply,
    BcEqual,
    BcAnd,
    BcIntNegate,
    BcWrapWithNullable,
    BcUnwrapNullable,
    BcGetInFrame,
    BcSetInFrame,
    BcGetInObject,
    BcGetInObjectFromStack,
    BcFnCall,
    BcCallFnIntrinsic,
    BcJump,
    BcJumpIfFalse,
    BcDuplicateTip,
);

// I am stuck between a rock, a hard place, and a tough spot:
//
// - If I allow default constructors, it becomes way too easy to make mistakes
//   and forget to pass in every field upon construction.
// - If I disable default constructors and force construction via an explicit
//   constructor that contains all fields, it messes with the BcInstr sum type.
//   Even if it were possible, it adds boilerplate that really shouldn't be
//   necessary.
// - So I made the default value of every field something obviously
//   human-written, to hopefully make it at least stand out in error messages
//   and failing tests.
//
// All of these options suck but the last one kind of finds an ok balance
// between:
// - preventing me from making mistakes
// - being concise
// - compiling without errors 😑

struct BcReturn { bool shouldReturnAValue = false; }
struct BcBoolConst { bool value = false; }
struct BcIntConst { long value = 123_456_789; }
struct BcStringConst { string value = "123_456_789"; }
struct BcNullConst {}
struct BcObjectIdConst { size_t value = 123_345_789; }
struct BcIntAdd {}
struct BcIntSub {}
struct BcIntMultiply {}
struct BcEqual {}
struct BcAnd {}
struct BcIntNegate {}
struct BcWrapWithNullable {}
struct BcUnwrapNullable {}
struct BcGetInFrame { size_t idxOfVariableInFrame = 123_456_789; }
struct BcSetInFrame { size_t idxOfVariableInFrame = 123_456_789; }
struct BcGetInObject { size_t objIdx = 123_456_789; size_t idxOfVariableInObject = 456_789_123; }
struct BcGetInObjectFromStack { size_t idxOfVariableInObject = 456_789_123; }
struct BcFnCall { size_t numArgs = 123_456_789; }
struct BcCallFnIntrinsic { size_t numArgs = 123_456_789; size_t intrinsicFunctionId; }
struct BcJump { ptrdiff_t offset = 123_456_789; }
struct BcJumpIfFalse { ptrdiff_t offset = 123_456_789; }
struct BcDuplicateTip {}
