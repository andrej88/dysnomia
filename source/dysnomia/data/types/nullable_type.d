module dysnomia.data.types.nullable_type;

import dysnomia.data.types;
import exceeds_expectations;


public class NullableType : Type
{
    Type payloadType;

    this(Type payloadType)
    {
        // TODO: what is the qualified name of an ad-hoc type such as `Int?`?
        string[] qualifiedName = payloadType.qualifiedName.dup;
        qualifiedName[$ - 1] ~= "?";
        super(qualifiedName);

        this.payloadType = payloadType;
    }

    override bool canReceive(Type other)
    {
        return
            payloadType.canReceive(other) ||
            (
                cast(NullableType) other !is null &&
                payloadType.canReceive((cast(NullableType) other).payloadType)
            ) ||
            super.canReceive(other);
    }

    override Type normalized()
    {
        if (NullableType nullablePayloadType = cast(NullableType) payloadType)
        {
            return payloadType.normalized();
        }
        else
        {
            return this;
        }
    }
}

@("normalized() — Flatten nested nullable types")
unittest
{
    Type typeFoo = new Type(["Foo"]);

    NullableType type = new NullableType(new NullableType(new NullableType(typeFoo)));
    Type result = type.normalized();
    expect(result).toEqual(new NullableType(typeFoo));
}

@("normalized() — Foo? becomes Foo?")
unittest
{
    Type typeFoo = new Type(["Foo"]);

    NullableType type = new NullableType(typeFoo);
    Type result = type.normalized();
    expect(result).toEqual(new NullableType(typeFoo));
}
