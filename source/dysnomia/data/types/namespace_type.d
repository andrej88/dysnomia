module dysnomia.data.types.namespace_type;

import dysnomia.data.types;


public final class NamespaceType : Type
{
    this(string[] qualifiedName)
    {
        super(qualifiedName);
    }
}
