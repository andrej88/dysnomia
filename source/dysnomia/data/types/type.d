module dysnomia.data.types.type;

import dysnomia.data.types;
import std.string;


public class Type
{
    string[] qualifiedName;

    this(string[] qualifiedName)
    {
        this.qualifiedName = qualifiedName;
    }

    bool canReceive(Type other)
    {
        return
            other == this ||
            other == typeNever ||
            (this == typeAny && (cast(NullableType) other) is null);
    }

    Type normalized() {
        return this;
    }

    override bool opEquals(Object other)
    const
    {
        if (other is null)
            return false;

        if (cast(typeof(this)) other is null)
            return false;

        return this.qualifiedName == (cast(typeof(this)) other).qualifiedName;
    }

    override int opCmp(Object other) const
    {
        if (this.toString() < other.toString()) return -1;
        if (this.toString() == other.toString()) return 0;
        return 1;
    }

    override size_t toHash()
    const @nogc @safe pure nothrow
    {
        assert(false, "Type.toHash not implemented");
    }

    override string toString()
    const @safe pure nothrow
    {
        return qualifiedName.join(".");
    }
}
