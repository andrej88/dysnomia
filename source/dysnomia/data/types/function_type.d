module dysnomia.data.types.function_type;

import dysnomia.data.types;


public class FunctionType : Type
{
    Type returnType;
    Type[] parameterTypes;

    this(Type returnType, Type[] parameterTypes)
    {
        this.returnType = returnType;
        this.parameterTypes = parameterTypes;

        // TODO: better notation that includes parameters. Should be parseable as a valid TypeExpression.
        super("Function(" ~ returnType.qualifiedName ~ ")");
    }
}
