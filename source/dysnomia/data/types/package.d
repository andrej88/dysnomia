module dysnomia.data.types;

public import dysnomia.data.types.function_type;
public import dysnomia.data.types.namespace_type;
public import dysnomia.data.types.nullable_type;
public import dysnomia.data.types.type;
public import dysnomia.data.types.union_type;


public static Type typeBool;
public static Type typeInt;
public static Type typeString;
public static Type typeVoid;
public static Type typeNever;
public static Type typeAny;
public static Type typeNull;

static this()
{
    typeBool = new Type(["Bool"]);
    typeInt = new Type(["Int"]);
    typeString = new Type(["String"]);
    typeVoid = new Type(["Void"]);
    typeNever = new Type(["Never"]);
    typeAny = new Type(["Any"]);
    typeNull = new NullableType(typeNever);
}
