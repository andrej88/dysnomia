module dysnomia.data.types.union_type;

import dysnomia.data.types;
import exceeds_expectations;
import std.algorithm;
import std.array;
import std.range;
import dysnomia.util.sumtype;


public class UnionType : Type
{
    Type[] payloadTypes;

    this(Type[] payloadTypes)
    {
        string[] payloadQualifiedNames = payloadTypes.map!(it => it.toString).array.sort.array;
        string qualifiedName = "(" ~ payloadQualifiedNames.join(" | ") ~ ")";
        super([qualifiedName]);

        this.payloadTypes = payloadTypes.sort.array;
    }

    override bool canReceive(Type other)
    {
        if (cast(UnionType) other)
        {
            UnionType otherUnionType = cast(UnionType) other;
            auto intersection = payloadTypes.setIntersection(otherUnionType.payloadTypes);

            return (intersection.walkLength == otherUnionType.payloadTypes.length);
        }
        else
        {
            return payloadTypes.canFind(other) || super.canReceive(other);
        }
    }

    override Type normalized()
    {
        bool hasNullables = payloadTypes.canFind!((Type e) => (cast(NullableType) e) !is null);

        Type[] unNullabledPayloads =
            payloadTypes
            .map!((Type type) {
                if (NullableType nullableType = cast(NullableType) type)
                {
                    return nullableType.payloadType;
                }
                else
                {
                    return type;
                }
            })
            .array;

        Type[] flattenedPayloads =
            unNullabledPayloads
            .map!(type => type.normalized())
            .fold!((acc, type) {
                if (UnionType unionType = cast(UnionType) type)
                {
                    return acc ~ unionType.payloadTypes;
                }
                else
                {
                    return acc ~ type;
                }
            })(cast(Type[]) []);

        Type[] deduplicatedPayloads =
            flattenedPayloads
            .fold!((acc, e) {
                if (acc.canFind!((a, b) => a.canReceive(b))(e))
                {
                    return acc;
                }
                else
                {
                    return acc ~ e;
                }
            })(cast(Type[]) []);

        // (0, _)      ->  typeNever        // can never be (0, true) because if it had nullable types, then it had types, so it can't be 0.
        // (1, true)   ->  Nullable(Foo)
        // (1, false)  ->  Foo
        // (_, true)   ->  (Foo | Bar)
        // (_, false)  ->  (Foo | Bar)?
        switch (deduplicatedPayloads.length)
        {
            case 0:
                return typeNever;

            case 1:
                if (hasNullables)
                    return new NullableType(deduplicatedPayloads[0]);
                else
                    return deduplicatedPayloads[0];

            default: {
                if (hasNullables)
                    return new NullableType(new UnionType(deduplicatedPayloads));
                else
                    return new UnionType(deduplicatedPayloads);
            }
        }
    }
}

@("normalized() — An empty union type degenerates to Never")
unittest
{
    UnionType type = new UnionType([]);
    Type result = type.normalized();
    expect(result).toEqual(typeNever);
}

@("normalized() — A union type of one type degenerates to that type")
unittest
{
    Type typeFoo = new Type(["Foo"]);

    UnionType type = new UnionType([typeFoo]);
    Type result = type.normalized();
    expect(result).toEqual(typeFoo);
}

@("normalized() — A union type of one nullable type degenerates to that type")
unittest
{
    Type typeFoo = new Type(["Foo"]);

    UnionType type = new UnionType([new NullableType(typeFoo)]);
    Type result = type.normalized();
    expect(result).toEqual(new NullableType(typeFoo));
}

@("normalized() — A union type containing only `Never?` degenerates to `Never?`")
unittest
{
    UnionType type = new UnionType([new NullableType(typeNever)]);
    Type result = type.normalized();
    expect(result).toEqual(new NullableType(typeNever));
}

@("normalized() — (Foo | Bar) remains (Foo | Bar)")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, typeBar]);
    Type result = type.normalized();
    expect(result).toEqual(new UnionType([typeFoo, typeBar]));
}

@("normalized() — (Foo | Bar | Foo) becomes (Foo | Bar)")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, typeBar, typeFoo]);
    Type result = type.normalized();
    expect(result).toEqual(new UnionType([typeFoo, typeBar]));
}

@("normalized() — (Foo | Foo) becomes Foo")
unittest
{
    Type typeFoo = new Type(["Foo"]);

    UnionType type = new UnionType([typeFoo, typeFoo]);
    Type result = type.normalized();
    expect(result).toEqual(typeFoo);
}

@("normalized() — (Foo | Bar | Never) becomes (Foo | Bar)")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, typeBar, typeNever]);
    Type result = type.normalized();
    expect(result).toEqual(new UnionType([typeFoo, typeBar]));
}

@("normalized() — (Foo | Bar | Never?) becomes (Foo | Bar)?")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, typeBar, new NullableType(typeNever)]);
    Type result = type.normalized();
    expect(result).toEqual(new NullableType(new UnionType([typeFoo, typeBar])));
}

@("normalized() — (Foo | Bar?) becomes (Foo | Bar)?")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, new NullableType(typeBar)]);
    Type result = type.normalized();
    expect(result).toEqual(
        new NullableType(
            new UnionType([typeFoo, typeBar])
        )
    );
}

@("normalized() — (Foo | Bar | Foo?) becomes (Foo | Bar)?")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([typeFoo, typeBar, new NullableType(typeFoo)]);
    Type result = type.normalized();
    expect(result).toEqual(
        new NullableType(
            new UnionType([typeFoo, typeBar])
        )
    );
}

@("normalized() — ((Foo | Bar) | Foo | (Bar | Caz)) becomes (Foo | Bar | Caz)")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);
    Type typeCaz = new Type(["Caz"]);

    UnionType type = new UnionType([new UnionType([typeFoo, typeBar]), typeFoo, new UnionType([typeBar, typeCaz])]);
    Type result = type.normalized();
    expect(result).toEqual(new UnionType([typeFoo, typeBar, typeCaz]));
}

@("normalized() — ((Foo | (Bar | Foo?))) becomes (Foo | Bar)?")
unittest
{
    Type typeFoo = new Type(["Foo"]);
    Type typeBar = new Type(["Bar"]);

    UnionType type = new UnionType([new UnionType([typeFoo, new UnionType([typeBar, new NullableType(typeFoo)])])]);
    Type result = type.normalized();
    expect(result).toEqual(
        new NullableType(
            new UnionType([typeFoo, typeBar])
        )
    );
}
