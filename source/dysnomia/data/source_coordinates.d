module dysnomia.data.source_coordinates;

import std.conv;


struct SourceCoordinates
{
    string path;
    int line;           /// 1-based
    int column;         /// 1-based, code point offset (NOT byte offset)

    invariant(
        line > 0,
        "Lines are 1-indexed, but line is " ~ line.to!string,
    );

    invariant(
        column > 0,
        "Columns are 1-indexed, but column is " ~ column.to!string,
    );

    string toString() const @safe pure nothrow
    {
        return text(path, ":", line, ":", column);
    }
}
