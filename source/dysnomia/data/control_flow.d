module dysnomia.data.control_flow;


enum ubyte mayPass = 1;
enum ubyte mayReturn = 2;

enum ControlFlow
{
    notYetAnalyzed = 0,
    passes = mayPass,
    returns = mayReturn,
    mayPassOrReturn = mayPass | mayReturn,
}
