module dysnomia.data.common;

import ast = dysnomia.data.ast;
import cst = dysnomia.data.cst;
import dysnomia.data.source_coordinates;
import std.algorithm;
import std.array;
import std.conv;
import std.sumtype;
import std.traits;
import std.typecons;
import tk = dysnomia.data.tokens;


public string source(tk.Token token)
{
    return token.match!(
        token => token.source
    );
}

public string source(T)(T token)
if (tk.isAToken!T)
{
    return token.source;
}

public string source(T : Nullable!Payload, Payload)(T t)
{
    if (t.isNull)
    {
        return "";
    }

    return t.get.source;
}

public string source(T)(T t)
if (isArray!T)
{
    Appender!string result;

    foreach (element; t)
    {
        result.put(element.source);
    }

    return result.data;
}

public string source(T)(T t)
if (cst.isConcatenationNode!T)
{
    Appender!string result;

    static foreach (string fieldName; FieldNameTuple!T)
    {
        result.put(__traits(getMember, t, fieldName).source);
    }

    return result.data;
}

public string source(T)(T t)
if (cst.isAlternationNode!T)
{
    return t.alternations.match!(
        payload => payload.source
    );
}

public string source(T)(T t)
if (cst.isSeparatedRepetitionNode!T)
{
    return t.flatten.map!(t => t.source).join;
}

public string source(T)(T t)
if (ast.isConcatenationNode!T || ast.isAlternationNode!T)
{
    return t.cst.source;
}

public size_t start(tk.Token token)
{
    return token.match!(
        token => token.start
    );
}

public size_t start(T)(T token)
if (tk.isAToken!T)
{
    return token.start;
}

public size_t start(T)(T t)
if (cst.isConcatenationNode!T)
{
    // This foundStart is just a way to avoid the "unreachable code" error since
    // every "normal" field would generate a return statement
    bool foundStart;
    size_t theStart;

    static foreach (string fieldName; FieldNameTuple!T)
    {
        {
            alias FieldType = typeof(__traits(getMember, T, fieldName));
            static if(is(FieldType : Nullable!Payload, Payload))
            {
                if (!__traits(getMember, t, fieldName).isNull)
                {
                    foundStart = true;
                    theStart = __traits(getMember, t, fieldName).get.start;
                }
            }
            else static if (isArray!FieldType)
            {
                if (!__traits(getMember, t, fieldName).empty)
                {
                    foundStart = true;
                    theStart = __traits(getMember, t, fieldName)[0].start;
                }
            }
            else
            {
                foundStart = true;
                theStart = __traits(getMember, t, fieldName).start;
            }

            if (foundStart) return theStart;
        }
    }

    assert(false, "Did not find a start in this CST node: " ~ t.to!string);
}

public size_t start(T)(T t)
if (cst.isAlternationNode!T)
{
    return t.alternations.match!(
        payload => payload.start
    );
}

public size_t start(T)(T t)
if (cst.isSeparatedRepetitionNode!T)
{
    return t.elements[0].start;
}

public size_t start(T)(T t)
if (ast.isConcatenationNode!T || ast.isAlternationNode!T)
{
    return t.cst.start;
}

public size_t end(tk.Token token)
{
    return token.match!(
        token => token.end,
    );
}

public size_t end(T)(T token)
if (tk.isAToken!T)
{
    return token.end;
}

public size_t end(T)(T t)
if (cst.isConcatenationNode!T)
{
    bool foundEnd;
    size_t theEnd;

    static foreach_reverse (string fieldName; FieldNameTuple!T)
    {{
        alias FieldType = typeof(__traits(getMember, T, fieldName));
        static if(is(FieldType : Nullable!Payload, Payload))
        {
            if (!__traits(getMember, t, fieldName).isNull)
            {
                foundEnd = true;
                theEnd = __traits(getMember, t, fieldName).get.end;
            }
        }
        else static if (isArray!FieldType)
        {
            if (!__traits(getMember, t, fieldName).empty)
            {
                foundEnd = true;
                theEnd = __traits(getMember, t, fieldName)[$ - 1].end;
            }
        }
        else
        {
            foundEnd = true;
            theEnd = __traits(getMember, t, fieldName).end;
        }

        if (foundEnd) return theEnd;
    }}

    assert(false, "Did not find an end in this CST node: " ~ t.to!string);
}

public size_t end(T)(T t)
if (cst.isAlternationNode!T)
{
    return t.alternations.match!(
        payload => payload.end
    );
}

public size_t end(T)(T t)
if (cst.isAlternationNode!T)
{
    return t.elements[0].end;
}

public size_t end(T)(T t)
if (ast.isConcatenationNode!T || ast.isAlternationNode!T)
{
    return t.cst.end;
}

SourceCoordinates sourceCoordinates(tk.Token token)
{
    return token.match!(
        token => token.sourceCoordinates,
    );
}

SourceCoordinates sourceCoordinates(T)(T cstConcatenationNode)
if (cst.isConcatenationNode!T)
{
    bool foundSourceCoordinates;
    SourceCoordinates result;

    static foreach (string fieldName; FieldNameTuple!T)
    {
        {
            alias FieldType = typeof(__traits(getMember, T, fieldName));
            static if(is(FieldType : Nullable!Payload, Payload))
            {
                if (!__traits(getMember, cstConcatenationNode, fieldName).isNull)
                {
                    foundSourceCoordinates = true;
                    result = __traits(getMember, cstConcatenationNode, fieldName).get.sourceCoordinates;
                }
            }
            else static if (isArray!FieldType)
            {
                if (!__traits(getMember, cstConcatenationNode, fieldName).empty)
                {
                    foundSourceCoordinates = true;
                    result = __traits(getMember, cstConcatenationNode, fieldName)[0].sourceCoordinates;
                }
            }
            else
            {
                foundSourceCoordinates = true;
                result = __traits(getMember, cstConcatenationNode, fieldName).sourceCoordinates;
            }

            if (foundSourceCoordinates) return result;
        }
    }

    assert(false, "Did not find source coordinates in this CST node: " ~ cstConcatenationNode.to!string);
}

SourceCoordinates sourceCoordinates(T)(T cstAlternationNode)
if (cst.isAlternationNode!T)
{
    return cstAlternationNode.alternations.match!(
        payload => payload.sourceCoordinates,
    );
}

SourceCoordinates sourceCoordinates(T)(T cstSeparatedRepetitionNode)
if (cst.isSeparatedRepetitionNode!T)
{
    return cstSeparatedRepetitionNode.elements[0].sourceCoordinates;
}

SourceCoordinates sourceCoordinates(T)(T astNode)
if (ast.isConcatenationNode!T || ast.isAlternationNode!T)
{
    return astNode.cst.sourceCoordinates;
}
