/// These data structures represent the Abstract Syntax Tree. The AST
/// is similar to the CST, but only contains nodes necessary for the
/// remainder of the compiler's execution to complete. Most notably,
/// it's missing rules related to spacing, line breaks, comments, and
/// certain keywords (e.g. `let`), and certain symbols (e.g. "(").
///
/// For example, the AST for a typed variable definition contains only
/// the variable name, the type, and the expression used to initialize
/// it. In contrast, the CST contains all of that plus the `=` sign,
/// the `let` keyword, the `:` between the name and the type, and all
/// the spacing between each of those.
///
/// Despite all that, the AST can still be thought of as a "grammar",
/// much like the CST. An auto-generated grammar can be found in
/// `abstract_grammar.abnf` in the project root. It's written in the
/// same format as the concrete grammar and all of its quirks (see
/// `source/dysnomia/data/cst.d`).
module dysnomia.data.ast;

import cst = dysnomia.data.cst;
import dysnomia.data.control_flow;
import dysnomia.data.types;
import std.algorithm;
import std.conv;
import std.meta;
import std.range;
import std.sumtype;
import std.traits;
import std.typecons;
import tk = dysnomia.data.tokens;


private struct ConcatenationNode {}
private struct AlternationNode {}

private mixin template ConcatenationNodeCommons(_CstType)
if (cst.isCstNode!_CstType)
{
    alias CstType = _CstType;

    alias ChildrenAndCstFieldTypes = Erase!(ControlFlow, EraseAll!(Type, Fields!(typeof(this))));
    alias ChildrenFieldTypes = Erase!(CstType, ChildrenAndCstFieldTypes);

    enum childrenAndCstFieldNames = () {
        string[] result;

        static foreach (size_t idx, string fieldName; FieldNameTuple!(typeof(this)))
        {
            static if (!is(Fields!(typeof(this))[idx] : Type) && !is(Fields!(typeof(this))[idx] : ControlFlow))
            {
                result ~= fieldName;
            }
        }

        return result;
    }();

    enum childrenFieldNames = () {
        string[] result;

        static foreach (size_t idx, string fieldName; FieldNameTuple!(typeof(this)))
        {
            static if (fieldName != "cst" && !is(Fields!(typeof(this))[idx] == Type) && !is(Fields!(typeof(this))[idx] == ControlFlow))
            {
                result ~= fieldName;
            }
        }

        return result;
    }();

    static assert(allSatisfy!(isAstNode, ChildrenFieldTypes));

    CstType cst;

    this() {}

    this(ChildrenAndCstFieldTypes args)
    {
        static foreach (size_t idx, string fieldName; childrenAndCstFieldNames)
        {
            __traits(getMember, this, fieldName) = args[idx];
        }
    }

    override bool opEquals(Object other)
    {
        if (other is null) return false;
        if (cast(typeof(this)) other is null) return false;

        static foreach (string fieldName; FieldNameTuple!(typeof(this)))
        {
            if (__traits(getMember, this, fieldName) != __traits(getMember, cast(typeof(this)) other, fieldName))
            {
                return false;
            }
        }

        return true;
    }

    override size_t toHash() const
    {
        assert(false, "do not hash!");
    }
}

private mixin template AlternationNodeCommons(_CstType, Types...)
if (cst.isCstNode!_CstType)
{
    alias CstType = _CstType;

    CstType cst;

    SumType!(Types) alternations;

    static assert(allSatisfy!(isAstNode, Types));

    this(T)(CstType cst, T value)
    if (staticIndexOf!(T, Types) != -1)
    {
        this.cst = cst;
        alternations = value.to!(SumType!(Types));
    }

    override bool opEquals(Object other)
    {
        if (other is null) return false;
        if (cast(typeof(this)) other is null) return false;

        if (this.alternations != (cast(typeof(this)) other).alternations)
        {
            return false;
        }

        return true;
    }

    override size_t toHash() const
    {
        assert(false, "do not hash!");
    }
}

public template isAstNode(T)
{
    static if (tk.isAToken!T || isConcatenationNode!T || isAlternationNode!T)
    {
        enum bool isAstNode = true;
    }
    else static if (isArray!T)
    {
        enum bool isAstNode = isAstNode!(ElementType!T);
    }
    else static if (is(T : Nullable!Payload, Payload))
    {
        enum bool isAstNode = isAstNode!Payload;
    }
    else
    {
        enum bool isAstNode = false;
    }
}

public template isConcatenationNode(T)
{
    static if (is(T == class))
    {
        enum bool isConcatenationNode = hasUDA!(T, ConcatenationNode);
    }
    else
    {
        enum bool isConcatenationNode = false;
    }
}

public template isAlternationNode(T)
{
    static if (is(T == class) && is(typeof(T.alternations) : SumType!Payload, Payload))
    {
        enum bool isAlternationNode = hasUDA!(T, AlternationNode);
    }
    else
    {
        enum bool isAlternationNode = false;
    }
}

@ConcatenationNode
public class Namespace
{
    mixin ConcatenationNodeCommons!(cst.Namespace);

    NamespaceDeclaration namespaceDeclaration;
    NamespaceBody namespaceBody;
}

@ConcatenationNode
public class NamespaceDeclaration
{
    mixin ConcatenationNodeCommons!(cst.NamespaceDeclaration);

    tk.Identifier[] namespaceName;
}

@ConcatenationNode
public class NamespaceBody
{
    mixin ConcatenationNodeCommons!(cst.NamespaceBody);

    NamespaceItem[] namespaceItems;
}

@AlternationNode
public class NamespaceItem
{
    mixin AlternationNodeCommons!(
        cst.NamespaceItem,
        FunctionDeclaration,
        ImportDeclaration,
    );
}

@ConcatenationNode
public class ImportDeclaration
{
    mixin ConcatenationNodeCommons!(cst.ImportDeclaration);

    tk.Identifier[] namespaceName;
}

@ConcatenationNode
public class FunctionDeclaration
{
    mixin ConcatenationNodeCommons!(cst.FunctionDeclaration);

    tk.Identifier name;
    Parameter[] parameters;
    Nullable!TypeExpression returnTypeExpression;
    FunctionBody functionBody;
}

@ConcatenationNode
public class Parameter
{
    mixin ConcatenationNodeCommons!(cst.Parameter);

    tk.Identifier name;
    TypeExpression typeExpression;
}

@AlternationNode
public class FunctionBody
{
    mixin AlternationNodeCommons!(
        cst.FunctionBody,
        Statement[],
        tk.KeywordIntrinsic,
    );
}

@AlternationNode
public class Statement
{
    mixin AlternationNodeCommons!(
        cst.Statement,
        TypedVariableDefinition,
        Expression,
    );

    ControlFlow controlFlow()
    {
        return this.alternations.match!(
            (e) => e.controlFlow,
        );
    }
}

@ConcatenationNode
public class TypedVariableDefinition
{
    mixin ConcatenationNodeCommons!(cst.TypedVariableDefinition);

    Type declaredType;

    tk.Identifier name;
    TypeExpression typeExpression;
    Expression initializer;

    ControlFlow controlFlow()
    {
        return initializer.controlFlow;
    }
}

@AlternationNode
public class Expression
{
    mixin AlternationNodeCommons!(
        cst.Expression,
        UnaryExpression,
        SumExpression,
        ProductExpression,
        ComparisonExpression,
        LogicalExpression,
        NullCoalescingExpression,
    );

    Type type()
    {
        return this.alternations.match!(
            (e) => e.type
        );
    }

    ControlFlow controlFlow()
    {
        return this.alternations.match!(
            (e) => e.controlFlow,
        );
    }
}

@ConcatenationNode
public class SumExpression
{
    mixin ConcatenationNodeCommons!(cst.Expression);
    invariant(tail.length != 0);

    Type type;

    UnaryExpression head;
    SumExpressionSegment[] tail;

    UnaryExpression[] operands()
    {
        return head ~ tail.map!(tailSegment => tailSegment.operand).array;
    }

    ControlFlow controlFlow()
    {
        if (head.controlFlow == ControlFlow.returns)
        {
            return ControlFlow.returns;
        }

        foreach (SumExpressionSegment tailSegment; tail)
        {
            if (tailSegment.operand.controlFlow == ControlFlow.returns)
            {
                return ControlFlow.returns;
            }
        }

        return ControlFlow.passes;
    }
}

@ConcatenationNode
public class SumExpressionSegment
{
    mixin ConcatenationNodeCommons!(cst.SumExpressionSegment);

    SumOperator operator;
    UnaryExpression operand;
}

@AlternationNode
public class SumOperator
{
    mixin AlternationNodeCommons!(
        cst.SumOperator,
        tk.SymbolPlus,
        tk.SymbolMinus,
    );
}

@ConcatenationNode
public class ProductExpression
{
    mixin ConcatenationNodeCommons!(cst.Expression);
    invariant(tail.length != 0);

    Type type;

    UnaryExpression head;
    ProductExpressionSegment[] tail;

    UnaryExpression[] operands()
    {
        return head ~ tail.map!(tailSegment => tailSegment.operand).array;
    }

    ControlFlow controlFlow()
    {
        if (head.controlFlow == ControlFlow.returns)
        {
            return ControlFlow.returns;
        }

        foreach (ProductExpressionSegment tailSegment; tail)
        {
            if (tailSegment.operand.controlFlow == ControlFlow.returns)
            {
                return ControlFlow.returns;
            }
        }

        return ControlFlow.passes;
    }
}

@ConcatenationNode
public class ProductExpressionSegment
{
    mixin ConcatenationNodeCommons!(cst.ProductExpressionSegment);

    ProductOperator operator;
    UnaryExpression operand;
}

@AlternationNode
public class ProductOperator
{
    mixin AlternationNodeCommons!(
        cst.ProductOperator,
        tk.SymbolAsterisk,
    );
}

@ConcatenationNode
public class ComparisonExpression
{
    mixin ConcatenationNodeCommons!(cst.Expression);
    invariant(tail.length != 0);

    Type type;

    UnaryExpression head;
    ComparisonExpressionSegment[] tail;

    UnaryExpression[] operands()
    {
        return head ~ tail.map!(tailSegment => tailSegment.operand).array;
    }

    ControlFlow controlFlow()
    {
        // This assumes these expressions can short circuit.

        ControlFlow result = ControlFlow.notYetAnalyzed;
        result |= head.controlFlow;

        foreach (ComparisonExpressionSegment tailSegment; tail)
        {
            result |= tailSegment.operand.controlFlow;
        }

        return result;
    }
}

@ConcatenationNode
public class ComparisonExpressionSegment
{
    mixin ConcatenationNodeCommons!(cst.ComparisonExpressionSegment);

    ComparisonOperator operator;
    UnaryExpression operand;
}

@AlternationNode
public class ComparisonOperator
{
    mixin AlternationNodeCommons!(
        cst.ComparisonOperator,
        tk.SymbolDoubleEquals,
    );
}

@ConcatenationNode
public class LogicalExpression
{
    mixin ConcatenationNodeCommons!(cst.Expression);
    invariant(tail.length != 0);

    Type type;

    UnaryExpression head;
    LogicalExpressionSegment[] tail;

    UnaryExpression[] operands()
    {
        return head ~ tail.map!(tailSegment => tailSegment.operand).array;
    }

    ControlFlow controlFlow()
    {
        // This assumes these expressions can short circuit.

        ControlFlow result = ControlFlow.notYetAnalyzed;
        result |= head.controlFlow;

        foreach (LogicalExpressionSegment tailSegment; tail)
        {
            result |= tailSegment.operand.controlFlow;
        }

        return result;
    }
}

@ConcatenationNode
public class LogicalExpressionSegment
{
    mixin ConcatenationNodeCommons!(cst.LogicalExpressionSegment);

    LogicalOperator operator;
    UnaryExpression operand;
}

@AlternationNode
public class LogicalOperator
{
    mixin AlternationNodeCommons!(
        cst.LogicalOperator,
        tk.SymbolAmpersand,
    );
}

@ConcatenationNode
public class NullCoalescingExpression
{
    mixin ConcatenationNodeCommons!(cst.Expression);
    invariant(tail.length != 0);

    Type type;

    UnaryExpression head;
    NullCoalescingExpressionSegment[] tail;

    UnaryExpression[] operands()
    {
        return head ~ tail.map!(tailSegment => tailSegment.operand).array;
    }

    ControlFlow controlFlow()
    {
        ControlFlow result = ControlFlow.notYetAnalyzed;
        result |= head.controlFlow;

        foreach (NullCoalescingExpressionSegment tailSegment; tail)
        {
            result |= tailSegment.operand.controlFlow;
        }

        return result;
    }
}

@ConcatenationNode
public class NullCoalescingExpressionSegment
{
    mixin ConcatenationNodeCommons!(cst.NullCoalescingExpressionSegment);

    NullCoalescingOperator operator;
    UnaryExpression operand;
}

@AlternationNode
public class NullCoalescingOperator
{
    mixin AlternationNodeCommons!(
        cst.NullCoalescingOperator,
        tk.SymbolDoubleQuestionMark,
    );
}

@ConcatenationNode
public class UnaryExpression
{
    mixin ConcatenationNodeCommons!(cst.UnaryExpression);

    Type type;

    Nullable!PrefixOperator prefixOperator;
    AtomicExpression operand;
    PostfixOperator[] postfixOperators;

    ControlFlow controlFlow()
    {
        if (operand.controlFlow == ControlFlow.returns)
        {
            return ControlFlow.returns;
        }

        foreach (PostfixOperator postfixOp; postfixOperators)
        {
            if (postfixOp.controlFlow == ControlFlow.returns)
            {
                return ControlFlow.returns;
            }
        }

        return ControlFlow.passes;
    }
}

@AlternationNode
public class PrefixOperator
{
    mixin AlternationNodeCommons!(
        cst.PrefixOp,
        tk.SymbolMinus,
    );
}

@AlternationNode
public class PostfixOperator
{
    mixin AlternationNodeCommons!(
        cst.PostfixOp,
        ArgumentBlock,
        MemberAccess,
    );

    ControlFlow controlFlow()
    {
        return this.alternations.match!(
            (e) => e.controlFlow,
        );
    }
}

@AlternationNode
public class AtomicExpression
{
    mixin AlternationNodeCommons!(
        cst.AtomicExpression,
        Expression,
        tk.Boolean,
        tk.Integer,
        String,
        tk.Null,
        IfElseExpression,
        ReturnExpression,
        tk.Identifier,
    );

    Type type;

    ControlFlow controlFlow()
    {
        return this.alternations.match!(
            (tk.Boolean _) => ControlFlow.passes,
            (tk.Integer _) => ControlFlow.passes,
            (tk.Null _) => ControlFlow.passes,
            (tk.Identifier _) => ControlFlow.passes,
            (e) => e.controlFlow,
        );
    }
}

@ConcatenationNode
public class String
{
    mixin ConcatenationNodeCommons!(cst.String);

    tk.StringPlaintext str;

    ControlFlow controlFlow = ControlFlow.passes;
}

@ConcatenationNode
public class IfElseExpression
{
    mixin ConcatenationNodeCommons!(cst.IfElseExpression);

    Type type;

    Expression condition;
    Statement[] thenBlock;
    Nullable!(IfElseExpressionTail) tail;

    ControlFlow controlFlow()
    {
        ControlFlow result = ControlFlow.notYetAnalyzed;

        foreach (Statement thenStatement; thenBlock)
        {
            if (thenStatement.controlFlow == ControlFlow.returns)
            {
                result |= ControlFlow.returns;
            }
        }

        if (result == ControlFlow.notYetAnalyzed)
        {
            result = ControlFlow.passes;
        }

        if (!tail.isNull)
        {
            result |= tail.get.controlFlow;
        }

        return result;
    }
}

@ConcatenationNode
public class IfElseExpressionTail
{
    mixin ConcatenationNodeCommons!(cst.IfElseExpressionTail);

    Statement[] elseBlock;

    ControlFlow controlFlow()
    {
        ControlFlow result = ControlFlow.notYetAnalyzed;

        foreach (Statement elseStatement; elseBlock)
        {
            if (elseStatement.controlFlow == ControlFlow.returns)
            {
                result |= ControlFlow.returns;
            }
        }

        return result;
    }
}

@ConcatenationNode
public class ReturnExpression
{
    mixin ConcatenationNodeCommons!(cst.ReturnExpression);

    Nullable!Expression payload;

    ControlFlow controlFlow = ControlFlow.returns;
}

@ConcatenationNode
public class ArgumentBlock
{
    mixin ConcatenationNodeCommons!(cst.ArgumentBlock);

    Expression[] arguments;

    ControlFlow controlFlow()
    {
        foreach (Expression argument; arguments)
        {
            if (argument.controlFlow == ControlFlow.returns)
            {
                return ControlFlow.returns;
            }
        }

        return ControlFlow.passes;
    }
}

@ConcatenationNode
public class MemberAccess
{
    mixin ConcatenationNodeCommons!(cst.MemberAccess);

    tk.Identifier identifier;

    // TODO: Once this is implemented in the bytecode, double check this.
    ControlFlow controlFlow = ControlFlow.passes;
}


// Types

@ConcatenationNode
class TypeExpression
{
    mixin ConcatenationNodeCommons!(cst.TypeExpression);

    AtomicTypeExpression payload;
    Nullable!TypePostfix postfix;
}

@AlternationNode
class AtomicTypeExpression
{
    mixin AlternationNodeCommons!(
        cst.AtomicTypeExpression,
        tk.Identifier,
    );
}

@ConcatenationNode
class TypePostfix
{
    mixin ConcatenationNodeCommons!(cst.TypePostfix);

    TypePostfixOp postfixOperator;
}

@AlternationNode
class TypePostfixOp
{
    mixin AlternationNodeCommons!(
        cst.TypePostfixOp,
        tk.SymbolQuestionMark,
    );
}
