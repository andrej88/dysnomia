module dysnomia.data.intrinsics;


size_t getIntrinsicFunctionId(string name)
{
    return name.hashOf();
}
