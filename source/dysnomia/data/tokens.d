module dysnomia.data.tokens;

import colorize;
import dysnomia.data.source_coordinates;
import std.conv;
import std.meta;
import std.string;
import std.sumtype;


/// A sum type of all token types. All token types have a `source`
/// field, containg the slice of the source the token is based on, as
/// well as `start` and `end` fields that indicate the start
/// (inclusive) and end (exclusive) byte offsets of the token in the
/// source.
alias Token = SumType!(
    Identifier,
    Boolean,
    Integer,
    Null,
    StringPlaintext,
    CommentPlaintext,

    KeywordFn,
    KeywordIf,
    KeywordElse,
    KeywordLet,
    KeywordReturn,
    KeywordNamespace,
    KeywordImport,
    KeywordIntrinsic,

    SymbolBraceLeft,
    SymbolBraceRight,
    SymbolColon,
    SymbolComma,
    SymbolEquals,
    SymbolParenLeft,
    SymbolParenRight,
    SymbolQuotes,
    SymbolSemicolon,
    SymbolPlus,
    SymbolMinus,
    SymbolAsterisk,
    SymbolDoubleEquals,
    SymbolDot,
    SymbolAmpersand,
    SymbolDoubleQuestionMark,
    SymbolQuestionMark,

    HSpace,
    VSpace,

    EndOfInput,
);

enum bool isAToken(T) = staticIndexOf!(T, Token.Types) != -1;

public string singularName(TypeInfo tokenTypeInfo)
in (
    tokenTypeInfo.to!string.startsWith("dysnomia.data.tokens."),
    "singularName received " ~ tokenTypeInfo.to!string
)
{
    string tokenLocalName = tokenTypeInfo.to!string["dysnomia.data.tokens.".length .. $];
    switch (tokenLocalName)
    {
        case "Identifier":               return `an identifier`;
        case "Boolean":                  return `a boolean`;
        case "Integer":                  return `an integer`;
        case "Null":                     return `null`;
        case "StringPlaintext":          return `string plaintext`;
        case "CommentPlaintext":         return `comment plaintext`;
        case "KeywordFn":                return `fn`.color(mode.bold);
        case "KeywordIf":                return `if`.color(mode.bold);
        case "KeywordElse":              return `else`.color(mode.bold);
        case "KeywordLet":               return `let`.color(mode.bold);
        case "KeywordReturn":            return `return`.color(mode.bold);
        case "KeywordNamespace":         return `namespace`.color(mode.bold);
        case "KeywordImport":            return `import`.color(mode.bold);
        case "KeywordIntrinsic":         return `intrinsic`.color(mode.bold);
        case "SymbolBraceLeft":          return `{`.color(mode.bold);
        case "SymbolBraceRight":         return `}`.color(mode.bold);
        case "SymbolColon":              return `:`.color(mode.bold);
        case "SymbolComma":              return `,`.color(mode.bold);
        case "SymbolEquals":             return `=`.color(mode.bold);
        case "SymbolParenLeft":          return `(`.color(mode.bold);
        case "SymbolParenRight":         return `)`.color(mode.bold);
        case "SymbolQuotes":             return `"`.color(mode.bold);
        case "SymbolSemicolon":          return `;`.color(mode.bold);
        case "SymbolPlus":               return `+`.color(mode.bold);
        case "SymbolMinus":              return `-`.color(mode.bold);
        case "SymbolAsterisk":           return `*`.color(mode.bold);
        case "SymbolDoubleEquals":       return `==`.color(mode.bold);
        case "SymbolDot":                return `.`.color(mode.bold);
        case "SymbolAmpersand":          return `&`.color(mode.bold);
        case "SymbolDoubleQuestionMark": return `??`.color(mode.bold);
        case "SymbolQuestionMark":       return `?`.color(mode.bold);
        case "HSpace":                   return `horizontal spacing`;
        case "VSpace":                   return `vertical spacing`;
        case "EndOfInput":               return `the end of the input`;
        default:                         assert(false, "No matching branch for token type " ~ tokenLocalName);
    }
}

private mixin template TokenCommons()
{
    string source;
    SourceCoordinates sourceCoordinates;
    size_t start;       /// Inclusive, 0-based, byte offset
    size_t end;         /// Exclusive, 0-based, byte offset

    invariant(
        source.length == end - start,
        "Token source length mismatch: \"" ~ source ~
        "\" has length "  ~ source.length.to!string ~
        " but (end - start) = " ~ (end - start).to!string
    );

    static Token make(string source, SourceCoordinates sourceCoordinates, size_t start, size_t end)
    {
        return typeof(this)(source, sourceCoordinates, start, end).to!Token;
    }
}

// General
struct Identifier { mixin TokenCommons; }
struct Boolean { mixin TokenCommons; }
struct Integer { mixin TokenCommons; }
struct Null { mixin TokenCommons; }
struct StringPlaintext { mixin TokenCommons; }
struct CommentPlaintext { mixin TokenCommons; }

// Keywords
struct KeywordFn { mixin TokenCommons; }
struct KeywordIf { mixin TokenCommons; }
struct KeywordElse { mixin TokenCommons; }
struct KeywordLet { mixin TokenCommons; }
struct KeywordReturn { mixin TokenCommons; }
struct KeywordNamespace { mixin TokenCommons; }
struct KeywordImport { mixin TokenCommons; }
struct KeywordIntrinsic { mixin TokenCommons; }

// Symbols
struct SymbolBraceLeft { mixin TokenCommons; }
struct SymbolBraceRight { mixin TokenCommons; }
struct SymbolColon { mixin TokenCommons; }
struct SymbolComma { mixin TokenCommons; }
struct SymbolEquals { mixin TokenCommons; }
struct SymbolParenLeft { mixin TokenCommons; }
struct SymbolParenRight { mixin TokenCommons; }
struct SymbolQuotes { mixin TokenCommons; }
struct SymbolSemicolon { mixin TokenCommons; }
struct SymbolPlus { mixin TokenCommons; }
struct SymbolMinus { mixin TokenCommons; }
struct SymbolAsterisk { mixin TokenCommons; }
struct SymbolDoubleEquals { mixin TokenCommons; }
struct SymbolDot { mixin TokenCommons; }
struct SymbolAmpersand { mixin TokenCommons; }
struct SymbolDoubleQuestionMark { mixin TokenCommons; }
struct SymbolQuestionMark { mixin TokenCommons; }

// Spacing
struct HSpace
{
    mixin TokenCommons;

    invariant(
        source.length != 0 || this == HSpace.init,
        "HSpace token must not be empty: " ~ this.to!string,
    );
}

struct VSpace { mixin TokenCommons; }
struct EndOfInput
{
    mixin TokenCommons;

    invariant(
        source.length == 0 && start == end,
        "EndOfInput token must not contain content: " ~ this.to!string,
    );
}
