/// The data structures used to represent the Concrete Syntax Tree
/// also represent the language's grammar.
///
/// A CST leaf = a grammar terminal = a Token ([dysnomia.data.tokens])
///
/// A CST non-leaf node = a grammar rule = either:
/// - A class mixing in ConcatenationNodeCommons
/// - A class mixing in AlternationNodeCommons
///
/// An auto-generated grammar can be found in `concrete_grammar.abnf`
/// in the project root. It's written in an ABNF-ish format, with some
/// modifications:
/// - Repetitions must all be zero-or-more.
/// - A rule must be either a simple alternation or a simple
///   concatenation.
/// - Avoid groups, divide a rule into smaller rules instead.
/// - Do not use any terminal values defined in the ABNF standard
///   (literal strings, characters, ranges, etc.). Instead, use Tokens
///   where applicable.
/// - Token rules should begin with tk, and aren't defined in the
///   grammar since the lexing phase happens before the parsing.
///
///
/// The grammar is also a bit simplified to make it easier to
/// represent using D data structures. All rules must be either pure
/// alternations or concatenations of other rules — keep each rule
/// minimal.
module dysnomia.data.cst;

import dysnomia.data.common;
import std.algorithm;
import std.conv;
import std.meta;
import std.range;
import std.sumtype;
import std.traits;
import std.typecons;
import tk = dysnomia.data.tokens;


tk.Token[] flatten(T : Nullable!Payload, Payload)(T t)
{
    if (t.isNull)
    {
        return [];
    }

    return t.get.flatten();
}

tk.Token[] flatten(T)(T t)
if (isArray!T)
{
    Appender!(tk.Token[]) result;

    foreach (element; t)
    {
        result.put(element.flatten());
    }

    return result.data;
}

tk.Token[] flatten(T)(T t)
if (tk.isAToken!T)
{
    return [t.to!(tk.Token)];
}

private struct ConcatenationNode {}
private struct AlternationNode {}
private struct SeparatedRepetitionNode {}

public template isCstNode(T)
{
    static if (tk.isAToken!T || isConcatenationNode!T || isAlternationNode!T || isSeparatedRepetitionNode!T)
    {
        enum bool isCstNode = true;
    }
    else static if (isArray!T)
    {
        enum bool isCstNode = isCstNode!(ElementType!T);
    }
    else static if (is(T : Nullable!Payload, Payload))
    {
        enum bool isCstNode = isCstNode!Payload;
    }
    else
    {
        enum bool isCstNode = false;
    }
}

template isConcatenationNode(T)
{
    static if (
        is(T == class) &&
        !isAlternationNode!T &&
        !isSeparatedRepetitionNode!T
    )
    {
        enum bool isConcatenationNode = allSatisfy!(isCstNode, Fields!T) && hasUDA!(T, ConcatenationNode);
    }
    else
    {
        enum bool isConcatenationNode = false;
    }
}

template isAlternationNode(T)
{
    static if (
        is(T == class) &&
        __traits(hasMember, T, "alternations") &&
        is(typeof(T.alternations) : SumType!Types, Types) &&
        FieldNameTuple!T.length == 1
    )
    {
        enum bool isAlternationNode = hasUDA!(T, AlternationNode);
    }
    else
    {
        enum bool isAlternationNode = false;
    }
}

template isSeparatedRepetitionNode(T)
{
    enum bool isSeparatedRepetitionNode = hasUDA!(T, SeparatedRepetitionNode);
}

private mixin template ConcatenationNodeCommons()
{
    alias ChildrenFieldTypes = Fields!(typeof(this));

    static assert(allSatisfy!(isCstNode, ChildrenFieldTypes));

    enum childrenFieldNames = FieldNameTuple!(typeof(this));

    invariant {
        static foreach (size_t idx, string fieldName; FieldNameTuple!(typeof(this)))
        {
            static if (is(Fields!this[idx] == class))
            {
                assert(__traits(getMember, this, fieldName) != null);
            }
        }
    }

    this() {}

    this(Fields!(typeof(this)) args)
    {
        static foreach (size_t idx, string fieldName; FieldNameTuple!(typeof(this)))
        {
            __traits(getMember, this, fieldName) = args[idx];
        }
    }

    tk.Token[] flatten()
    {
        Appender!(tk.Token[]) result;

        static foreach (string fieldName; FieldNameTuple!(typeof(this)))
        {
            result.put(__traits(getMember, this, fieldName).flatten);
        }

        return result.data;
    }

    override bool opEquals(Object other)
    {
        if (other is null) return false;
        if (cast(typeof(this)) other is null) return false;

        static foreach (string fieldName; FieldNameTuple!(typeof(this)))
        {
            if (__traits(getMember, this, fieldName) != __traits(getMember, cast(typeof(this)) other, fieldName))
            {
                return false;
            }
        }

        return true;
    }

    override size_t toHash() const
    {
        assert(false, "do not hash!");
    }
}

private mixin template AlternationNodeCommons(Types...)
{
    // Sadly, this mixin template cannot have a match() method because it causes
    // an error about dual-context functions. The solution is to access the
    // alternations field.

    SumType!(Types) alternations;

    this(T)(T value)
    if (staticIndexOf!(T, Types) != -1)
    {
        alternations = value.to!(SumType!(Types));
    }

    tk.Token[] flatten()
    {
        return alternations.match!(
            payload => payload.flatten
        );
    }

    override bool opEquals(Object other)
    {
        if (other is null) return false;
        if (cast(typeof(this)) other is null) return false;

        if (this.alternations != (cast(typeof(this)) other).alternations)
        {
            return false;
        }

        return true;
    }

    override size_t toHash() const
    {
        assert(false, "do not hash!");
    }
}

private mixin template SeparatedRepetitionNodeCommons(Element, Separator)
{
    static assert(isCstNode!Element);
    static assert(isCstNode!Separator);

    alias ElementType = Element;
    alias SeparatorType = Separator;

    Element[] elements;
    Separator[] separators;

    invariant(
        elements.length == separators.length + 1,
        "SeparatedRepetitionNode instances should have one separator between each element. Received " ~
        elements.length.to!string ~ " elements but " ~
        separators.length.to!string ~ " separators instead of the expected " ~
        (separators.length.to!int - 1).to!string ~ ".",
    );

    enum childrenFieldNames = FieldNameTuple!(typeof(this));

    this(Element[] elements, Separator[] separators)
    {
        this.elements = elements;
        this.separators = separators;
    }

    tk.Token[] flatten()
    {
        Appender!(tk.Token[]) result;

        foreach (size_t index, Element element; elements)
        {
            result.put(element.flatten);

            if (index < elements.length.to!int - 1)
            {
                result.put(separators[index].flatten);
            }
        }

        return result.data;
    }

    override bool opEquals(Object other)
    {
        if (other is null) return false;
        if (cast(typeof(this)) other is null) return false;

        return
            (this.elements == (cast(typeof(this)) other).elements) &&
            (this.separators == (cast(typeof(this)) other).separators);
    }

    override size_t toHash() const
    {
        assert(false, "do not hash!");
    }
}

@ConcatenationNode
class File
{
    mixin ConcatenationNodeCommons;

    Namespace[] namespaces;
    tk.EndOfInput endOfInput;
}

@ConcatenationNode
class Namespace
{
    mixin ConcatenationNodeCommons;

    NamespaceDeclaration namespaceDeclaration;
    NamespaceBody namespaceBody;
}

@ConcatenationNode
class NamespaceBody
{
    mixin ConcatenationNodeCommons;

    NamespaceItemEntry[] namespaceItemEntries;
}

@ConcatenationNode
class NamespaceItemEntry
{
    mixin ConcatenationNodeCommons;

    Nullable!NamespaceItem namespaceItem;
    EndOfLine endOfLine;
}

@AlternationNode
class NamespaceItem
{
    mixin AlternationNodeCommons!(
        FunctionDeclaration,
        ImportDeclaration,
    );
}

@ConcatenationNode
class NamespaceDeclaration
{
    mixin ConcatenationNodeCommons;

    tk.KeywordNamespace namespace;
    AnySpace space1;
    NamespaceIdentifier namespaceIdentifier;
    EndOfLine endOfLine;
}

@SeparatedRepetitionNode
class NamespaceIdentifier
{
    mixin SeparatedRepetitionNodeCommons!(tk.Identifier, NamespaceIdentifierSeparator);
}

@ConcatenationNode
class NamespaceIdentifierSeparator
{
    mixin ConcatenationNodeCommons;

    Nullable!(AnySpace) space1;
    tk.SymbolDot dot;
    Nullable!(AnySpace) space2;
}

@ConcatenationNode
class ImportDeclaration
{
    mixin ConcatenationNodeCommons;

    tk.KeywordImport keywordImport;
    Nullable!AnySpace space1;
    NamespaceIdentifier namespace;
}

@ConcatenationNode
class FunctionDeclaration
{
    mixin ConcatenationNodeCommons;

    tk.KeywordFn fn;
    AnySpace space1;
    tk.Identifier name;
    Nullable!AnySpace space2;
    ParameterBlock parameterBlock;
    Nullable!AnySpace space3;
    Nullable!FunctionReturnType functionReturnType;
    FunctionBody functionBody;
}

@ConcatenationNode
class ParameterBlock
{
    mixin ConcatenationNodeCommons;

    tk.SymbolParenLeft paramStart;
    ParameterListEntry[] parameters;
    Nullable!Parameter trailingParameter;
    Nullable!AnySpace space;
    tk.SymbolParenRight paramEnd;
}

@AlternationNode
class ParameterListEntry
{
    mixin AlternationNodeCommons!(
        ParameterWithComma,
        AnySpace,
    );
}

@ConcatenationNode
class ParameterWithComma
{
    mixin ConcatenationNodeCommons;

    Parameter parameter;
    Nullable!AnySpace space;
    tk.SymbolComma comma;
}

@ConcatenationNode
class Parameter
{
    mixin ConcatenationNodeCommons;

    tk.Identifier name;
    Nullable!AnySpace space1;
    tk.SymbolColon colon;
    Nullable!AnySpace space2;
    TypeExpression typeExpression;
}

@ConcatenationNode
class FunctionReturnType
{
    mixin ConcatenationNodeCommons;

    TypeExpression typeExpression;
    Nullable!AnySpace space;
}

@AlternationNode
class FunctionBody
{
    mixin AlternationNodeCommons!(
        Block,
        tk.KeywordIntrinsic,
    );
}

@ConcatenationNode
class Block
{
    mixin ConcatenationNodeCommons;

    tk.SymbolBraceLeft bodyStart;
    BlockLevelEntry[] blockEntries;
    Nullable!(tk.HSpace) space;
    tk.SymbolBraceRight bodyEnd;
}

@AlternationNode
class BlockLevelEntry
{
    mixin AlternationNodeCommons!(
        BlockLevelStatementEntry,
        EndOfLine,
    );
}

@ConcatenationNode
class BlockLevelStatementEntry
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) leadingSpace;
    Statement statement;
    EndOfLine endOfLine;
}

@AlternationNode
class Statement
{
    mixin AlternationNodeCommons!(
        TypedVariableDefinition,
        Expression,
    );
}

@ConcatenationNode
public class TypedVariableDefinition
{
    mixin ConcatenationNodeCommons;

    tk.KeywordLet let;
    AnySpace space1;
    tk.Identifier name;
    Nullable!AnySpace space2;
    tk.SymbolColon colon;
    Nullable!AnySpace space3;
    TypeExpression typeExpression;
    Nullable!(tk.HSpace) space4;
    tk.SymbolEquals equals;
    Nullable!AnySpace space5;
    Expression initializer;
}

@ConcatenationNode
class Expression
{
    mixin ConcatenationNodeCommons;

    UnaryExpression head;
    Nullable!(PolyadicExpressionTail) tail;
}

@ConcatenationNode
class UnaryExpression
{
    mixin ConcatenationNodeCommons;

    // TODO: Allow HSpaces around prefix operators?
    Nullable!PrefixOp prefixOp;
    AtomicExpression operand;
    PostfixOp[] postfixOps;
}

@AlternationNode
class PrefixOp
{
    mixin AlternationNodeCommons!(
        tk.SymbolMinus,
    );
}

@AlternationNode
class PostfixOp
{
    mixin AlternationNodeCommons!(
        ArgumentBlock,
        MemberAccess,
    );
}

@AlternationNode
class AtomicExpression
{
    mixin AlternationNodeCommons!(
        ParentheticalExpression,
        Boolean,
        Integer,
        String,
        tk.Null,
        IfElseExpression,
        ReturnExpression,
        tk.Identifier,
    );
}

@ConcatenationNode
class ParentheticalExpression
{
    mixin ConcatenationNodeCommons;

    tk.SymbolParenLeft parenLeft;
    Nullable!AnySpace space1;
    Expression expression;
    Nullable!AnySpace space2;
    tk.SymbolParenRight parenRight;
}

@ConcatenationNode
class Boolean
{
    mixin ConcatenationNodeCommons;

    tk.Boolean boolean;
}

@ConcatenationNode
class Integer
{
    mixin ConcatenationNodeCommons;

    tk.Integer integer;
}

@ConcatenationNode
class String
{
    mixin ConcatenationNodeCommons;

    tk.SymbolQuotes quoteStart;
    tk.StringPlaintext stringContents;
    tk.SymbolQuotes quoteEnd;
}

@ConcatenationNode
class IfElseExpression
{
    mixin ConcatenationNodeCommons;

    tk.KeywordIf keywordIf;
    AnySpace space1;
    Expression condition;
    Nullable!AnySpace space2;
    Block thenBlock;
    Nullable!IfElseExpressionTail tail;
}

@ConcatenationNode
class IfElseExpressionTail
{
    mixin ConcatenationNodeCommons;

    Nullable!AnySpace space3;
    tk.KeywordElse keywordElse;
    Nullable!AnySpace space4;
    Block elseBlock;
}

@ConcatenationNode
class ReturnExpression
{
    mixin ConcatenationNodeCommons;

    tk.KeywordReturn return_;
    Nullable!ReturnExpressionPayload payload;
}

@ConcatenationNode
class ReturnExpressionPayload
{
    mixin ConcatenationNodeCommons;

    tk.HSpace space;
    Expression expression;
}

@ConcatenationNode
class ArgumentBlock
{
    mixin ConcatenationNodeCommons;

    tk.SymbolParenLeft argsStart;
    ArgumentListEntry[] arguments;
    Nullable!Expression trailingArgument;
    Nullable!AnySpace space;
    tk.SymbolParenRight argsEnd;
}

@AlternationNode
class ArgumentListEntry
{
    mixin AlternationNodeCommons!(
        ArgumentWithComma,
        AnySpace,
    );
}

@ConcatenationNode
class ArgumentWithComma
{
    mixin ConcatenationNodeCommons;

    Expression expression;
    Nullable!AnySpace space;
    tk.SymbolComma comma;
}

@ConcatenationNode
class MemberAccess
{
    mixin ConcatenationNodeCommons;

    Nullable!(AnySpace) space1;
    tk.SymbolDot dot;
    Nullable!(AnySpace) space2;
    tk.Identifier identifier;
}

@AlternationNode
class PolyadicExpressionTail
{
    mixin AlternationNodeCommons!(
        SumExpressionTail,
        ProductExpressionTail,
        ComparisonExpressionTail,
        LogicalExpressionTail,
        NullCoalescingExpressionTail,
    );
}

@ConcatenationNode
class SumExpressionTail
{
    mixin ConcatenationNodeCommons;

    SumExpressionSegment first;
    SumExpressionSegment[] rest;
}

@ConcatenationNode
class SumExpressionSegment
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) space1;
    SumOperator operator;
    Nullable!(AnySpace) space2;
    UnaryExpression expression;
}

@AlternationNode
class SumOperator
{
    mixin AlternationNodeCommons!(
        tk.SymbolPlus,
        tk.SymbolMinus,
    );
}

@ConcatenationNode
class ProductExpressionTail
{
    mixin ConcatenationNodeCommons;

    ProductExpressionSegment first;
    ProductExpressionSegment[] rest;
}

@ConcatenationNode
class ProductExpressionSegment
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) space1;
    ProductOperator operator;
    Nullable!(AnySpace) space2;
    UnaryExpression expression;
}

@AlternationNode
class ProductOperator
{
    mixin AlternationNodeCommons!(
        tk.SymbolAsterisk,
    );
}

@ConcatenationNode
class ComparisonExpressionTail
{
    mixin ConcatenationNodeCommons;

    ComparisonExpressionSegment first;
    ComparisonExpressionSegment[] rest;
}

@ConcatenationNode
class ComparisonExpressionSegment
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) space1;
    ComparisonOperator operator;
    Nullable!(AnySpace) space2;
    UnaryExpression expression;
}

@AlternationNode
class ComparisonOperator
{
    mixin AlternationNodeCommons!(
        tk.SymbolDoubleEquals,
    );
}

@ConcatenationNode
class LogicalExpressionTail
{
    mixin ConcatenationNodeCommons;

    LogicalExpressionSegment first;
    LogicalExpressionSegment[] rest;
}

@ConcatenationNode
class LogicalExpressionSegment
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) space1;
    LogicalOperator operator;
    Nullable!(AnySpace) space2;
    UnaryExpression expression;
}

@AlternationNode
class LogicalOperator
{
    mixin AlternationNodeCommons!(
        tk.SymbolAmpersand,
    );
}

@ConcatenationNode
class NullCoalescingExpressionTail
{
    mixin ConcatenationNodeCommons;

    NullCoalescingExpressionSegment first;
    NullCoalescingExpressionSegment[] rest;
}

@ConcatenationNode
class NullCoalescingExpressionSegment
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) space1;
    NullCoalescingOperator operator;
    Nullable!(AnySpace) space2;
    UnaryExpression expression;
}

@AlternationNode
class NullCoalescingOperator
{
    mixin AlternationNodeCommons!(
        tk.SymbolDoubleQuestionMark,
    );
}

// Types

@ConcatenationNode
class TypeExpression
{
    mixin ConcatenationNodeCommons;

    AtomicTypeExpression payload;
    Nullable!TypePostfix postfix;
}

@AlternationNode
class AtomicTypeExpression
{
    mixin AlternationNodeCommons!(
        tk.Identifier,
    );
}

@ConcatenationNode
class TypePostfix
{
    mixin ConcatenationNodeCommons;

    Nullable!(AnySpace) space;
    TypePostfixOp postfixOperator;
}

@AlternationNode
class TypePostfixOp
{
    mixin AlternationNodeCommons!(
        tk.SymbolQuestionMark,
    );
}


// Spacing and Comments

@AlternationNode
class AnySpace
{
    mixin AlternationNodeCommons!(
        BlankLines,
        tk.HSpace,
    );
}

@ConcatenationNode
class BlankLines
{
    mixin ConcatenationNodeCommons;

    EndOfLine mandatoryLine;
    EndOfLine[] optionalLines;
    Nullable!(tk.HSpace) finalSpace;
}

@ConcatenationNode
class EndOfLine
{
    mixin ConcatenationNodeCommons;

    Nullable!(tk.HSpace) leadingSpace;
    Nullable!Comment comment;
    tk.VSpace vSpace;
}

@ConcatenationNode
class Comment
{
    mixin ConcatenationNodeCommons;

    tk.SymbolSemicolon semicolon;
    Nullable!(tk.HSpace) space1;
    Nullable!(tk.CommentPlaintext) comment;
}
