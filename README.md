# Dysnomia 

Dysnomia is a programming language designed with a focus on clean, friendly, and unambiguous syntax.

## Repository Structure

- `.git-hooks` contains ... Git hooks.
- `.joyride` configures the [Joyride
  plugin](https://github.com/BetterThanTomorrow/joyride/) for Visual Studio
  Code. It adds all tests from the test folder to VSCode's "Test Explorer"
  panel.
- `.vscode` contains the usual VSCode stuff.
- `doc` documents Dysnomia, though as of now it's not very up-to-date.
- `scripts` contains various helper scripts.
  - Most of these scripts are written in D but can be run as normal scripts due
    to their shebangs.
  - The various `test`-related scripts run or watch unit and "integration"
    tests.
  - `show_all_error_messages.d` runs all tests in the `test` directory that
    should output compile errors, and displays those errors.
  - `unregress_tests.fish` replaces all `expected_(cst/ast/tokens).sdl` files in
    the test directory with whatever is in the corresponding
    `actual_(cst/ast/tokens).sdl` files. This is useful for when a minor test
    adjustment is made that results in a lot of manual labor (e.g. adjusting
    string indices).
  - `setup.sh` configured Git to run the hooks in the `.git-hooks` directory.
- `source` contains all the D source code for the compiler.
  `source/dysnomia/cli/main.d` contains the main function for running Dysnomia
  normally. `source/dysnomia/generate_grammar.d` contains the main function for
  the `generate-grammar` DUB configuration, which keeps the ABNF files
  up-to-date with the source code.
- `stdlib` contains the standard library for dysnomia.
- `test` contains sample Dysnomia programs and their expected behaviors. These
  tests are run by `scripts/run_tests.d` and are detected and run by the Joyride
  plugin. The only sub-folder is `build-and-run` since that's all Dysnomia can
  do at the moment, but in the future it might make sense to have, say, `lint`
  or `format` sub-folders as well. The `build-and-run` folder itself is
  subdivided according to how I think it makes sense to introduce language
  features.
- The top-level files in the repo are mostly standard Git and D/Dub things. The
  `.abnf` files are  auto-generated grammar files for both the concrete and
  abstract grammars. The concrete grammar includes spacing, comments, and
  literal string nodes such as keywords. The abstract grammar excludes these.

## The Big Ideas

### Types

A basic variable declaration is as follows:
```
let x: Int
```

...and a function declaration:
```
fn foo(x: String) Float {
  ; ...
}
```

Types are written in postfix and are always CamelCase.

Dysnomia allows somewhat low-level memory control, and the type system should support this as much as possible. References to data use square brackets:
```
let ref_to_a_number: [Int]
```

Multiple values can be stored in one variable (tuples):
```
let stuff: (Int String)         ; An Int and a String
let eight_letters: (8*Char)     ; Eight characters, i.e. a static array of length 8.
```

These can all combined:
```
let some_data: [Float 10*(String String)]   ; A reference to a float and a 10-element array of (String String) tuples
```

Dynamically resizable arrays are also supported:
```
let result: [*Int]
```

Types can be named:
```
typealias Person = (
  String
  String
  Int
)
```

Structs with named fields :

```
struct Person {
  name: String
  surname: String
  parents: (2*[Person])
  children: [*[Person]]
}
```

A similar struct in C (without any extra fields for array bounds checking):
```c
struct Person {
  char* name;
  char* surname;
  Person* parents[2];
  Person** children;
}
```


### Flag arguments

Has this ever happened to you?
```c
someFunction("yes", 83, true, true, true, false, true, false, false)
```

Perhaps you avoid it using bit flags?
```c
someFunction("yes", 83, FLAG_HAS_TRAIT | FLAG_DOES_THING | FLAG_MAKES_STUFF | FLAG_READS_DATA)
```

Or maybe your language supports named and default parameters?
```kotlin
someFunction("yes", 83, hasTrait: true, doesThing: true, makesStuff: true, readsData: true)
```

But what if you could do something like this?
```
someFunction("yes", 83, #hasTrait, #doesThing, #makesStuff, #readsData)
```

Just an idea for now but I think it could be pretty useful.


### Avoiding precedence ambiguity

Code like the following can be a bit tricky for a human to understand:
```d
a.b + -c() * d && e == false ? f * -g + h : i - -j / k;
```

Dysnomia doesn't allow this. Instead, you would write something like:

```d
(((a.b + (-c() * d)) && e) == false) ? ((f * -g) + h) : (i - (-j / k))
```

While still noisy, the reader doesn't get distracted figuring out operator precedence.

For now, there are three levels of precedence:
- member access (`a.b`) and function calls (`c()`)
- unary operators (`-g`)
- all other binary operators

Within each level, different operators may not be mixed without clarifying parentheses, with the following exceptions:
- Addition and subtraction (`a + b - c`)
- Multiplication and division (`a * b / c`)
- Member access and function calls (`a.b()`)

These are always evaluated left-to-right.

The reasoning behind these levels is to allow omitting parentheses only when tradition spacing conventions make the meaning clear:
```d
-a.b.c() + d.e.f;
```

You *could* write this as:
```d
- a . b . c()+d . e . f;
```

But this is silly. Existing conventions for spacing more-or-less make operator precedence obvious, but Dysnomia enforces this.

In the future there might be a fourth level for operators that are typically written across multiple lines, such as pipes:
```elixir
a.b + c.d
|> e.f()
|> g()
```

Parentheses around `a.b + c.d` aren't needed because the line breaks help with readability.


## Compiler phases

```mermaid
graph TD;
    source(Source Code<br/>UTF-8)
    tokens(List of Tokens<br/><code>dysnomia.data.tokens</code>)
    cst(Concrete Syntax Tree<br/><code>dysnomia.data.cst</code>)
    ast(???)

    source-->|<code>dysnomia.phases.lex</code>|tokens
    tokens-->|<code>dysnomia.phases.parse</code>|cst
    cst-->|<code>dysnomia.phases.to_ast</code>|ast
```
