 ERROR  @ test/build-and-run/08_if/if_type_mismatch/source.dys:8:9
The blocks of the if/else expression do not evaluate to the same type:
- The first block ends with 20 of type Int
- The second block ends with "ten" of type String
