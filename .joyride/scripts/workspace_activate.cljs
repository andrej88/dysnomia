(ns workspace-activate
  (:require [joyride.core :as joyride]
            ["vscode" :as vscode]
            [promesa.core :as p]
            ["child_process" :as cp]
            [clojure.string :as s]))


(defonce !db (atom {:disposables []}))


#_{:clj-kondo/ignore [:unused-private-var]}
(defn- inspect [pre v]
  (println pre v "\n")
  v)


;; To make the activation script re-runnable we dispose of
;; event handlers and such that we might have registered
;; in previous runs.
(defn- clear-disposables! []
  (run! (fn [disposable]
          (when (not (nil? (.-dispose disposable)))
            (.dispose disposable)))
        (:disposables @!db))
  (swap! !db assoc :disposables []))


;; Pushing the disposables on the extension context's
;; subscriptions will make VS Code dispose of them when the
;; Joyride extension is deactivated.
(defn- push-disposable [disposable]
  (swap! !db update :disposables conj disposable)
  (-> (joyride/extension-context)
      .-subscriptions
      (.push disposable)))


(defn- test-item-collection-to-vec [tic]
  (let [result (atom [])]
    (.forEach tic (fn [e]
                    (swap! result conj e)))
    (clj->js @result)))


(defn- remove-sgr-codes [text]
  (s/replace text #"\x1B\[[\d;]*m" ""))


(defn- get-or-create-root-test-entry [test-controller uri]
  (let [existing (-> test-controller .-items (.get (.toString uri)))]
    (if (nil? existing)
      (let [test-item (.createTestItem test-controller
                                       (.toString uri)
                                       (-> uri .-path (.split "/") .pop)
                                       uri)]
        (-> test-controller .-items (.add test-item))
        test-item)
      existing)))


(defn- get-or-create-test-entry-recursively [test-controller [node-name children] parent-test-item parent-uri]
  (let [uri (vscode/Uri.joinPath parent-uri node-name)
        existing (-> parent-test-item .-children (.get (.toString uri)))
        test-item (or existing (let [new-test-item (.createTestItem test-controller
                                                                    (.toString uri)
                                                                    node-name
                                                                    uri)]
                                 (-> parent-test-item .-children (.add new-test-item))
                                 new-test-item))]

    (when (map? children)
      (let [children-to-delete (atom [])]
        (run! (fn [child-test-item]
                (let [child-test-item-basename (-> child-test-item .-uri .-path (.split "/") last)
                      is-this-child-expected (children child-test-item-basename)]
                  (when (not is-this-child-expected)
                    (swap! children-to-delete #(conj % (.-id child-test-item))))))

              (-> test-item
                  .-children
                  test-item-collection-to-vec))

        (run! (fn [child-id]
                (-> test-item
                    .-children
                    (.delete child-id)))
              @children-to-delete)))

    (when (map? children)
      (run! (fn [child-entry]
              (get-or-create-test-entry-recursively test-controller child-entry test-item uri))
            (into (sorted-map) children)))

    test-item))


(defn clear-test-items [test-controller]
  (let [items-to-delete (atom [])]
    (-> test-controller .-items (.forEach (fn [test-item]
                                            (swap! items-to-delete #(conj % (.-id test-item))))))
    (run! (fn [test-item-id]
            (-> test-controller
                .-items
                (.delete test-item-id)))
          @items-to-delete)))


(defn- discover-test-files-in-workspace [test-controller workspace-folder]
  (-> (vscode/workspace.findFiles (vscode/RelativePattern. workspace-folder "test/build-and-run/**/*.dys"))
      (p/then (fn [files]
                (let [paths-split (map (fn [e]
                                         {:uri e
                                          :path-vec (-> e .-path (.split "/"))})
                                       files)

                      paths-without-file (map (fn [e]
                                                (update e :path-vec #(drop-last %)))
                                              paths-split)

                      paths-relative-from-test-root (map (fn [e]
                                                           (let [index-of-test-root (.indexOf (:path-vec e) "test")]
                                                             (update e :path-vec #(drop (inc index-of-test-root) %))))
                                                         paths-without-file)

                      test-tree (reduce (fn [acc e]
                                          (assoc-in acc (:path-vec e) (:uri e)))
                                        {}
                                        paths-relative-from-test-root)]

                  (->> test-tree
                       (into (sorted-map))
                       (run! (fn [[k v]]
                               (let [root-test-uri (vscode/Uri.joinPath (vscode/Uri.file vscode/workspace.rootPath) "test" k)
                                     root-test-entry (get-or-create-root-test-entry test-controller root-test-uri)]
                                 (->> v
                                      (into (sorted-map))
                                      (run! (fn [entry]
                                              (get-or-create-test-entry-recursively test-controller
                                                                                    entry
                                                                                    root-test-entry
                                                                                    root-test-uri)))))))))))))


(defn- discover-test-files-everywhere [test-controller]
  (.map (.-workspaceFolders vscode/workspace)
        #(discover-test-files-in-workspace test-controller %)))


(defn- initialize-test-controller [test-controller]
  (.map (.-workspaceFolders vscode/workspace)
        (fn [workspace-folder]
          (let [pattern (vscode/RelativePattern. workspace-folder "test/build-and-run/**/*")
                watcher (vscode/workspace.createFileSystemWatcher pattern false true false)]

            (.onDidCreate watcher
                          (fn []
                            (discover-test-files-in-workspace test-controller workspace-folder)))

            (.onDidDelete watcher
                          (fn []
                            (discover-test-files-in-workspace test-controller workspace-folder)))

            (push-disposable watcher))))
  (discover-test-files-everywhere test-controller))


(defn- get-test-leaves [test-items]
  (->> test-items
       (reduce (fn [acc e]
                 (let [children (-> e .-children)
                       result (if (or (nil? children) (= (-> children .-size) 0))
                                (conj acc e)
                                (->> children
                                     test-item-collection-to-vec
                                     get-test-leaves
                                     (concat acc)))]
                   result))
               [])
       clj->js))


(defn- create-promise-for-test-item [test-run test-item]
  (.started test-run test-item)
  (p/let [test-path (.-uri test-item)

          dysnomia-exe-path    (-> vscode/workspace.rootPath
                                   vscode/Uri.file
                                   (vscode/Uri.joinPath "dysnomia")
                                   .-path)

          get-uri-for-test-file (fn [file-basename]
                                  (vscode/Uri.joinPath test-path file-basename))

          test-source-files-pattern (vscode/RelativePattern. test-path "*.dys")

          test-source-files (p/->> test-source-files-pattern
                                   vscode/workspace.findFiles
                                   (map #(.-path %))
                                   sort)    ; Sort because sometimes the order in which test files are provided affects the error shown (e.g. duplicate namespace names)

          expectation-files-pattern (vscode/RelativePattern. test-path "{expected_tokens.sdl,expected_cst.sdl,expected_ast.sdl,expected_exit_code.txt,expected_error.txt,expected_output.txt}")

          expectation-files-uris (vscode/workspace.findFiles expectation-files-pattern)

          expected-results-from-files (p/->> expectation-files-uris
                                             (map (fn [uri]
                                                    (-> (vscode/workspace.openTextDocument uri)
                                                        (p/then (fn [file]
                                                                  [(case (-> uri .-path (.split "/") .pop)
                                                                     "expected_tokens.sdl" :tokens
                                                                     "expected_cst.sdl" :cst
                                                                     "expected_ast.sdl" :ast
                                                                     "expected_exit_code.txt" :exit-code
                                                                     "expected_output.txt" :stdout
                                                                     "expected_error.txt" :stderr)
                                                                   (-> file .getText s/trim-newline)])))))
                                             p/all
                                             (into {}))


          ; Exit code 0, stderr empty:
          ;     Fine
          ;
          ; Exit code != 0, stderr empty:
          ;     Fine, main function returns some value
          ;
          ; Exit code 0, stderr non-empty:
          ;     Weird? A warning was printed, maybe? Although that can't happen yet.
          ;
          ; Exit code != 0, stderr non-empty:
          ;     Error during compilation probably

          ; If neither an error message nor a special exit code are expected,
          ; then either of those happening indicates an error and both should be
          ; shown as failure messages. However, non-zero exit codes can happen
          ; without an error being printed (e.g. the dysnomia main function
          ; returns an Int), and an error may be printed without the test caring
          ; about the specific exit code the compiler returns.
          expected-results (cond-> expected-results-from-files

                             (nil? (:stderr expected-results-from-files))          (assoc :stderr "")

                             (and (nil? (:stderr expected-results-from-files))
                                  (nil? (:exit-code expected-results-from-files))) (assoc :exit-code "0"))

          dysnomia-options (as-> (keys expected-results) $
                             (map (fn [expectation-type]
                                    (case expectation-type
                                      :tokens (str "--debug-print-tokens=" (.-path (get-uri-for-test-file "actual_tokens.sdl")))
                                      :cst (str "--debug-print-cst=" (.-path (get-uri-for-test-file "actual_cst.sdl")))
                                      :ast (str "--debug-print-ast=" (.-path (get-uri-for-test-file "actual_ast.sdl")))
                                      nil))
                                  $)
                             (filter some? $)
                             (conj $ (str "--debug-print-runtime=" (.-path (get-uri-for-test-file "runtime.txt"))))
                             (conj $ (str "--stdlib-path=./stdlib")))]

    (if (or (empty? test-source-files) (empty? expected-results))
      (.skipped test-run test-item)
      (p/let [run-output (p/create (fn [dys-resolve]
                                     (cp/execFile dysnomia-exe-path
                                                  (clj->js (concat dysnomia-options test-source-files))
                                                  #js {:cwd vscode/workspace.rootPath}
                                                  (fn [err stdout stderr]
                                                    (dys-resolve {:exit-code (str (if (nil? err)
                                                                                    0
                                                                                    (.-code err)))
                                                                  :stdout (-> stdout
                                                                              remove-sgr-codes
                                                                              s/trim-newline)
                                                                  :stderr (-> stderr
                                                                              remove-sgr-codes
                                                                              s/trim-newline)})))))

              actual-files-pattern (vscode/RelativePattern. test-path "{actual_tokens.sdl,actual_cst.sdl,actual_ast.sdl,runtime.txt}")
              actual-files-uris (vscode/workspace.findFiles actual-files-pattern)
              actual-file-outputs (p/->> actual-files-uris
                                         (map (fn [uri]
                                                (-> (vscode/workspace.openTextDocument uri)
                                                    (p/then (fn [file]
                                                              [(case (-> uri .-path (.split "/") .pop)
                                                                 "actual_tokens.sdl" :tokens
                                                                 "actual_cst.sdl" :cst
                                                                 "actual_ast.sdl" :ast
                                                                 "runtime.txt" :duration)
                                                               (-> file .getText s/trim-newline)])))))
                                         p/all
                                         (into {}))

              actual-results (merge run-output
                                    (cond-> actual-file-outputs
                                      (:duration actual-file-outputs) (update :duration #(js/parseFloat %))))]

        (let [failing-expectations (->> expected-results
                                        (map (fn [[expected-value-type expected-value]]
                                               [expected-value-type (= expected-value (expected-value-type actual-results))]))
                                        (filter (fn [[_ did-pass]] (not did-pass)))

                                        (into {}))
              duration (if (:duration actual-results)
                         (* (:duration actual-results) 1000.0)
                                                                       ; It needs to be actual `undefined` produced in
                                                                       ; the undefined region of JavaScript, otherwise
                                                                       ; it's just sparkling null.
                         (js/eval "undefined"))]

          (if (empty? failing-expectations)
            (.passed test-run test-item duration)
            (let [messages (->> failing-expectations
                                (map (fn [[type _]]
                                       (let [message (str "Unexpected "
                                                          (case type
                                                            :tokens "tokens"
                                                            :cst "CST"
                                                            :ast "AST"
                                                            :stdout "standard output"
                                                            :stderr "standard error"
                                                            :exit-code "exit code"))]
                                         (vscode/TestMessage.diff message
                                                                  (type expected-results)
                                                                  (type actual-results)))))
                                clj->js)]
              (.failed test-run test-item messages duration))))))))


(defn- run-handler [test-controller request _cancellation-token]
  (let [this-test-run (.createTestRun test-controller request)]
    (-> (p/let [top-level-test-items (or (.-include request) (-> test-controller .-items test-item-collection-to-vec))
                test-items           (get-test-leaves top-level-test-items)


                dub-build-output (p/create (fn [resolve reject]
                                             (cp/exec "dub build"
                                                      #js {:cwd vscode/workspace.rootPath}
                                                      (fn [err stdout stderr]
                                                        (if (not (nil? err))
                                                          (reject [err stdout stderr])
                                                          (resolve stdout))))))]


          (.appendOutput this-test-run (s/replace dub-build-output "\n" "\r\n") "\r\n")

          (p/do
            (->> test-items
                 (map #(create-promise-for-test-item this-test-run %))
                 p/all)

            (.end this-test-run)))
        (p/catch (fn [[_err stdout stderr]]
                   (.appendOutput this-test-run "DUB build failed\r\n\r\n")
                   (.appendOutput this-test-run "Standard Output:\r\n\r\n")
                   (.appendOutput this-test-run (str (s/replace stdout "\n" "\r\n") "\r\n\r\n"))
                   (.appendOutput this-test-run "Standard Error:\r\n\r\n")
                   (.appendOutput this-test-run (str (s/replace stderr "\n" "\r\n") "\r\n\r\n"))
                   (.end this-test-run))))))


(defn- my-main []
  (clear-disposables!)

  (p/let [test-controller (vscode/tests.createTestController "dysnomia-unit-test-controller" "Dysnomia Unit Tests")]

    (initialize-test-controller test-controller)

    (set! (.-refreshHandler test-controller)
          (fn []
            (clear-test-items test-controller)
            (discover-test-files-everywhere test-controller)))

    (set! (.-resolveHandler test-controller)
          (fn [test]
            (if (nil? test)
              (do
                (clear-test-items test-controller)
                (discover-test-files-everywhere test-controller))
              (println "not yet implemented"))))

    (.createRunProfile test-controller
                       "Run Tests"
                       (.-Run vscode/TestRunProfileKind)
                       (fn [request cancellation-token]
                         (run-handler test-controller request cancellation-token))
                       true)

    (push-disposable test-controller)))


(when (= (joyride/invoked-script) joyride/*file*)
  (my-main))
