# Lexing

The lexer processes the source code character by character and produces a list of tokens. The source code must be UTF-8 encoded and use Line Feed (`0x0A`) characters to seperate lines.

Comments and string literals may contain any valid UTF-8 codepoint, except that string literals cannot contain a `"` since that indicates the end of the string and comments cannot contain a line break since that indicates the end of the comment.


## Identifiers

Identifiers used as variable, function, and namespace names must start and end with an alphabetic character and may contain alphabetic characters or underscores in-between. Keywords (see below) are not valid identifiers.


## Integers

Integer number literals are either:
- the single digit `0`
- any digit from `1` through `9` optionally followed by any combination of digits `0` through `9` and underscores.


## Spacing

Where the grammar calls for whitespace (horizontal spacing), any combination of spaces (`0x20`) or horizontal tabs (`0x09`) may be used.


## Line breaks

Where the grammar calls for line breaks (vertical spacing), Line Feeds (`0x0A`) must be used.


## Symbols

The following non-alphanumeric, non-whitespace symbols are accepted by the lexer:

`{ } : , = ( ) + - . ; "`


## Keywords

The following words have a special meaning to the lexer and will never be interpreted as identifiers:

`fn if let return namespace`


## String Literals

String literals begin and end with `"` and may contain any (or no) characters in between.


## Comments

Comments begin with a `;` and end when the line ends.
