# Language Overview

Dysnomia source files contain namespaces. It is recommended to use one namespace per file, and for the namespace name to match the path of that file.

A namespace may contain function declarations or imports from other namespaces.

For example, a file at `my_hello_world_repository/src/hello_world/main.dys` might look like this:

```
namespace HelloWorld.Main

import Dys.Io


fn main() {
    Io.println("Hello, world!")
}
```

Namespace and type identifiers are written in `PascalCase`, while variable, function, and file names are written in `snake_case`.
