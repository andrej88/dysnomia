# Introduction

Dysnomia is a high-level language that is compiled to WebAssembly. It is statically typed and its syntax is loosely based on Kotlin and Swift.

The compilation of Dysnomia code goes through several steps under-the-hood:

1. **Reading:** The file containing Dysnomia source code is loaded into memory.
2. **[Lexing](./Lexing.md):** The source code is processed character by characted and a list of tokens is produced.
3. **Parsing:** The list of tokens is converted into a Concrete Syntax Tree. The CST can be converted back into the original text exactly.
4. **Syntax Abstraction:** The CST is converted into an Abstract Syntax Tree by removing comments, line breaks, spaces, and tokens that provides redundant information (such as the `=` in variable definitions).
5. **Static Analysis:** The AST is analyzed to ensure the code is as sound as it can be. Type Checking occurs in this phase. Control flow analysis happens as well, to ensure functions have valid return statements in all paths.
6. **Bytecode Generation:** The AST is converted into a more linear data structure similar to other languages' bytecode. At the moment there is no serializable binary format for the bytecode.
7. **Run:** The bytecode-like data structure is interpreted.


## How to Use

Run the `dysnomia` executable passing in all required source files as well as the path to the standard library:

```sh
./dysnomia --stdlib-path=./stdlib ./my_file.dys ./my_other_file.dys
```
