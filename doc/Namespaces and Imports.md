# Namespaces and Imports

## Namespaces

Dysnomia code is organized into namespaces. A namespace declaration looks like this:

```
namespace MyNamespace
```

Anything that is defined after that line and before the end of the file (or before the next namespace declaration) belongs to the namespace called `MyNamespace`.

Namespaces names may consist of several parts to indicate that they are nested within other namespaces:

```
namespace MyApp.DataStructures.TernaryHeap
```

Here, the namespace `MyApp` contains another namespace `DataStructures` which itself contains a namespace `TernaryHeap`.

Typically, namespace names should match the path of the file they're contained in, but this is not enforced. For example, the file `my_app_repo/src/my_app/cli.dys` would start with the following:

```
namespace MyApp.Cli
```

A file may contain several namespaces.


## Imports

Other namespaces may be imported using the `import` keyword. Once imported, the namespace may be referred to using the last segment of its name.

```
namespace MyApp

import Dys.Io

fn main() {
    Io.println("Hello, world!")
}
```

Here, we import the namespace `Dys.Io`, part of Dysnomia's standard library. This namespace provides us with the `println` function used for printing text to standard output. To call this function, we need to access it as a member of `Io`.
