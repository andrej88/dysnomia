# Dysnomia

## Preface
This documentation, like Dysnomia itself, is a work in progress. Sentences and paragraphs in *italics* talk about features not yet implemented in the language.

1. [Introduction](./Introduction.md)
2. [Language Overview](./Language%20Overview.md)
2. [Lexing](./Lexing.md)
3. [Namespaces and Imports](./Namespaces%20and%20Imports.md)
99. [Code Style](./Code%20Style.md)
