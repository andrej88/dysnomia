# Dysnomia code style guidelines

These are conventions for code written in Dysnomia, not for the compiler's source code which is written in D.


## Source text format

Source code should be UTF-8 encoded.


## Naming conventions

Names of things relevant mostly to the compiler, such as namespaces or types, are written in `PascalCase`.

Names of things relevant mostly during the execution of the program, such as variables or functions, are written in `snake_case`.

This is just a rule of thumb, and there are exceptions. For example, compile-time constants should use `snake_case`, since they are more similar to variables than types or namespaces.

If a name contains an acronym, that acronym is cased like any other word: `HttpRequest`, `Json.Parser`, `to_html()`, and so on.


## Indentation

Undecided. Do what you want, I guess.
